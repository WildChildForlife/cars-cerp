<?php
namespace CERP\SiteBundle\TwigExtension;


class TypeOfTwigExtension extends \Twig_Extension {
  public function isBoolean($var)
  {
    return is_bool($var);
  }
  public function typeOf($var, $type_test=null)
    {
 
    switch ($type_test)
    {
        default:
                return false;
                break;
 
        case 'array':
                return is_array($var);
                break;
 
        case 'bool':
                return is_bool($var);
                break;	
 
        case 'float':
                return is_float($var);
                break;
 
        case 'int':
                return is_int($var);
                break;
 
        case 'numeric':
                return is_numeric($var);
                break;
 
        case 'object':
                return is_object($var);
                break;
 
        case 'scalar':
                return is_scalar($var);
                break;
 
        case 'string':
                return is_string($var);
                break;
        case 'datetime':
                return ($var instanceof \DateTime);
                break;
    }
}
 
 
  // Twig va exécuter cette méthode pour savoir quelle(s) fonction(s) ajoute notre service
  public function getFunctions()
  {
    return array(
      'typeOf' => new \Twig_Function_Method($this, 'typeOf')
    );
  }
    public function getTests()
  {
    return array(
      'typeOf' => new \Twig_Function_Method($this, 'typeOf')
    );
  }
  // La méthode getName() identifie votre extension Twig, elle est obligatoire
  public function getName()
  {
    return 'TypeOfTwigExtension';
  }
 
 
}