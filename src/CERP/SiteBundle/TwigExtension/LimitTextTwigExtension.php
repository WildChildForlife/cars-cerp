<?php
namespace CERP\SiteBundle\TwigExtension;


class LimitTextTwigExtension extends \Twig_Extension 
{

    public function limitText($sString, $iLimit)
    {
        $sString = htmlspecialchars_decode($sString);
        return ( strlen((string)$sString) > $iLimit ) ? substr($sString, 0, $iLimit) . '...' : $sString;
    }



    // Twig va exécuter cette méthode pour savoir quelle(s) fonction(s) ajoute notre service
    public function getFunctions()
    {
        return array(
            'limitText' => new \Twig_Function_Method($this, 'limitText')
        );
    }
    // La méthode getName() identifie votre extension Twig, elle est obligatoire
    public function getName()
    {
        return 'LimitTextTwigExtension';
    }

 
}