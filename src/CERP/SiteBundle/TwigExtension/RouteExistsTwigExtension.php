<?php
namespace CERP\SiteBundle\TwigExtension;


class RouteExistsTwigExtension extends \Twig_Extension 
{

    protected $oContainer;
    protected $oRequest;
    protected $oSession;

    public function __construct($oContainer)
    {
        $this->oContainer = $oContainer;
    }


    function routeExists($sName)
    {
        // I assume that you have a link to the container in your twig extension class
        $oRouter = $this->oContainer->get('router');
        return (null === $oRouter->getRouteCollection()->get($sName)) ? false : true;
    }



    // Twig va exécuter cette méthode pour savoir quelle(s) fonction(s) ajoute notre service
    public function getFunctions()
    {
        return array(
            'routeExists' => new \Twig_Function_Method($this, 'routeExists')
        );
    }
    // La méthode getName() identifie votre extension Twig, elle est obligatoire
    public function getName()
    {
        return 'RouteExistsTwigExtension';
    }

 
}