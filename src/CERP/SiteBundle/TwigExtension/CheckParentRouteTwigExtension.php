<?php
namespace CERP\SiteBundle\TwigExtension;

use CERP\SiteBundle\Entity\Page;



class CheckParentRouteTwigExtension extends \Twig_Extension 
{

    public function CheckParentR($oPage, $sRoute)
    {
        // Routes initial ou parent de la page actuel ---PAS EGAL--- avec le route de l'item
        if ( is_object($oPage) && $oPage->getRoute() != $sRoute )
        {

            // Page actuelle ---CONTIENT--- un parent
            if ( $oPage->getPageParente() != NULL )  
            {
                return $this->CheckParentR($oPage->getPageParente(), $sRoute);  
            }
            // SI PLUS DE PARENTS
            else return false;          
        }
        // Si le route de la page ---EGALE--- le route de l'item
        else return true;

        return false;
    }



    // Twig va exécuter cette méthode pour savoir quelle(s) fonction(s) ajoute notre service
    public function getFunctions()
    {
        return array(
            'CheckParentR' => new \Twig_Function_Method($this, 'CheckParentR')
        );
    }
    // La méthode getName() identifie votre extension Twig, elle est obligatoire
    public function getName()
    {
        return 'CheckParentRouteTwigExtension';
    }

 
}