<?php
namespace CERP\SiteBundle\TwigExtension;


class PluralTwigExtension extends \Twig_Extension 
{

    public function plural($sString, $iNumber)
    {
        return ( $iNumber < 2 ) ? $iNumber . ' ' . $sString : $iNumber . ' ' . $sString . 's';
    }



    // Twig va exécuter cette méthode pour savoir quelle(s) fonction(s) ajoute notre service
    public function getFunctions()
    {
        return array(
            'plural' => new \Twig_Function_Method($this, 'plural')
        );
    }
    // La méthode getName() identifie votre extension Twig, elle est obligatoire
    public function getName()
    {
        return 'pluralTwigExtension';
    }

 
}