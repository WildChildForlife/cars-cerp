<?php 
namespace CERP\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ListRouteFormType extends AbstractType
{
    private $oContainer;
    private $oListRoutes;

    public function __construct($oContainer)
    {
        $this->oContainer = $oContainer;

        $oRoutes = $oContainer->get('router');
        $oRoutes = $oRoutes->getRouteCollection()->all();

        $aRoutes = [];

        $oGlobalFunctions = $oContainer->get('globalfunctions');
        //if ( $oGlobalFunctions->strpos_arr($iKey, $this->oContainer->getParameter('a2lix_translation_form.locales'), "__RG") !== false )    

        $sDefaultLanguage = $this->oContainer->getParameter('locale');
        $sDefaultLanguagePrefixRoute = $sDefaultLanguage . "__RG__AS";

        foreach ($oRoutes as $iKey => $sValue) 
        {
            if ( strpos($iKey, $sDefaultLanguagePrefixRoute) !== false )    
            $aRoutes[substr($iKey, count($sDefaultLanguage) + 1)] = $sValue->getPath();
        }

        $this->oListRoutes = $aRoutes;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => $this->oListRoutes
        ));
    }

    

    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'list_route_form';
    }
}


 ?>