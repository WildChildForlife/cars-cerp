<?php 
namespace CERP\SiteBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Doctrine\ORM\EntityRepository;

class ItemAdmin extends Admin
{

    protected $baseRoutePattern = 'item';
    public $supportsPreviewMode = true;


    //   Preview
    protected function configureShowFields(ShowMapper $showMapper)
    {

        $showMapper

            ->add('nom')
            ->add('route')
            ->add('class')
            ->add('position')
            ->add('blank')
            ;

    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('translations', 'a2lix_translations_gedmo', array(
                'translatable_class' => "CERP\SiteBundle\Entity\Item",
                'label' => 'Nom',
                'fields' => array( 
                        'nom' => array(                   
                            'field_type' => null,                
                            'label' => false,          
                            'locale_options' => array(          
                                'fr' => array(
                                    'label' => false          
                                ),
                                'en' => array(
                                    'label' => false          
                                )
                            )
                        )
                    )
                ))
            // ->add('nom', null, array('label' => 'Nom'))
            ->add('page', null, array('label' => 'Page liée', 'empty_value' => 'Aucun', 'attr' => array( 'class' => 'page-relation' )))
            ->add('route', 'list_route_form', array('label' => 'Route', 'required' => false, 'attr' => array( 'class' => 'route-item' )))
            ->add('class', null, array('label' => 'Class HTML','required' => false))
            ->add('position', null, array('label' => 'Position', 'required' => false))
            ->add('blank', null, array('label' => 'Ouvrir dans un nouvel onglet'))            
            ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nom')
            ->add('route')
            ->add('class')
            ->add('position')
            ->add('blank')

        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->addIdentifier('media', 'sonata_type_model_list', array('label' => 'Media', 'template' => 'ApplicationSonataAdminBundle:User:picture.html.twig'), array('link_parameters' => array('context' => 'page')))            
            ->add('nom')
            ->add('route')
            ->add('class')
            ->add('position')
            ->add('blank')

         ;
    }
}
?>