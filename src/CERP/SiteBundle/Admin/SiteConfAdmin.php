<?php 
namespace CERP\SiteBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Doctrine\ORM\EntityRepository;

class SiteConfAdmin extends Admin
{

    protected $baseRoutePattern = 'siteconf';
    public $supportsPreviewMode = true;

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('delete')
            ;
    }


    //   Preview
    protected function configureShowFields(ShowMapper $showMapper)
    {

        $showMapper

            ->add('contactAdmin')
            ;

    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('contactAdmin', 'email', array('label' => 'Contact Admin'))
            ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('contactAdmin')            
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper            
            ->add('contactAdmin', null, array('editable' => true))

         ;
    }
}
?>