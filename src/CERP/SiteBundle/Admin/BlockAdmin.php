<?php 
namespace CERP\SiteBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Doctrine\ORM\EntityRepository;

class BlockAdmin extends Admin
{

    protected $baseRoutePattern = 'block';
    public $supportsPreviewMode = true;


    //   Preview
    protected function configureShowFields(ShowMapper $showMapper)
    {

        $showMapper

            ->add('contenu')
            ->add('title')
            ->add('date')
            ;

    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', null, array('label' => 'Titre'))
            ->add('groupe', null, array('label' => 'Groupe'))
            ->add('contenu', 'sonata_type_immutable_array', array(
                'label' => 'Contenu', 
                'required' => false,
                'keys' => array(
                    array('fr', 'ckeditor', array('label' => 'Français', 'required' => false, 'attr' => array('class' => 'col-md-6'))),
                    array('en', 'ckeditor', array('label' => 'Anglais', 'required' => false, 'attr' => array('class' => 'col-md-6')))
                    )
                ))
            ->add('lang', 'choice', array(
                'label' => 'Langue', 
                'mapped' => null, 
                'choices' => array( 
                    'fr' => "Français", 
                    'en' => "Anglais" ), 
                'attr' => array( 
                    'class' => 'lang-switcher'
                 )
                 ))

            ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('date')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('edit', null, array('template' => 'ApplicationSonataAdminBundle:Widgets:edit_picto.html.twig'))
            //->addIdentifier('media', 'sonata_type_model_list', array('label' => 'Media', 'template' => 'ApplicationSonataAdminBundle:User:picture.html.twig'), array('link_parameters' => array('context' => 'page')))            
            ->add('title', null, array('editable' => true))
            ->add('contenu', null)
            ->add('date')
         ;
    }
}
?>