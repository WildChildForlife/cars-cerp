<?php 
namespace CERP\SiteBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use CERP\SiteBundle\Entity\Item;

class MenuAdmin extends Admin
{

    protected $baseRoutePattern = 'menu';
    public $supportsPreviewMode = true;


    //   Preview
    protected function configureShowFields(ShowMapper $showMapper)
    {
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper

            /*
             * The default option is to just display the value as text (for boolean this will be 1 or 0)
             */
            ->add('name')
            ->add('position');
            if ($this->isGranted('ROLE_ADMIN'))
            {
                $showMapper
                    ->add('template')
                    ->add('global');
            }
        $showMapper    
            ->add('actif')
            // ->add('items')

            ;

    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array('label' => 'Nom'))
            ->add('position', null, array('label' => 'Position'));
            if ($this->isGranted('ROLE_ADMIN'))
            {
                $formMapper->add('template', null, array('label' => 'Template'));
            }
        $formMapper    
            ->add('items', 'sonata_type_collection', array('label' => 'Items','by_reference' => false, 'cascade_validation' => true), array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ));
            if ($this->isGranted('ROLE_ADMIN'))
            {
                $formMapper->add('global', null, array('label' => 'Global', 'required' => false));
            }
        $formMapper
            ->add('actif', null, array('label' => 'Actif', 'required' => false))
        //     ->add('page', 'entity', array('label' => 'Page parente', 'class' => 'CERPSiteBundle:Page', 'empty_value' => 'Aucun', 'required' => false))
        //     ->add('media', 'sonata_type_model_list', array('label' => 'Media' , 'required' => false), array('link_parameters' => array('context' => 'page')))           
        //     ->add('contenuFR', 'textarea', array('label' => 'Contenu FR','attr' => array('class' => 'ckeditor')))
        //     ->add('contenuEN', 'textarea', array('label' => 'Contenu EN','attr' => array('class' => 'ckeditor')))
        //     ->add('category', null, array('empty_value' => 'Aucun'))
        ;

  
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('position');
            if ($this->isGranted('ROLE_ADMIN'))
            {
                $datagridMapper
                    ->add('template')
                    ->add('global');
            }
        $datagridMapper
            ->add('actif')
            // ->add('items')
        //     ->add('contenuFR','',array('label' => 'Contenu FR'))
        //     ->add('contenuEN','',array('label' => 'Contenu EN'))
        //     ->add('category')
        //     ->add('description')
        //     ->add('keywords')
            

        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('edit', null, array('template' => 'ApplicationSonataAdminBundle:Widgets:edit_picto.html.twig'))
            //->addIdentifier('media', 'sonata_type_model_list', array('label' => 'Media', 'template' => 'ApplicationSonataAdminBundle:User:picture.html.twig'), array('link_parameters' => array('context' => 'page')))
            ->addIdentifier('name', null)
            ->add('position', null, array('editable' => false));
            if ($this->isGranted('ROLE_ADMIN'))
            {
                $listMapper
                    ->add('template', null, array('editable' => true))
                    ->add('global', null, array('editable' => true));
            }
        $listMapper    
            ->add('actif', null, array('editable' => true));
    }


    public function getNewInstance()
    {
        $oMenu = parent::getNewInstance();
        $oItem = new Item();
        $oMenu->addItem($oItem);

        return $oMenu;
    }
}
?>