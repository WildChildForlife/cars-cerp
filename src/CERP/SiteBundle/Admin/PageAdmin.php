<?php 
namespace CERP\SiteBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use CERP\SiteBundle\Entity\Block;

class PageAdmin extends Admin
{

    protected $baseRoutePattern = 'page';
    protected $maxPageLinks = 1;
    protected $perPageOptions = array('500');
    protected $maxPerPage = 500;
    
    protected $datagridValues = array(
        '_per_page'   => 500,
        // display the first page (default = 1)
        '_page' => 1,
        // reverse order (default = 'ASC')
        //'_sort_order' => 'ASC',
        // name of the ordered field (default = the model's id field, if any)
        //'_sort_by' => 'page_parente.name',
    );
    public $supportsPreviewMode = false;

    protected $listModes = array(
        'list' => array(
            'class' => 'fa fa-list fa-fw',
        )
    );

    public function getTemplate($sName)
    {
        switch ($sName) {
            case 'list':
                return 'CERPSiteBundle:Admin:page_list.html.twig';
                break;
 
            default:
                return parent::getTemplate($sName);
                break;
        }
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper

            /*
             * The default option is to just display the value as text (for boolean this will be 1 or 0)
             */
            ->add('name');
            if ($this->isGranted('ROLE_ADMIN'))
            {
                $showMapper->add('route');
            }
        $showMapper    
            ->add('title')
            ->add('media', null, array('label' => 'Photo'))
            ->add('page', null, array('label' => 'Page parente'))
            ->add('actif')
            ->add('bandeau')
            ;

    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->with('General', array(
                    'class' => 'col-md-6',
                ));

                $formMapper->add('id', null , array('label' => 'id', 'read_only' => true));
        $formMapper->add('name', 'text', array('label' => 'Nom'));
            if ($this->isGranted('ROLE_ADMIN'))
            {
                $formMapper->add('route', 'list_route_form', array('label' => 'Route', 'required' => false, 'attr' => array( 'class' => 'route-item' )));
            }
         $formMapper   
            ->add('page', 'entity', array('label' => 'Page parente', 'class' => 'CERPSiteBundle:Page', 'empty_value' => 'Aucune', 'required' => false))
            ->add('media', 'sonata_type_model_list', array('label' => 'Media' , 'required' => false), array('link_parameters' => array('context' => 'General')))           
            ->add('bandeau', 'sonata_type_model_list', array('label' => 'Bandeaux' , 'required' => false), array('link_parameters' => array('context' => 'Bandeaux')))           
            ->add('actif', null, array('label' => 'Actif', 'required' => false))
            
        ->end()
        ->with('SEO', array(
                    'class' => 'col-md-6',
                ))
            ->add('translations', 'a2lix_translations_gedmo', array(
                'label' => false,
                'translatable_class' => "CERP\SiteBundle\Entity\Page",
                'fields' => array( 
                        'title' => array(                   
                            'field_type' => null,                
                            'label' => 'Description.',          
                            'locale_options' => array(          
                                'fr' => array(
                                    'label' => 'Titre'          
                                ),
                                'en' => array(
                                    'label' => 'Title'          
                                )
                            )
                        ),                     
                        'description' => array(                 
                            'field_type' => null,               
                            'label' => 'Description.',
                            'required' => false,          
                            'locale_options' => array(          
                                'fr' => array(
                                    'label' => 'Description'    
                                ),
                                'en' => array(
                                    'label' => 'Description'    
                                )
                            )
                        ),                                            
                        'keywords' => array(                  
                            'field_type' => null,
                            'required' => false,              
                            'label' => 'Description.',          
                            'locale_options' => array(          
                                'fr' => array(
                                    'label' => 'Mots clés'      
                                ),
                                'en' => array(
                                    'label' => 'Keywords'       
                                )
                            )
                        )
                    )
                ))
        ->end()
         ->with('Analytics', array(
                    'class' => 'col-md-6',
                ))
            ->add('analytics', null, array('label' => false, 'required' => false))
        ->end()


            /*->add('title', 'text', array('label' => 'Title', 'required' => false))
            ->add('description', 'text', array('label' => 'Description' ,'required' => false))
            ->add('keywords', 'text', array('label' => 'Mots clé', 'required' => false))*/
        ->end()
        ->with('Blocks', array(
                    'class' => 'col-md-12',
                ))
             ->add('blocks', 'sonata_type_collection', array(
                    'by_reference'       => false,
                    'cascade_validation' => true
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
        ->end()

        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
        if ($this->isGranted('ROLE_ADMIN'))
            $datagridMapper->add('route', null , array('label' => 'route'));
        $datagridMapper
            ->add('title')
            //->add('media')
            //->add('category')
            ->add('description')
            ->add('keywords')
            ->add('actif')
            ->add('page_parente')
            

        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('edit', null, array('template' => 'ApplicationSonataAdminBundle:Widgets:edit_picto.html.twig'))
            ->addIdentifier('id', null)
            ->addIdentifier('name', null)
            ->add('media', 'sonata_type_model_list', array('label' => 'Photo', 'template' => 'ApplicationSonataAdminBundle:User:picture.html.twig'), array('link_parameters' => array('context' => 'page'))) 
            ->add('bandeau', 'sonata_type_model_list', array('label' => 'Bandeau', 'template' => 'ApplicationSonataAdminBundle:User:picture.html.twig'), array('link_parameters' => array('context' => 'Bandeaux')))   ;  
            if ($this->isGranted('ROLE_ADMIN'))
            {
                $listMapper->add('route', null, array('label' => 'Route', 'template' => 'ApplicationSonataAdminBundle:Widgets:transform_route.html.twig'));
            }       
            
        $listMapper
            ->add('title', null, array('editable' => true))
            ->add('description', null, array('editable' => true))
            //->add('keywords', null, array('editable' => true))
            //->add('page', null, array('label' => 'Page parente', 'editable' => true))
            // ->addIdentifier('contenu FR', 'textarea', array('label' => 'contenu FR', 'template' => 'ApplicationSonataAdminBundle:CRUD::base_list_field.html.twig'))
            // ->addIdentifier('contenu EN', 'textarea', array('label' => 'contenu EN', 'template' => 'ApplicationSonataAdminBundle:CRUD::base_list_field.html.twig'))
            //->add('category', null, array('label' => 'Catégorie'))
            ->add('actif', null, array('editable' => true))

        ;
    }

    /**
     * {@inheritdoc}
     */
    // public function getNewInstance()
    // {
    //     $oPage = parent::getNewInstance();
    //     $oBlock = new Block();

    //     $oPage->addBlock($oBlock);

    //     return $oPage;
    // }



    public function getNewInstance()
    {
        $oPage = parent::getNewInstance();
        $oBlock = new Block();
        $oPage->addBlock($oBlock);

        return $oPage;
    }

    //     public function getNewInstance()
    // {
    //     $oPage = parent::getNewInstance();
    //     $oBlock1 = new Block();
    //     $oBlock2 = new Block();
    //     $oLocal1 = new Local();
    //     $oLocal2 = new Local();
    //     $oLocal1->getByName('Français');
    //     $oLocal2->getByName('Anglais');

    //     $oPage->addBlock($oBlock1);
    //     $oPage->addBlock($oBlock2);
    //     return $oPage;
    // }



}
?>