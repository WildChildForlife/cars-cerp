<?php 
namespace CERP\SiteBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use FOS\UserBundle\Model\UserManagerInterface;

class UserAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
                ->add('username', null, array('label' => 'Nom d\'utilisateur'))
                ->add('email', 'email', array('label' => 'Email'))
                ->add('plainPassword', 'text', array('label' => 'Mot de passe'))
                ->add('first_name', null, array('label' => 'Prénom'))
                // ->add('middle_name')
                ->add('last_name', null, array('label' => 'Nom'))
                ->add('birthday', null, array('label' => 'Date de naissance'))
                // ->add('band') 
            ->end()
            ->with('Groups')
                ->add('groups', 'sonata_type_model_hidden', array('required' => false))
            ->end()
            ->with('Management')
                ->add('roles', 'sonata_security_roles', array( 'multiple' => true))
                ->add('locked', null, array('required' => false))
                // ->add('expired', null, array('required' => false))
                ->add('enabled', null, array('required' => false))
                ->add('credentialsExpired', null, array('required' => false))
            ->end()
        ;

        
    }

    public function preUpdate($user)
    {
        $this->getUserManager()->updateCanonicalFields($user);
        $this->getUserManager()->updatePassword($user);
    }

    public function setUserManager(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username')
            ->add('email')
            ->add('enabled')
            ->add('roles')
            ->add('first_name')
            ->add('middle_name')
            ->add('last_name')
            ->add('birthday', 'doctrine_orm_date_range', array('input_type' => 'timestamp'))
            ->add('band', null, array(), null, array('expanded' => false, 'multiple' => true))
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('edit', null, array('template' => 'ApplicationSonataAdminBundle:Widgets:edit_picto.html.twig'))
            ->add('username', null, array('editable' => true))
            ->addIdentifier('email', null, array('editable' => true))
            ->add('enabled', null, array('editable' => true))
            ->add('roles', null)
            ->add('first_name', null, array('editable' => true))
            ->add('middle_name', null, array('editable' => true))
            ->add('last_name', null, array('editable' => true))
            ->add('birthday', null, array('editable' => true))
            ->add('band', null, array('editable' => true))
        ;
    }
}
?>