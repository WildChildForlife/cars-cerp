<?php 

namespace CERP\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractTranslation;

use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Gedmo\Translatable\Mapping\Event\TranslatableAdapter;
use Gedmo\Tool\Wrapper\AbstractWrapper;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * @ORM\Table(name="page_translations",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_idx", columns={
 *         "locale", "object_id", "field", "foreign_key"
 *     })})
 * @ORM\Entity(repositoryClass="Gedmo\Translatable\Entity\Repository\TranslationRepository")
 */
class PageTranslation extends AbstractTranslation
{
	/**
     * Convinient constructor
     *
     * @param string $locale
     * @param string $field
     * @param string $content
     */
    public function __construct($locale = null, $field = null, $content = null)
    {
        $this->setLocale($locale);
        $this->setField($field);
        $this->setContent($content);
        $this->setObjectClass(get_class($this));
    }
    /**
     * @ORM\ManyToOne(targetEntity="CERP\SiteBundle\Entity\Page", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;

    /**
     * @var string $foreignKey
     *
     * @ORM\Column(name="foreign_key", type="string", length=64, nullable=true)
     */
    protected $foreignKey;

    
    /**
     * Set object
     *
     * @param \CERP\SiteBundle\Entity\Page $object
     * @return PageTranslation
     */
    public function setObject(\CERP\SiteBundle\Entity\Page $object = null)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * Get object
     *
     * @return \CERP\SiteBundle\Entity\Page 
     */
    public function getObject()
    {
        return $this->object;
    }

}
