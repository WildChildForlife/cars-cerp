<?php

namespace CERP\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CERP\SiteBundle\Entity\Repository\BlockRepository")
 * @ORM\Table(name="block")
 */
class Block
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(name="contenu", type="array", nullable=true) */
    protected $contenu = array('fr' => '', 'en' => '');

    /** @ORM\Column(name="date", type="date") */
    protected $date;

    /** @ORM\Column(name="title", type="string", length=255, nullable=true) */
    protected $title;

    /** @ORM\Column(name="position", type="integer", nullable=true) */
    protected $position;

    /** @ORM\Column(name="groupe", type="string", nullable=true) */
    protected $groupe;

    /** @ORM\Column(name="actif", type="boolean", nullable=true) */
    protected $actif;

    /** @ORM\ManyToOne(targetEntity="Page", inversedBy="blocks") */
    protected $page;



    public function __toString() 
    {        
        return $this->contenu;
    }

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Block
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Block
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set page
     *
     * @param \CERP\SiteBundle\Entity\Page $page
     * @return Block
     */
    public function setPage(\CERP\SiteBundle\Entity\Page $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return \CERP\SiteBundle\Entity\Page 
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Block
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     * @return Block
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean 
     */
    public function getActif()
    {
        return $this->actif;
    }


    /**
     * Set contenu
     *
     * @param array $contenu
     * @return Block
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return array 
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    public function getContenuByLang($sLang)
    {
        return ( isset($this->contenu[$sLang]) && !empty($this->contenu[$sLang]) ) ? $this->contenu[$sLang] : $this->contenu[0];
    }


    /**
     * Set groupe
     *
     * @param string $groupe
     * @return Block
     */
    public function setGroupe($groupe)
    {
        $this->groupe = $groupe;

        return $this;
    }

    /**
     * Get groupe
     *
     * @return string 
     */
    public function getGroupe()
    {
        return $this->groupe;
    }
}
