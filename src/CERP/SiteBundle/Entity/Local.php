<?php

namespace CERP\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CERP\SiteBundle\Entity\Repository\LocalRepository")
 * @ORM\Table(name="local")
 */
class Local
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(name="name", type="string", length=255) */
    protected $name;

    /** @ORM\Column(name="position", type="integer", nullable=true) */
    protected $position;

    /** @ORM\Column(name="actif", type="boolean", nullable=true) */
    protected $actif;

    /**
     * @ORM\OneToMany(targetEntity="Block", mappedBy="local")
     */
    protected $blocks;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString() 
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Local
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Local
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     * @return Local
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean 
     */
    public function getActif()
    {
        return $this->actif;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->blocks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add blocks
     *
     * @param \CERP\SiteBundle\Entity\Block $blocks
     * @return Local
     */
    public function addBlock(\CERP\SiteBundle\Entity\Block $blocks)
    {
        $this->blocks[] = $blocks;

        return $this;
    }

    /**
     * Remove blocks
     *
     * @param \CERP\SiteBundle\Entity\Block $blocks
     */
    public function removeBlock(\CERP\SiteBundle\Entity\Block $blocks)
    {
        $this->blocks->removeElement($blocks);
    }

    /**
     * Get blocks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBlocks()
    {
        return $this->blocks;
    }
}
