<?php

namespace CERP\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CERP\SiteBundle\Entity\Repository\SiteConfRepository")
 * @ORM\Table(name="siteconf")
 */
class SiteConf
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(name="contactAdmin", type="string", length=255, nullable=true) */
    protected $contactAdmin;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contactAdmin
     *
     * @param string $contactAdmin
     * @return SiteConf
     */
    public function setContactAdmin($contactAdmin)
    {
        $this->contactAdmin = $contactAdmin;

        return $this;
    }

    /**
     * Get contactAdmin
     *
     * @return string 
     */
    public function getContactAdmin()
    {
        return $this->contactAdmin;
    }
}
