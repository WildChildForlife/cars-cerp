<?php

namespace CERP\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CERP\SiteBundle\Entity\Item;

/**
 * @ORM\Entity(repositoryClass="CERP\SiteBundle\Entity\Repository\MenuRepository")
 * @ORM\Table(name="menu")
 */
class Menu
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(name="name", type="string", length=255) */
    protected $name;


    /** @ORM\Column(name="template", type="string", length=255) */
    protected $template;

    /** @ORM\Column(name="position", type="integer") */
    protected $position;

    /** @ORM\Column(name="actif", type="boolean") */
    protected $actif = false;

    /** @ORM\Column(name="global", type="boolean") */
    protected $global;
    /**
    * @ORM\ManyToMany(targetEntity="Item", cascade="all")
    * @ORM\OrderBy({"position" = "ASC"})
    */
    private $items;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Menu
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Menu
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set template
     *
     * @param string $template
     * @return Menu
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return string 
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     * @return Menu
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean 
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Add items
     *
     * @param \CERP\SiteBundle\Entity\Item $items
     * @return Menu
     */
    public function addItem(\CERP\SiteBundle\Entity\Item $items)
    {
        $this->items[] = $items;

        return $this;
    }

    /**
     * Remove items
     *
     * @param \CERP\SiteBundle\Entity\Item $items
     */
    public function removeItem(\CERP\SiteBundle\Entity\Item $items)
    {
        $this->items->removeElement($items);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set global
     *
     * @param boolean $global
     * @return Menu
     */
    public function setGlobal($global)
    {
        $this->global = $global;

        return $this;
    }

    /**
     * Get global
     *
     * @return boolean 
     */
    public function getGlobal()
    {
        return $this->global;
    }


    // /**
    //  * Get items
    //  *
    //  * @return \Doctrine\Common\Collections\Collection 
    //  */
    // public function getOrderedItems()
    // {
    //    return $em->getRepository('CERPSiteBundle:Item')->findBy(array(), array('position'=>'ASC'));

    // }

}
