<?php

namespace CERP\SiteBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * SiteConfRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SiteConfRepository extends EntityRepository
{

	public function getOneOrNullResult()
	{
		return $this->_em->createQuery('SELECT sc
											FROM CERPSiteBundle:SiteConf sc
											')							
							->getOneOrNullResult();	
	}
}
