<?php

namespace CERP\SiteBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

use CERP\SiteBundle\Entity\Repository\TranslatableRepository;
 

/**
 * PathologieRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PathologieRepository extends TranslatableRepository implements ContainerAwareInterface
{
	 /**
     * @var ContainerInterface
     */
	    private $container;

	     public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

	public function getPathologieByParams($sPatho, $sMere1, $sMere2, $sMere3)
	{


		$oQuery = $this->_em->createQuery('SELECT p
											FROM CERPSiteBundle:Pathologie p
											WHERE p.slug = :slug AND p.page_mere1 = :page_mere1 AND p.page_mere2 = :page_mere2 AND p.page_mere3 = :page_mere3
									     	')
							->setParameters(array(
													'slug' => $sPatho,
													'page_mere1' => $sMere1,
													'page_mere2' => $sMere2,
													'page_mere3' => $sMere3
												 )
											);

		// $oPathologie = $oQuery->getSingleResult();
		$oPathologie = $oQuery->getOneOrNullResult();

		
		



		// Si la langue par défault (table mère) n'est pas égale à la langue demandée
		if ( $this->container->get('request')->getLocale() !== $this->container->getParameter('locale') ) 

		{
			// Chercher les traductions
			$oPathologieTranslation = $this->_em->getRepository('CERPSiteBundle:PathologieTranslation')->_em

			->createQuery('SELECT pt.field, pt.content 
							FROM CERPSiteBundle:PathologieTranslation pt 
							WHERE pt.object = :object_id 
							AND pt.locale = :locale
						')
			->setParameters(array(
					'object_id' => $oPathologie->getId(),
					'locale' => $this->container->get('request')->getLocale(),
			))->getScalarResult();


			// Si des traductions ont été trouvées
			if ( count($oPathologieTranslation) )
			{
				// Remplacer ces traductions dans le résultat initial
				foreach ($oPathologieTranslation as $iKey => $sValue) 
				{
					if ( isset($sValue['field']) && isset($sValue['content']) )	
					{
						$sSetter = 'set' . ucfirst($sValue['field']);
						$oPathologie->$sSetter($sValue['content']);
					}
				}
			}
		}


// ladybug_dump($oPathologie);
// die();

		return $oPathologie;
	}




}
