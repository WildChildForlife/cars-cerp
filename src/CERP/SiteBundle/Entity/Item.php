<?php

namespace CERP\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CERP\SiteBundle\Entity\Page;

use Gedmo\Mapping\Annotation as Gedmo;

use CERP\SiteBundle\Entity\ItemTranslation;

/**
 * @ORM\Entity(repositoryClass="CERP\SiteBundle\Entity\Repository\ItemRepository")
 * @ORM\Table(name="item")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\TranslationEntity(class="CERP\SiteBundle\Entity\ItemTranslation")
 */
class Item
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @Gedmo\Translatable
    * @ORM\Column(name="nom", type="string", length=255) 
    */
    protected $nom;

    /** @ORM\Column(name="route", type="string", length=255, nullable=true) */
    protected $route;

    /** @ORM\Column(name="class", type="string", length=255, nullable=true) */
    protected $class;

    /** @ORM\Column(name="position", type="integer", nullable=true) */
    protected $position;

    /** @ORM\Column(name="blank", type="boolean", nullable=true) */
    protected $blank = false;

    /**
    * @ORM\ManyToOne(targetEntity="Page", cascade="all")
    */
    private $page;

    /**
     * Post locale
     * Used locale to override Translation listener's locale
     *
     * @Gedmo\Locale
     */
    protected $locale;

    /**
     * @ORM\OneToMany(targetEntity="CERP\SiteBundle\Entity\ItemTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;




    public function __toString()
    {
        return $this->nom;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Item
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set blank
     *
     * @param boolean $blank
     * @return Item
     */
    public function setBlank($blank)
    {
        $this->blank = $blank;

        return $this;
    }

    /**
     * Get blank
     *
     * @return boolean 
     */
    public function getBlank()
    {
        return $this->blank;
    }



    /**
     * Set route
     *
     * @param string $route
     * @return Item
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return string 
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set class
     *
     * @param string $class
     * @return Item
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return string 
     */
    public function getClass()
    {
        return $this->class;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }
   

    /**
     * Set page
     *
     * @param \CERP\SiteBundle\Entity\Page $page
     * @return Item
     */
    public function setPage(\CERP\SiteBundle\Entity\Page $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return \CERP\SiteBundle\Entity\Page 
     */
    public function getPage()
    {
        return $this->page;
    }

    static public function getTranslationEntityClass() { return "CERP\SiteBundle\Entity\ItemTranslation"; }

    /**
     * Sets translatable locale
     *
     * @param string $locale
     */
    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }


    public function addTranslation(ItemTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    public function removeTranslation(ItemTranslation $t)
    {
        $this->translations->removeElement($t);
    }

    /**
     * Set translations
     *
     * @param ArrayCollection $translations
     * @return Product
     */
    public function setTranslations($translations)
    {
        foreach ($translations as $translation) {
            $translation->setObject($this);
        }

        $this->translations = $translations;
        return $this;
    }

    /**
     * Get translations
     *
     * @return ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }
}
