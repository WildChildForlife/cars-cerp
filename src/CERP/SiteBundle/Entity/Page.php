<?php

namespace CERP\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;

use CERP\SiteBundle\Entity\PageTranslation;

use Gedmo\Translatable\Translatable;

/**
 * @ORM\Entity(repositoryClass="CERP\SiteBundle\Entity\Repository\PageRepository")
 * @ORM\Table(name="page")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\TranslationEntity(class="CERP\SiteBundle\Entity\PageTranslation")
 */
class Page implements Translatable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @Gedmo\Translatable
    * @ORM\Column(name="title", type="string", length=255, nullable=true) 
    */
    private $title;

    /**
    * @Gedmo\Translatable
    * @ORM\Column(name="description", type="string", length=255, nullable=true) 
    */
    private $description;

    /**
    * @Gedmo\Translatable
    * @ORM\Column(name="keywords", type="string", length=255, nullable=true) 
    */
    private $keywords;

    /**
    * @ORM\Column(name="analytics", type="string", length=255, nullable=true) 
    */
    private $analytics;


     /** @ORM\Column(name="actif", type="boolean", nullable=true) */
    protected $actif = false;

    /** @ORM\Column(name="route", type="string", length=255, nullable=true) */
    protected $route;

    /** @ORM\Column(name="name", type="string", length=255, nullable=true) */
    protected $name;


    /** @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade="all") */
    protected $media;

    /**
    * @ORM\ManyToOne(targetEntity="Page")
    */
    private $page_parente;

    public $pages_enfant;

    /* @ORM\ManyToOne(targetEntity="Application\Sonata\ClassificationBundle\Entity\Category", cascade="all") */    
    //protected $category;

    /**
     * @ORM\OneToMany(targetEntity="Block", mappedBy="page", cascade="all", orphanRemoval=true)
     */
    protected $blocks;

    /**
     * Post locale
     * Used locale to override Translation listener's locale
     *
     * @Gedmo\Locale
     */
    protected $locale;

    /**
     * @ORM\OneToMany(targetEntity="CERP\SiteBundle\Entity\PageTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /** @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade="all") */
    protected $bandeau;
  


    public function __toString() {
        return $this->getName();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     * @return Page
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean 
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Page
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get locale
     *
     * @return string 
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return Page
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }


    /**
     * Set media
     *
     * @param string $media
     * @return Page
     */
    public function setMedia(\Application\Sonata\MediaBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

     /**
     * Get media
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media 
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set page_parente
     *
     * @param \CERP\SiteBundle\Entity\Page $pageParente
     * @return Page
     */
    public function setPageParente(\CERP\SiteBundle\Entity\Page $pageParente = null)
    {
        $this->page_parente = $pageParente;

        return $this;
    }

    /**
     * Get page_parente
     *
     * @return \CERP\SiteBundle\Entity\Page 
     */
    public function getPageParente()
    {
        return $this->page_parente;
    }

    /**
     * Get page_parente
     *
     * @return \CERP\SiteBundle\Entity\Page 
     */
    public function getPage()
    {
        return $this->page_parente;
    }

   /**
     * Set page_parente
     *
     * @param \CERP\SiteBundle\Entity\Page $pageParente
     * @return Page
     */
    public function setPage(\CERP\SiteBundle\Entity\Page $pageParente = null)
    {
        $this->page_parente = $pageParente;

        return $this;
    }


    /*
     * Set category
     *
     * @param \Application\Sonata\ClassificationBundle\Entity\Category $category
     * @return Page
     */
    // public function setCategory(\Application\Sonata\ClassificationBundle\Entity\Category $category = null)
    // {
    //     $this->category = $category;

    //     return $this;
    // }

    /*
     * Get category
     *
     * @return \Application\Sonata\ClassificationBundle\Entity\Category 
     */
    // public function getCategory()
    // {
    //     return $this->category;
    // }


    /**
     * Set route
     *
     * @param string $route
     * @return Page
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return string 
     */
    public function getRoute()
    {
        return $this->route;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->blocks = new \Doctrine\Common\Collections\ArrayCollection();
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add blocks
     *
     * @param \CERP\SiteBundle\Entity\Block $blocks
     * @return Page
     */
    public function addBlock(\CERP\SiteBundle\Entity\Block $blocks)
    {
        //$this->blocks[] = $blocks;
        $blocks->setPage($this);
        $this->blocks->add($blocks);

        return $this;
    }

    /**
     * Remove blocks
     *
     * @param \CERP\SiteBundle\Entity\Block $blocks
     */
    public function removeBlock(\CERP\SiteBundle\Entity\Block $blocks)
    {
        $this->blocks->removeElement($blocks);
    }

    /**
     * Get blocks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBlocks()
    {
        return $this->blocks;
    }

    /**
     * Get blocks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrderedBlocks()
    {
        $aBlocks = array();

        if ( count($this->blocks ) > 0 )
        foreach ($this->blocks as $iKey => $sValue)
        {                 
            $aBlocks[$sValue->getTitle()] = $sValue;
        }
        return $aBlocks;
    }

    /**
     * Sets translatable locale
     *
     * @param string $locale
     */
    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }


    public function addTranslation(PageTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    public function removeTranslation(PageTranslation $t)
    {
        $this->translations->removeElement($t);
    }

    /**
     * Set translations
     *
     * @param ArrayCollection $translations
     * @return Product
     */
    public function setTranslations($translations)
    {
        foreach ($translations as $translation) {
            $translation->setObject($this);
        }

        $this->translations = $translations;
        return $this;
    }

    /**
     * Get translations
     *
     * @return ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Page
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /* Add item
     *
     * @param \CERP\SiteBundle\Entity\Item $item
     * @return Page
     */
    public function addItem(\CERP\SiteBundle\Entity\Item $item)
    {
        $this->item[] = $item;
    }

     /* Remove item
     *
     * @param \CERP\SiteBundle\Entity\Item $item
     */
    public function removeItem(\CERP\SiteBundle\Entity\Item $item)
    {
        $this->item->removeElement($item);
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Page
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    static public function getTranslationEntityClass() { return "CERP\SiteBundle\Entity\PageTranslation"; }


    /**
     * Set analytics
     *
     * @param string $analytics
     * @return Page
     */
    public function setAnalytics($analytics)
    {
        $this->analytics = $analytics;

        return $this;
    }

    /**
     * Get analytics
     *
     * @return string 
     */
    public function getAnalytics()
    {
        return $this->analytics;
    }

    /**
     * Set bandeau
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $bandeau
     * @return Page
     */
    public function setBandeau(\Application\Sonata\MediaBundle\Entity\Media $bandeau = null)
    {
        $this->bandeau = $bandeau;

        return $this;
    }

    /**
     * Get bandeau
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media 
     */
    public function getBandeau()
    {
        return $this->bandeau;
    }
}
