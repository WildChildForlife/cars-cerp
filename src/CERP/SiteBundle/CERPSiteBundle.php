<?php

namespace CERP\SiteBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CERPSiteBundle extends Bundle
{
	public function getParent()
    {
        return 'SonataDoctrineORMAdminBundle';
    }
}
