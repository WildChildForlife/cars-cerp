<?php

namespace CERP\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Youtubedl\Youtubedl;
use PHPVideoToolkit\Video;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Session\Session;

use CERP\SiteBundle\Entity\Page;
use CERP\SiteBundle\Entity\SiteConf;

class VitrineController extends Controller
{
    public function indexAction()
    {
        $oPage = $this->getDoctrine()->getRepository('CERPSiteBundle:Page')->getPageActifByParam('name', 'Index');            
        return $this->render('CERPSiteBundle:Pages:index.html.twig', array("oPage" => $oPage));        
    }

    public function contactAction()
    {
        $oPage = $this->getDoctrine()->getRepository('CERPSiteBundle:Page')->getPageActifByParam('name', 'Contact');
        $oSession = new Session();
        $oContactType = new ContactType();
        $oContact = new Contact();
        $oForm = $this->createForm($oContactType,$oContact);
        $oForm->handleRequest($this->getRequest());

        if ($oForm->isValid())
        {
            $oSiteConf = $this->getDoctrine()->getRepository('CERPSiteBundle:SiteConf')->getOneOrNullResult();

            $em = $this->getDoctrine()->getManager();
            $em->persist($oContact);
            $em->flush();


             $oMessage = \Swift_Message::newInstance()
            ->setSubject($oContact->getObjet())
            ->setFrom($oContact->getEmail())
            ->setTo($oSiteConf->getContactAdmin())
            ->setBody($oContact->getMessage())
            ;

             $this->get('mailer')->send($oMessage);
            //Envoyer un mail de notification

             $oSession->getFlashBag()->add('contact-success', 'contact.success');
        }
            return $this->render('CERPSiteBundle:Pages:contact.html.twig', array("oPage" => $oPage, "oForm" => $oForm->createView()));        
    }



    public function cgvAction()
    {
        $oPage = $this->getDoctrine()->getRepository('CERPSiteBundle:Page')->getPageActifByParam('name', 'CGV');
        
            return $this->render('CERPSiteBundle:Pages:cgv.html.twig', array("oPage" => $oPage));        
    }

    public function mentionslegalesAction()
    {
        $oPage = $this->getDoctrine()->getRepository('CERPSiteBundle:Page')->getPageActifByParam('name', 'Mentions légales');
        
            return $this->render('CERPSiteBundle:Pages:mentionslegales.html.twig', array("oPage" => $oPage));        
    }
     
    /**
	* @Secure(roles="ROLE_USER, ROLE_ADMIN")
	*/
    public function landingAction()
    {
        return $this->render('CERPSiteBundle:Pages:landing.html.twig');
    }

    

    public function GetMediaAction()
    {
        $oMedia = $this->getDoctrine()->getRepository('ApplicationSonataMediaBundle:Media')->findAll();


         return $this->render('CERPSiteBundle:Pages:getmedia.html.twig', array(
            "oMedia" => $oMedia
        ));

    }

    public function pathologieDedieAction($sMembre = null, $sPatho = null)
    {        
        if ($sMembre == null && $sPatho == null ) { throw $this->createNotFoundException("Cette pathologie n'existe pas"); }

        $oPathologie = $this->getDoctrine()->getRepository('CERPSiteBundle:Pathologie')->getPathologieByParams($sPatho, 'Podologie', 'Pathologies', $sMembre);

        $oPage = $this->getDoctrine()->getRepository('CERPSiteBundle:Page')->getPageActifByParam('name', 'Page dédié pathologie');

        if (!$oPathologie) { throw $this->createNotFoundException("Cette pathologie n'existe pas"); }
                
        return $this->render('CERPSiteBundle:Pages:page-dedie.html.twig', array("oPathologie" => $oPathologie, "PageMere" => "Podologie", "oPage" => $oPage));        
    }



    public function maladiesSpecifiquesDedieAction($sMembre = null, $sPatho = null)
    {        
        if ($sMembre == null && $sPatho == null ) { throw $this->createNotFoundException("Cette pathologie n'existe pas"); }

        $oPathologie = $this->getDoctrine()->getRepository('CERPSiteBundle:Pathologie')->getPathologieByParams($sPatho, 'Podologie', 'Maladies spécifiques', $sMembre);

        $oPage = $this->getDoctrine()->getRepository('CERPSiteBundle:Page')->getPageActifByParam('name', 'Page dédié pathologie');

         if (!$oPathologie) { throw $this->createNotFoundException("Cette pathologie n'existe pas"); }
        
            return $this->render('CERPSiteBundle:Pages:page-dedie.html.twig', array("oPathologie" => $oPathologie, "PageMere" => "Podologie", "oPage" => $oPage));        
    }



    public function guidePathoDedieAction($sMembre = null, $sPatho = null)
    {
        if ($sMembre == null && $sPatho == null ) { throw $this->createNotFoundException("Cette pathologie n'existe pas"); }

        $oPathologie = $this->getDoctrine()->getRepository('CERPSiteBundle:Pathologie')->getPathologieByParams($sPatho, 'Podz Pro', 'Pathologies', $sMembre);

        $oPage = $this->getDoctrine()->getRepository('CERPSiteBundle:Page')->getPageActifByParam('name', 'Page dedie Podz Pro');

        if (!$oPathologie) { throw $this->createNotFoundException("Cette pathologie n'existe pas"); }
        
            return $this->render('CERPSiteBundle:Pages:page-dedie.html.twig', array("oPathologie" => $oPathologie, "PageMere" => "Podz Professionnel", "oPage" => $oPage));        
    }



    public function newsletterAction(Request $oRequest)
    {
       
        $oNewsLetterType = new NewsLetterType();
        $oNewsLetter = new NewsLetter();
        $oForm = $this->createForm($oNewsLetterType,$oNewsLetter);

        $oForm->handleRequest($this->getRequest());

        return $this->render('CERPSiteBundle:Portions:newsletter.html.twig', array("oForm" => $oForm->createView(), 'request'=>$oRequest));
        
    }

    public function newsletterHandlerAction(Request $oRequest)
    {
       
        $oNewsLetterType = new NewsLetterType();
        $oNewsLetter = new NewsLetter();
        $oForm = $this->createForm($oNewsLetterType,$oNewsLetter);

        $oForm->handleRequest($this->getRequest());

        if ($oForm->isValid())
        {
            $oManager = $this->getDoctrine()->getManager();
            $oManager->persist($oNewsLetter);
            $oManager->flush();

            $oSession = new Session();

            $oSession->getFlashBag()->add('newsletter-success', 'newsletter.success');           
        }


        $aParams = $this->getRefererParams();

        $aFinalParams = [];

        if ( count($aParams) > 3 )
        {
            $aFinalParams = $aParams;
            unset($aFinalParams['_controller']);
            unset($aFinalParams['_locale']);
            unset($aFinalParams['_route']);
        }

       
        
        return $this->redirect($this->generateUrl($aParams['_route'], $aFinalParams));
        
    }

    public function praticienAction()
    {
        $oPage = $this->getDoctrine()->getRepository('CERPSiteBundle:Page')->getPageActifByParam('name', 'Praticien');

        return $this->render('CERPSiteBundle:Pages:trouver-praticien.html.twig', array("oPage" => $oPage));        
    }
}
