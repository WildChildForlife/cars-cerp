$(document).ready(function(){
    if( $('#map').length ){
       $('#map').gmap3({
          map:{
              options:{
                center:[48.861693,2.3412025],
                zoom: 17,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeId: "monTheme",
                scrollwheel : false,
                mapTypeControl: false,
                panControl: false,
                zoomControl: false,
                scaleControl: false,
                streetViewControl: false,
                rotateControl: false,
                rotateControlOptions: false,
                overviewMapControl: false,
                OverviewMapControlOptions: false,
                draggable: false,
            }
            },
          styledmaptype:{
            id: "monTheme",
                options:{
                name: "monTheme"
            },
            styles: [
                        {
                            featureType: 'all',
                            stylers: [
                                {saturation: -200},
                                {gamma: 0.80}
                            ]
                        },
                        
                        {
                            featureType: 'road.local',
                            stylers: [
                                {hue: "#5F5F5F"},
                                {visibility: 'simplified'}
                            ]
                        },
                    ]
            },
             marker:{
                values:[
                    //{latLng:[48.861693,2.3412025], address:"3 Rue du Louvre, 75001 Paris", options:{icon: "img/global/marker.png"}},
                ],
                options:{
                  draggable: false
                }
              }
        }); // map 
    }
	


    /******* Page Historique ******/
    if( $('#timeline').length ){
        $(function(){
            $().timelinr({
                arrowKeys: 'true'
            })
        });
    }


    /****** Page Equipe *******/
    if( $('.page-equipe').length ){
        var owl = $('#owl-team');
        owl.owlCarousel({
            items : 3,
            autoPlay:false,
            loop:true,
            navigation:true, 
            onTranslated: onTranslatedEvent
        });

        function onTranslatedEvent(event)
        {
            $(".bloc-team").stop().animate({ opacity:0 }, function(){
                $(this).css('display', 'none');
                $("#owl-team .owl-item .slide-owl").removeClass('current-item');
                $("#owl-team .owl-item.active").not(":eq(1)").removeClass('current-item');
                $("#owl-team .owl-item.active:eq(1)").addClass('current-item');
                console.log($("#owl-team .owl-item.active:eq(1) a").data("target"));
                $(".bloc-team[data-team='" + $("#owl-team .owl-item.active:eq(1) a").data("target") + "']").css({ display:"block" }).stop().animate({ opacity: 1 });
            });
        }
    }



    /****** Scroll Bloc Concept Podz ********/
    if( $('.ContentTxtConceptPodz').length ){
        $('.ContentTxtConceptPodz').niceScroll({
            touchbehavior:false,
            cursorcolor:"#999999",
            cursoropacitymax:1,
            cursorwidth:3,
            background:"#f6f6f6",
            autohidemode:false
        });
    }

    /****** Scroll Bloc Concept Podz ********/
    if( $('.scrollTxtTP').length ){
        $('.scrollTxtTP').niceScroll({
            touchbehavior:false,
            cursorcolor:"#999999",
            cursoropacitymax:1,
            cursorwidth:3,
            background:"#f6f6f6",
            autohidemode:false
        });
    }


    /**** BlackAndWhite list Guides Pothologies ******/
    if( $('.bwWrapper').length ){
        $('.bwWrapper:not(.active-img)').BlackAndWhite({
            hoverEffect : true, 
            webworkerPath : false,
            invertHoverEffect: false,
            intensity:1,
            speed: { 
                fadeIn: 200,
                fadeOut: 800
            }
        });
    }



    /****** Accordion *********/
    if( $('#accordion').length ){
        $( "#accordion" ).accordion({
            collapsible: true,
            heightStyle: "content"  
        });
    }
    if( $('.accordion_result').length ){
        $('.accordion_result').accordion({
            collapsible: true,
            heightStyle: "content"  
        });
    }

    /****** Slider Bloc Podologie *********/
    if( $('.royalSliderBloc').length ){
        $('.royalSliderBloc').royalSlider({
            arrowsNav: true,
            loop: true,
            keyboardNavEnabled: true,
            controlsInside: false,
            imageScaleMode: 'fill',
            arrowsNavAutoHide: false,
            autoScaleSliderWidth: 490,     
            autoScaleSliderHeight: 340,
            controlNavigation: 'none',
            thumbsFitInViewport: false,
            navigateByClick: true,
            startSlideId: 0,
            autoScaleSlider: true,
            // autoPlay: {
            //     enabled: true,
            //     pauseOnHover: false
            // },
            transitionType:'move',
            globalCaption: true
        });
    }



    /****** Erreur Contact *******/
    if( $('#FormContact').length ){
        $('#FormContact').validate({
            rules: {
                nom: "required",
                prenom: "required",
                email: {
                    required: true,
                    email: true
                }
            },
            errorPlacement: function(error, element) {
                element.parent().find('label').addClass('sError');
                if( error.text() != "" ){
                    error.appendTo($("#message-error .content-msg-error"));
                    $('.content-msg-error').css('left', '0');
                    $('.content-msg-error').html('<span for="" class="error">'+REQUIRED_FIELDS+'</span>');
                }
            },
            success: function(label, element) {
                $(element).parent().find('label').removeClass('sError');
            },
            messages: {
                nom: REQUIRED_FIELDS,
                prenom: REQUIRED_FIELDS,
                email: REQUIRED_FIELDS
            }
        });
    }
	$(document).on('click', '.closePopupValidation, #overlay-contact', function(event) {
        $('#overlay-contact').fadeOut();
        $('#messageValidation').hide();
        event.preventDefault();
    });
    if( $('#messageValidation').length ){
        $('#messageValidation, #overlay-contact').removeClass('hidden');
    }



    /***** Validation Newsletter ********/
    $(document).on('click', '#newsletterSend', function(event) {
        var mailNewsletter = $('#email_newsletter').val();
        if (mailNewsletter=='' || mailNewsletter==undefined || !isValidEmailAddress(mailNewsletter) ) {
            $('#email_newsletter').addClass('error');
            $('#email_newsletter').attr('value', INVAlID_FIELD);
            return false;
        }else{
            $('#FormNewsletterFooter').submit();
        }
        event.preventDefault();
    });

    $(document).on('click', '#email_newsletter', function(event) {
        if( $(this).val() == INVAlID_FIELD ){
            console.log("FOCUS");
            if( $(this).hasClass('firstFocus') ){
                $(this).removeClass('firstFocus');
            }else{
                $(this).attr('value', '');
            }
        }
    });
    
    $(document).on('click', '.closePopupValidationNews, #overlay-newsletter', function(event) {
        $('#overlay-newsletter').fadeOut();
        $('#newslettereValidation').hide();
        event.preventDefault();
    });



    $(document).on('click', '#ListChoixPathologie li a', function(event) {
        $dataFilter = $(this).data('filter');

        $('#ListChoixPathologie li a').parent().removeClass('current');
        $(this).parent().addClass('current');

        // console.log('Data Filter : '+$dataFilter);
        $('#accordion h3').each(function(index, el) {
            if( $(this).data('filter') != $dataFilter){
                $(this).fadeOut('400');
            }else{
                $(this).fadeIn('400');
            }
        });
        $('#accordion div.ui-accordion-content').each(function(index, el) {
            if( $(this).data('filter') != $dataFilter){
                $(this).fadeOut('400');
            }else{
                $(this).fadeOut('400');
            }
        });
        event.preventDefault();
    });




    /****** Scroll Page Footer ********/
    if( $('.ScrollPageStatic').length ){
        $('.ScrollPageStatic').niceScroll({
            touchbehavior:false,
            cursorcolor:"#999999",
            cursoropacitymax:1,
            cursorwidth:3,
            background:"#f6f6f6",
            autohidemode:false
        });
    }



    /***** Click SVG *******/
    $(document).on('mouseenter', '.hover-svg-1', function(event) {
        var $NameData = $(this).data('name');
        $('#TitleTranche').text($NameData);
        event.preventDefault();
    });

    /***** Click sur une Zone dans le corps ******/
    $(document).on('click', '.hover-svg-1', function(event) {
        var $NameZone = $(this).data('name');
        $('.BlocCorps').fadeOut('400', function() {
            $('#navModuleAnalyse li').removeClass('current-item');
            $('#navModuleAnalyse li[data-step="2"]').addClass('current-item');
            $('.backModule').attr('data-step', 2);
            $('.backModule').attr('data-namezoneprev', $NameZone);
            $('.BlocZoneSpe[data-blocsvg="'+$NameZone+'"]').css('display', 'block');
            setTimeout(function() {
                $('.BlocZoneSpe[data-blocsvg="'+$NameZone+'"]').addClass('active');
            }, 10);
        });
        event.preventDefault();
    });

    $(document).on('mouseenter', '.BlocZoneSpe .zone', function(event) {
        var $PhrasePatho = $(this).data('pathologies');
        var $NameZoneSpe = $(this).data('namezone');
        $('#TitleTranche').text($NameZoneSpe);
        $('.phrasePathologie').text($PhrasePatho);
        if( !$('.phrasePathologie').hasClass('active') ){
            $('.phrasePathologie').addClass('active');
        }
        event.preventDefault();
    });


    $(document).on('click', '.BlocZoneSpe .zone', function(event) {
        var $NameResult = $(this).data('namezone');
        $('.BlocZoneSpe').fadeOut('400', function() {
            $('.BlocZoneSpe.active').removeClass('active');
            $('#navModuleAnalyse li').removeClass('current-item');
            $('#navModuleAnalyse li[data-step="3"]').addClass('current-item');
            $('.backModule').attr('data-step', 3);
            $('.phrasePathologie').removeClass('active');
            $('#TitleTranche').text('');
            $('.BlocResultats[data-nameresultat="'+$NameResult+'"]').css('display', 'block');
            setTimeout(function() {
                $('.BlocResultats[data-nameresultat="'+$NameResult+'"]').addClass('active');
            }, 400);
        });
        event.preventDefault();
    });

    $(document).on('click', '.backModule', function(event) {
        var $DataStep = $(this).data('step');
        if( $DataStep == "2" ){
            $('.BlocZoneSpe').fadeOut('400', function() {
                $('#navModuleAnalyse li').removeClass('current-item');
                $('#navModuleAnalyse li[data-step="1"]').addClass('current-item');
                $('.phrasePathologie').removeClass('active');
                $('.BlocCorps').fadeIn('400');
                $('.backModule').attr('data-step', 1);
            });
        }else if( $DataStep == "3" ){
            var $NameZonePrev = $(this).data('namezoneprev');
            $('.BlocResultats.active').removeClass('active');
            $('.BlocResultats').fadeOut('400', 'linear', function() {
                $('#navModuleAnalyse li').removeClass('current-item');
                $('#navModuleAnalyse li[data-step="2"]').addClass('current-item');
                setTimeout(function() {
                    $('.BlocZoneSpe[data-blocsvg="'+$NameZonePrev+'"]').css('display', 'block');
                    $('.BlocZoneSpe[data-blocsvg="'+$NameZonePrev+'"]').addClass('active');
                }, 600);
                $('.backModule').attr('data-step', 2);
            });
        }else{
            return false;
        }
        event.preventDefault();
    });
    /***** Hover marqueur *****/
    // $('.BlocZoneSpe .zone').mouseenter(function(event) {
    //     $(this).find(".rond").fadeTo( 500 , 1);
    //     $(this).find(".cercle").animate({
    //         r: 14, 
    //         fill: "none", 
    //         "stroke-width": 2, 
    //         "stroke-opacity": 1, 
    //         opacity: 1 
    //         },500
    //     );
    //     console.log("enter");
    // });
    // $('.BlocZoneSpe .zone .cercle').mouseleave(function(event) {
    //     $(this).parent().find(".rond").fadeTo( 500 , 0.5);
    //     $(this).animate({
    //         r: 9,
    //         fill: "none",
    //         "stroke-width": 2,
    //         "stroke-opacity": 1,
    //         opacity: 0
    //         },500
    //     );  
    //     console.log("leave");
    // });
    // $(".zone").hover(
    //             function() {
    //                 $(this).find(".rond").fadeTo( 500 , 1);
    //                 $(this).find(".cercle").animate({
    //                     r: 14, // Rayon
    //                     fill: "none", // Couleur à l'intétieur du cercle
    //                     "stroke-width": 2, // Epaisseur du bord du cercle
    //                     "stroke-opacity": 1, // Opacité du bord du cercle
    //                     opacity: 1 // Opacité du cercle
    //                     },500
    //                 );
    //             },  function() {                    
                
    //                 $(this).find(".rond").fadeTo( 500 , 0.5);
    //                 $(this).find(".cercle").animate({
    //                     r: 9,
    //                     fill: "none",
    //                     "stroke-width": 2,
    //                     "stroke-opacity": 1,
    //                     opacity: 0
    //                     },500
    //                 );                  
                    
    //             }
                    
    //         );

});


/***** Video Page Concept Podz ******/
if(document.getElementById("videoPlayer") != null ){
    // global variable for the player
    var player;
    function onYouTubePlayerAPIReady() {
      player = new YT.Player('videoPlayer', {
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onYTPlayerStateChange
        }
      });
    }

    function onPlayerReady(event) { 
      var playButton = document.getElementById("play-video");
      playButton.addEventListener("click", function() {
        console.log('Ready');
        document.getElementById("coverVideo").style.display='none';
        player.playVideo();
      });      
    }

    function onYTPlayerStateChange(event){
        console.log('state change');
        if(event.data === YT.PlayerState.ENDED) {     
           document.getElementById("coverVideo").style.display='block';
        }
    }

    // Inject YouTube API script
    var tag = document.createElement('script');
    tag.src = "//www.youtube.com/player_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
}