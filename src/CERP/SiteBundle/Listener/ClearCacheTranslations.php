<?php  

namespace CERP\SiteBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Lexik\Bundle\TranslationBundle\Entity\Translation;
use Symfony\Component\Finder\Finder;

class ClearCacheTranslations
{
	protected $sPath;
	protected $sEnv;


	public function __construct($oContainer)
    {
        $this->sPath = $oContainer->get('kernel')->getRootDir();
        $this->sEnv = $oContainer->get('kernel')->getEnvironment();  

    }

	public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if ($entity instanceof Translation) {

             $this->clearLanguageCache();
        }
    }

    public function clearLanguageCache()
    {    	

	    $cacheDir = $this->sPath.'/cache';
	    $finder = new Finder();

	    $finder->files()->in($cacheDir . "/" . $this->sEnv . "/translations");

		if ($this->sEnv == "dev")
		{

		    if ( file_exists($cacheDir . "/" . $this->sEnv . "/app" . ucfirst($this->sEnv) . "UrlMatcher.php.meta") ) unlink($cacheDir . "/" . $this->sEnv . "/app" . ucfirst($this->sEnv) . "UrlMatcher.php.meta");
		    if ( file_exists($cacheDir . "/" . $this->sEnv . "/app" . ucfirst($this->sEnv) . "UrlGenerator.php.meta") ) unlink($cacheDir . "/" . $this->sEnv . "/app" . ucfirst($this->sEnv) . "UrlGenerator.php.meta");
		}

		if ( file_exists($cacheDir . "/" . $this->sEnv . "/app" . ucfirst($this->sEnv) . "UrlMatcher.php") ) unlink($cacheDir . "/" . $this->sEnv . "/app" . ucfirst($this->sEnv) . "UrlMatcher.php");
		if ( file_exists($cacheDir . "/" . $this->sEnv . "/app" . ucfirst($this->sEnv) . "UrlGenerator.php") ) unlink($cacheDir . "/" . $this->sEnv . "/app" . ucfirst($this->sEnv) . "UrlGenerator.php");

		foreach($finder as $file)
		{
		   	unlink($file->getRealpath());
		}

	}

}



?>