<?php  

namespace CERP\SiteBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

use Symfony\Component\HttpKernel\Exception\HttpException;

use Symfony\Component\HttpFoundation\RedirectResponse;

class GlobalMenu
{
	protected $oRouter;
	protected $oRequest;
	protected $oSession;
	protected $oDoctrine;
	protected $oKernel;

	public function __construct($oContainer)
    {
        $this->oRouter = $oContainer->get('router');
        $this->oRequest = $oContainer->get('request');
        $this->oSession = $oContainer->get('session');
        $this->oDoctrine = $oContainer->get('doctrine');
        $this->oKernel = $oContainer->get('kernel');
    }

    public function onKernelRequest($oEvent)
	{
		$this->oSession->remove('bGloablMenusExist');

		if ( $this->oKernel->getEnvironment() == 'dev' || !$this->oSession->has('bGloablMenusExist') )
		{
			$this->oSession->set('bGloablMenusExist', true);

			//remplissage de session ac Menu

			$aMenu = $this->oDoctrine->getRepository('CERPSiteBundle:Menu')->getActif();

	    	$this->oSession->set('GlobalMenus', $aMenu);
		}
	}
}

?>