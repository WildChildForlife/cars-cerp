<?php  

namespace CERP\SiteBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

use Symfony\Component\HttpKernel\Exception\HttpException;

use Symfony\Component\HttpFoundation\RedirectResponse;

class BrowsersLocal
{
	protected $oRouter;
	protected $oRequest;
	protected $oSession;

	public function __construct($oContainer)
    {
        $this->oRouter = $oContainer->get('router');
        $this->oRequest = $oContainer->get('request');
        $this->oSession = $oContainer->get('session');
    }

    public function onKernelRequest($oEvent)
	{
		if ( !$this->oSession->has('bFirstVisit') )
		{
			$this->oSession->set('bFirstVisit', true);
			if ( in_array('fr', $this->oRequest->getLanguages()) ) $this->oSession->set('_locale', 'fr');
	        else if ( in_array('fr_FR', $this->oRequest->getLanguages()) ) $this->oSession->set('_locale', 'fr');
	        else $this->oSession->set('_locale', 'en');
	    	
	    	$oEvent->setResponse(
	    		new RedirectResponse(
	    			$this->oRouter->generate('fr__RG__CERP_homepage', array(
	    				'_locale' => $this->oSession->get('_locale')
	    				))
	    			)
	    		);
		}

	}
}



?>