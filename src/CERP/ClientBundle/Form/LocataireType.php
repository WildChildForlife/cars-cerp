<?php

namespace CERP\ClientBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CERP\ClientBundle\Entity;
use CERP\ClientBundle\Form\BlackListType;

class LocataireType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', null, array('label' => 'Nom'))
            ->add('prenom', null, array('label' => 'Prenom'))
            ->add('cin', null, array('label' => 'CIN / N° Passport '))
            ->add('telephone', null, array('label' => 'Telephone'))
            ->add('permis', null, array('label' => 'N° Permis'))
            ->add('adresse', null, array('label' => 'Adresse postale'))
        // $builder
        //     ->add('blacklists', 'collection', array('type' => new BlackListType()))        
            //->add('blacklists', null, array('property' => 'incident','label' => 'manatif : '))  // it shows this fields even in the create form makhdamach lina
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CERP\ClientBundle\Entity\Locataire'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'locataire';
    }
}
