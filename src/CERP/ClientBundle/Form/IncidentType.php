<?php

namespace CERP\ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class IncidentType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', 'choice', array( 
                'choices' => array(
                    'Dommage Voiture' => 'Dommage Voiture',
                    'Vol' => 'Vol',
                    'Mauvais comportement' => 'Mauvais comportement',
                    'Paiement' => 'Paiement'
                    ), 
                'label' => 'Type :', 
                'attr' => array( 'class' => 'form-control select2' )
                ))
            ->add('description', 'text', array( 'label' => 'Incident :', 'attr' => array( 'class' => 'form-control' ), 'required' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CERP\ClientBundle\Entity\Incident'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'incident';
    }
}
