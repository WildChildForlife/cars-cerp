<?php

namespace CERP\ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VehiculeType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('chassis')
            ->add('modele')
            ->add('kilometrage')            
            ->add('marque') 
           

            // ->add('energie', 'sonata_type_collection', array('label' => 'Energie','by_reference' => false, 'cascade_validation' => true), array(
            //         'edit' => 'inline',
            //         'inline' => 'table'
            //     ))         
            // ->add('marque', 'sonata_type_collection', array(
            //         'type'   => new MarqueType(),
            //         'allow_add' => true,
            //         'allow_delete' => true,
            //         'by_reference' => false,
            //         'required' => false
            // ))   
            // ->add('marque', 'sonata_type_collection', array('label' => 'marque','by_reference' => false, 'cascade_validation' => true), array(
            //         'edit' => 'inline',
            //         'inline' => 'table'
            //     ))                                                         
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CERP\ClientBundle\Entity\Vehicule'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vehicule';
    }
}
