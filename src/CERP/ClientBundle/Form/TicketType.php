<?php

namespace CERP\ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class TicketType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', 'choice', array('choices' => array('Amelioration' => 'Amelioration', 'Bug' => 'Bug', 'Reclamation' => 'Reclamation', 'Remerciement' => 'Remerciement')))
            ->add('priorite', 'choice', array('choices' => array('Haute' => 'Haute', 'Moyenne' => 'Moyenne', 'Basse' => 'Basse')))            
            ->add('etat', 'choice', array('choices' => array('Ouvert' => 'Ouvert', 'En cours' => 'En cours', 'Traité' => 'Traité', 'Fermé' => 'Fermé')))
            ->add('titre',null,array('label' => 'Titre'))
            ->add('description','textarea',array('label' => 'Description'))
            ->add('dateDemande',null,array('label' => 'Date de création du ticket', 'date_widget' => "single_text", 'time_widget' => "single_text"))
           
            // ->add('marque', 'collection', array(
            //         'type'   => new MarqueType(),
            //         'allow_add' => true,
            //         'allow_delete' => true,
            //         'by_reference' => false,
            //         'required' => false
            // ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CERP\ClientBundle\Entity\Ticket'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ticket';
    }
}
