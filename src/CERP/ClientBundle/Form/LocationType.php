<?php

namespace CERP\ClientBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CERP\ClientBundle\Entity;
use CERP\ClientBundle\Form\LocationType;
use Doctrine\ORM\EntityRepository;


class LocationType extends AbstractType
{
    private $oUser;

    public function __construct($oUser)
    {
        $this->oUser = $oUser;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder        
            /*->add('libelle', null, array('label' => 'libelle : '))*/
            // ->add('dateLocation', null, array('label' => "Date prévue pour la location", 'date_widget' => "single_text", 'time_widget' => "single_text"))
            ->add('dateLocation' , null , array( 'label' => 'Date prévue pour la location', "html5" => false, 'time_widget' => 'single_text', 'date_widget' => 'single_text', 'date_format' => 'dd/MM/yyyy' ))
            ->add('dateRetour' , null , array( 'label' => 'Date prévue du retour du véhicule', "html5" => false, 'time_widget' => 'single_text', 'date_widget' => 'single_text', 'date_format' => 'dd/MM/yyyy' ))
            // ->add('dateRetour', null, array('label' => "Date de retour", 'date_widget' => "single_text", 'time_widget' => "single_text"))
            ->add('locataire', 'entity', array('label' => 'Locataire : ', 'class' => 'CERPClientBundle:Locataire', 'query_builder' => function (EntityRepository $er) 
            {
                return $er->createQueryBuilder('l')
                            ->where('l.user = ' . $this->oUser->getId());
            }))
            ->add('voiture', 'entity', array('label' => 'Voiture :', 'class' => 'CERPClientBundle:Voiture', 'query_builder' => function (EntityRepository $er) 
            {
                return $er->createQueryBuilder('v')
                            ->where('v.user = ' . $this->oUser->getId());
            }))
            //->add('voiture', null, array('label' => 'Voiture : '))
            //->add('locataire', null, array('label' => 'Locataire : '))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CERP\ClientBundle\Entity\Location'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'location';
    }
}
