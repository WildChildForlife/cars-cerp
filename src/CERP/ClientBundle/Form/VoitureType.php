<?php

namespace CERP\ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CERP\ClientBundle\Form\MarqueType;


class VoitureType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('chassis')
            ->add('prixLocation',null,array('label' => 'Prix de location B2C'))            
            ->add('prixVoiture',null,array('label' => 'Prix d\'achat de la voiture'))  
            ->add('prixLocationB2B',null,array('label' => 'Prix de location B2B'))                        
            ->add('matricule',null,array('label' => 'Matricule'))
            ->add('modele', null, array('label' => 'Modèle'))
            ->add('couleur', null, array('label' => 'Couleur'))
            ->add('nbrPorte', null, array('label' => 'Nombre de portes'))
            ->add('puissanceFiscale', null, array('label' => 'Puissance fiscale'))
            ->add('nbrVitesse', null, array('label' => 'Nombre de vitesses'))
            // ->add('boitevitesse')
            ->add('kilometrage',null,array('label' => 'Kilométrage'))
            ->add('annee',null,array('label' => 'Année du modéle'))
            // ->add('marque',null,array('label' => 'Marque'))
            ->add('boitevitesse',null,array('label' => 'Boite à vitesse'))
            ->add('energie' ,null , array( 'label' => 'Energie'))
            ->add('dateMiseEnCirculation' , null , array( 'label' => 'Date de mise en circulation', "html5" => false, 'widget' => 'single_text', 'format' => 'dd/MM/yyyy' ))
            ->add('vidange' ,null , array( 'label' => 'Kilométrage du prochain vidange'))
            ->add('dateVisiteTechnique' , null , array( 'label' => 'Date de la prochaine visite technique', "html5" => false, 'widget' => 'single_text', 'format' => 'dd/MM/yyyy' ))
            ->add('dateAssurance' , null , array( 'label' => 'Date de fin d\'assurance', "html5" => false, 'widget' => 'single_text', 'format' => 'dd/MM/yyyy' ))
            ->add('equipements' ,null , array( 'label' => 'Equipements', 'multiple' => true, 'expanded' => true))
            // ->add('marque', 'collection', array(
            //         'type'   => new MarqueType(),
            //         'allow_add' => true,
            //         'allow_delete' => true,
            //         'by_reference' => false,
            //         'required' => false
            // )) 
            ->add('typeVehicule',null,array('label' => 'Type de véhicule'))
            ->add('disponibleEchange',null, array('label' => 'Disponible pour échange'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CERP\ClientBundle\Entity\Voiture'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'voiture';
    }
}
