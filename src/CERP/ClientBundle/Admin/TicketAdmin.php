<?php

namespace CERP\ClientBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;


class TicketAdmin extends Admin
{


    protected function configureRoutes(RouteCollection $collection)
    {
        $collection            
            ->remove('delete')
            
            ;
    }


    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //Filtres
        $datagridMapper                    
            ->add('priorite', null, array('label' => 'Priorité'))
            ->add('type', null, array('label' => 'Type'))        
            ->add('etat', null, array('label' => 'Etat'))
            ->add('titre', null, array('label' => 'Titre'))
            ->add('description', null, array('label' => 'Description'))
            ->add('dateDemande', null, array('label' => 'Date création'))
            ->add('user', null, array('label' => 'Client'))
            ->add('consultant', null, array('label' => 'Consultant'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper            
            ->addIdentifier('priorite', 'sonata_type_model_list', array('label' => 'Priorité', 'template' => 'ApplicationSonataAdminBundle:Widgets:list_picto_ticket_priorite.html.twig'))
            ->add('type')            
            ->add('etat')
            ->add('titre')
            ->add('description')
            ->add('dateDemande')
            ->add('user')
            ->add('consultant')            

            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper            
            ->add('priorite', 'choice', array('label' => 'Priorité', 'choices' => array('Haute' => 'Haute', 'Moyenne' => 'Moyenne', 'Basse' => 'Basse')))
            ->add('type', 'choice', array('label' => 'Type', 'choices' => array('Amelioration' => 'Amelioration', 'Bug' => 'Bug', 'Reclamation' => 'Reclamation', 'Remerciement' => 'Remerciement')))       
            ->add('etat', 'choice', array('label' => 'Etat', 'choices' => array('Ouvert' => 'Ouvert', 'En cours' => 'En cours', 'Traité' => 'Traité', 'Fermé' => 'Fermé')))
            ->add('titre', null, array('label' => 'Titre'))
            ->add('description', 'textarea', array('label' => 'Description') )
            ->add('dateDemande',null,array('label' => 'Date de création du ticket', 'date_widget' => "single_text", 'time_widget' => "single_text"))
            ->add('consultant', null, array('label' => 'Consultant'))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper            
            ->add('priorite')
            ->add('type')            
            ->add('etat')
            ->add('titre')
            ->add('description')
            ->add('dateDemande')
            ->add('consultant')
        ;
    }
}
