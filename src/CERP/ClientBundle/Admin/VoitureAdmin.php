<?php

namespace CERP\ClientBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class VoitureAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('chassis',null, array('label' => 'Chassis'))
            ->add('matricule',null, array('label' => 'Matricule'))
            ->add('prixLocation',null, array('label' => 'Prix de location B2C'))
            ->add('prixLocationB2B',null, array('label' => 'Prix de location B2B'))
            ->add('prixVoiture',null, array('label' => 'Prix d\'achat de la voiture'))
            ->add('couleur',null, array('label' => 'Couleur'))
            ->add('nbrPorte',null, array('label' => 'Nombre de portes'))
            ->add('puissanceFiscale',null, array('label' => 'Puissance fiscale'))
            ->add('nbrVitesse',null, array('label' => 'Nombre de vitesses'))
            ->add('boitevitesse',null, array('label' => 'Boite à vitesse'))
            ->add('kilometrage',null, array('label' => 'Kilométrage'))
            ->add('annee',null, array('label' => 'Année'))
            ->add('disponibleEchange',null, array('label' => 'Disponible pour échange'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('chassis',null, array('label' => 'Chassis'))
            ->add('matricule',null, array('label' => 'Matricule'))
            ->add('prixLocation',null, array('label' => 'Prix de location B2C'))
            ->add('prixLocationB2B',null, array('label' => 'Prix de location B2B'))
            ->add('prixVoiture',null, array('label' => 'Prix d\'achat de la voiture'))                        
            ->add('couleur',null, array('label' => 'Couleur'))
            ->add('nbrPorte',null, array('label' => 'Nombre de portes'))
            ->add('puissanceFiscale',null, array('label' => 'Puissance fiscale'))
            ->add('nbrVitesse',null, array('label' => 'Nombre de vitesses'))
            ->add('boitevitesse',null, array('label' => 'Boite à vitesse'))
            ->add('kilometrage',null, array('label' => 'Kilométrage'))
            ->add('annee',null, array('label' => 'Année'))
            ->add('disponibleEchange',null, array('label' => 'Disponible pour échange'))
            ->add('pieces',null, array('label' => 'Pièces'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id')
            ->add('chassis',null, array('label' => 'Chassis'))
            ->add('matricule',null, array('label' => 'Matricule'))
            ->add('prixLocation',null, array('label' => 'Prix de location B2C'))
            ->add('prixLocationB2B',null, array('label' => 'Prix de location B2B'))
            ->add('prixVoiture',null, array('label' => 'Prix d\'achat de la voiture'))            
            ->add('couleur',null, array('label' => 'Couleur'))
            ->add('nbrPorte',null, array('label' => 'Nombre de portes'))
            ->add('puissanceFiscale',null, array('label' => 'Puissance fiscale'))
            ->add('nbrVitesse',null, array('label' => 'Nombre de vitesses'))
            ->add('boitevitesse',null, array('label' => 'Boite à vitesse'))
            ->add('kilometrage',null, array('label' => 'Kilométrage'))
            ->add('annee',null, array('label' => 'Année'))
            ->add('disponibleEchange',null, array('label' => 'Disponible pour échange'))
            ->add('pieces',null, array('label' => 'Pièces'))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('chassis',null, array('label' => 'Chassis'))
            ->add('matricule',null, array('label' => 'Matricule'))
            ->add('prixLocation',null, array('label' => 'Prix de location B2C'))
            ->add('prixLocationB2B',null, array('label' => 'Prix de location B2B'))
            ->add('prixVoiture',null, array('label' => 'Prix d\'achat de la voiture'))            
            ->add('couleur',null, array('label' => 'Couleur'))
            ->add('nbrPorte',null, array('label' => 'Nombre de portes'))
            ->add('puissanceFiscale',null, array('label' => 'Puissance fiscale'))
            ->add('nbrVitesse',null, array('label' => 'Nombre de vitesses'))
            ->add('boitevitesse',null, array('label' => 'Boite à vitesse'))
            ->add('kilometrage',null, array('label' => 'Kilométrage'))
            ->add('annee',null, array('label' => 'Année'))
            ->add('disponibleEchange',null, array('label' => 'Disponible pour échange'))
            ->add('pieces',null, array('label' => 'Pièces'))
        ;
    }
}
