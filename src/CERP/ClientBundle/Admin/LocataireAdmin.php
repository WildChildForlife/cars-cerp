<?php

namespace CERP\ClientBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class LocataireAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nom', null, array('label' => 'Nom'))
            ->add('prenom', null, array('label' => 'Prénom'))
            ->add('cin', null, array('label' => 'CIN'))
            ->add('telephone', null, array('label' => 'Numéro de téléphone'))
            ->add('permis', null, array('label' => 'Numéro de permis'))
            ->add('adresse', null, array('label' => 'Adresse'))
            ->add('actif')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('nom', null, array('label' => 'Nom'))
            ->add('prenom', null, array('label' => 'Prénom'))
            ->add('cin', null, array('label' => 'CIN'))
            ->add('telephone', null, array('label' => 'Numéro de téléphone'))
            ->add('permis', null, array('label' => 'Numéro de permis'))
            ->add('adresse', null, array('label' => 'Adresse'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
            ->add('actif')            
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('nom', null, array('label' => 'Nom'))
            ->add('prenom', null, array('label' => 'Prénom'))
            ->add('cin', null, array('label' => 'CIN'))
            ->add('telephone', null, array('label' => 'Numéro de téléphone'))
            ->add('permis', null, array('label' => 'Numéro de permis'))
            ->add('adresse', null, array('label' => 'Adresse'))
            ->add('actif')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('nom', null, array('label' => 'Nom'))
            ->add('prenom', null, array('label' => 'Prénom'))
            ->add('cin', null, array('label' => 'CIN'))
            ->add('telephone', null, array('label' => 'Numéro de téléphone'))
            ->add('permis', null, array('label' => 'Numéro de permis'))
            ->add('adresse', null, array('label' => 'Adresse'))
            ->add('actif')
        ;
    }
}
