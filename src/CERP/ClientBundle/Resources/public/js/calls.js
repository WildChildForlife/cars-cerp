$(document).ready(function(){


	function addMarquesPic(marques) 
    {
      if (!marques.id) { return marques.text; }
      var marques = $('<span><img src="' + $(marques.element).data('image') + '" class="image-select2-marques" /> ' + marques.text + '</span>');
        
      return marques;
    }
	$(".select2-image").select2({
        templateResult: addMarquesPic,
        templateSelection: addMarquesPic
    });

    $(".colorpicker-form").colorpicker().on('changeColor', function(ev) {
        $(".colorpicker-form").css({ color: ev.color.toString(), backgroundColor: ev.color.toString() });   
    });

    $(".datepicker").datepicker({
        format: 'dd/mm/yyyy',
        language: 'fr'
    });
    $(".timepicker").timepicker({
        minuteStep: 5
    });
});