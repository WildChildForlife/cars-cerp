$(document).ready(function()
{
    if ( $(".colorpicker-form").length && $(".colorpicker-form").val() != "" )
    {
        $(".colorpicker-form").css({ color: $(".colorpicker-form").val(), backgroundColor: $(".colorpicker-form").val() });   
    }

    if ( $(".couleur-list").length && $(".couleur-list").data("couleur") != "" )
    {
        $.each($(".couleur-list"), function(){
            $(this).css({ backgroundColor: $(this).data("couleur") });       
        });
    }
});