$(document).ready(function(){

	var $sLink = '';
	$(document).on("click", ".warning-delete", function(e){
        e.preventDefault();
        // console.log($(this).)
        $sLink = ( $(this).hasClass('specialbutton-red') ) ? true : $(this).attr('href');


        var $sPrimaryField = $(this).parents('tr').find('.primary-col').text();
        var $sSecondaryField = $(this).parents('tr').find('.secondary-col').text();
        $(".modal-warning.delete-confirmation-modal").css({ display: 'block' }).animate({ opacity: 1 });
        $(".delete-confirmation-modal .insert-to-delete").text($sPrimaryField + ' ' + $sSecondaryField);
        $(".delete-confirmation-modal").removeClass('hidden');
    });
    $(document).on("click", ".dismiss-modal", function(){
        $(this).parents('.modal-warning').animate({ opacity: 0 }, 500, function(){
            $(this).css({ display : 'none' });
        });
    });
    $(document).on("click", ".do-it-modal", function(){
        if ( typeof $(this).data('href') != "undefined" && $(this).data('href') != "" ) $sLink = $(this).data('href');
        window.location.href = $sLink;
    }); 

});