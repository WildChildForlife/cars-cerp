/* In the functions section out of document ready */

/**
 * Add a new row in a form Collection
 *
 * Difference from source is that I use Bootstrap convention
 * to get the part we are interrested in, the input tag itself and not
 * create a new .collection-field block inside the original.
 * 
 * Source: http://symfony.com/doc/current/cookbook/form/form_collections.html
 */

// setup an "add a tag" link
//var $addTagLink = $('<button type="button" class="btn btn-block btn-danger">Ajouter un incident</button>');
var $newLinkLi = $('<div class="form-group"></div>');

/* ********** */
$(document).ready(function()
{
    var $collectionHolder;        
    // Get the ul that holds the collection of tags
    $collectionHolder = $('.blacklist-bloc');
    // add the "add a tag" anchor and li to the tags ul
    $collectionHolder.append($newLinkLi);
    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);
    $(".ajouter-incident").on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();
        $(".ras").css({ display:"none" });
        // add a new tag form (see next code block)
        addTagForm($collectionHolder, $newLinkLi);
        $(".select2").select2();
    });    
})

function addTagForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);
    newForm = newForm.replace(/<div>/g, '<div class="form-group">');

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<div class="form-group-separate"></div>').append(newForm);
    $newLinkLi.before($newFormLi);
}