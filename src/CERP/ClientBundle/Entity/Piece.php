<?php

namespace CERP\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Piece
 *
 * @ORM\Table(name="Piece")
 * @ORM\Entity(repositoryClass="CERP\ClientBundle\Entity\Repository\PieceRepository")
 */
class Piece
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date_Changement", type="datetime")
     */
    private $dateChangement;

    /** @ORM\ManyToOne(targetEntity="Voiture", inversedBy="pieces") */
    protected $voiture;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

        public function __toString()
    {
        return $this->libelle;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Piece
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set dateChangement
     *
     * @param \DateTime $dateChangement
     * @return Piece
     */
    public function setDateChangement($dateChangement)
    {
        $this->dateChangement = $dateChangement;

        return $this;
    }

    /**
     * Get dateChangement
     *
     * @return \DateTime 
     */
    public function getDateChangement()
    {
        return $this->dateChangement;
    }

    /**
     * Set voiture
     *
     * @param \CERP\ClientBundle\Entity\Voiture $voiture
     * @return Piece
     */
    public function setVoiture(\CERP\ClientBundle\Entity\Voiture $voiture = null)
    {
        $this->voiture = $voiture;

        return $this;
    }

    /**
     * Get voiture
     *
     * @return \CERP\ClientBundle\Entity\Voiture 
     */
    public function getVoiture()
    {
        return $this->voiture;
    }
}
