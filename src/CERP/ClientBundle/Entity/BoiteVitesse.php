<?php

namespace CERP\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BoiteVitesse
 *
 * @ORM\Table(name="BoiteVitesse")
 * @ORM\Entity(repositoryClass="CERP\ClientBundle\Entity\Repository\BoiteVitesseRepository")
 */
class BoiteVitesse
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity="CERP\ClientBundle\Entity\Vehicule", mappedBy="boitevitesse", cascade="all")
     */
    protected $vehicules;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vehicules = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
   {
       return $this->libelle;
   }      

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return BoiteVitesse
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Add voitures
     *
     * @param \CERP\ClientBundle\Entity\Voiture $voitures
     * @return BoiteVitesse
     */
    public function addVoiture(\CERP\ClientBundle\Entity\Voiture $voitures)
    {
        $this->voitures[] = $voitures;

        return $this;
    }

    /**
     * Remove voitures
     *
     * @param \CERP\ClientBundle\Entity\Voiture $voitures
     */
    public function removeVoiture(\CERP\ClientBundle\Entity\Voiture $voitures)
    {
        $this->voitures->removeElement($voitures);
    }

    /**
     * Get voitures
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVoitures()
    {
        return $this->voitures;
    }
}
