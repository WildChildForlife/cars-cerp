<?php

namespace CERP\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Voiture
 *
 * @ORM\Table(name="Voiture")
 * @ORM\Entity(repositoryClass="CERP\ClientBundle\Entity\Repository\VoitureRepository")
 */
class Voiture extends Vehicule
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
   
    /**
     * @var string
     *
     * @ORM\Column(name="matricule", type="string", length=255)
     */
    private $matricule;

    /**
     * @var string
     *
     * @ORM\Column(name="couleur", type="string", length=255)
     */
    private $couleur;


    /**
     * @var integer
     *
     * @ORM\Column(name="puissance_fiscale", type="integer")
     */
    private $puissanceFiscale;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_vitesse", type="integer")
     */
    private $nbrVitesse;


    /**
     * @var integer
     *
     * @ORM\Column(name="annee", type="integer")
     */
    private $annee;

    /**
     * @var float
     *
     * @ORM\Column(name="prixLocation", type="float", nullable=true)
     */
    private $prixLocation;

    /**
     * @var float
     *
     * @ORM\Column(name="prixLocationB2B", type="float", nullable=true)
     */
    private $prixLocationB2B;

    /**
     * @var float
     *
     * @ORM\Column(name="prixVoiture", type="float", nullable=true)
     */
    private $prixVoiture;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_porte", type="integer")
     */
    private $nbrPorte;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dateMiseEnCirculation", type="datetime")
     */
    private $dateMiseEnCirculation;


    /**
     * @ORM\OneToMany(targetEntity="CERP\ClientBundle\Entity\Piece", mappedBy="voiture" , cascade="all")
     */
    protected $pieces;

    /**
     * @ORM\ManyToMany(targetEntity="CERP\ClientBundle\Entity\Equipement", cascade={"persist"})
     */
    protected $equipements;

    /** @ORM\ManyToOne(targetEntity="Energie", inversedBy="voitures") */
    protected $energie;

    /** @ORM\ManyToOne(targetEntity="BoiteVitesse", inversedBy="voitures") */
    protected $boitevitesse;



    /** @ORM\ManyToOne(targetEntity="TypeVehicule", inversedBy="vehicules") */
    protected $typeVehicule;

    /**
     * @ORM\OneToMany(targetEntity="CERP\ClientBundle\Entity\Location", mappedBy="voiture" , cascade="all")
     */
    protected $locations;

    /**
     * @var string
     *
     * @ORM\Column(name="chassis", type="string", length=255)
     */
    private $chassis;

    /**
     * @var string
     *
     * @ORM\Column(name="modele", type="string", length=255)
     */
    private $modele;

    /** @ORM\ManyToOne(targetEntity="Marque", inversedBy="voitures") */
    protected $marque;

    /**
     * @var boolean
     *
     * @ORM\Column(name="actif", type="boolean", nullable=true)
     */
    private $actif=true;


    /**
     * @var integer
     *
     * @ORM\Column(name="kilometrage", type="integer")
     */
    private $kilometrage;

    /**
     * @var integer
     *
     * @ORM\Column(name="vidange", type="integer")
     */
    private $vidange;

    /**
     * @var date
     *
     * @ORM\Column(name="dateAssurance", type="datetime")
     */
    private $dateAssurance;

    /**
     * @var date
     *
     * @ORM\Column(name="dateVisiteTechnique", type="datetime")
     */
    private $dateVisiteTechnique;

    /**
     * @var boolean
     *
     * @ORM\Column(name="disponibleEchange", type="boolean")
     */
    private $disponibleEchange=false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="disponibilite", type="boolean")
     */
    private $disponibilite=true;   

    /** @ORM\ManyToOne(targetEntity="CERP\UserBundle\Entity\User", inversedBy="voitures") */
    protected $user;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dateCreation = new \DateTime();
        $this->pieces = new \Doctrine\Common\Collections\ArrayCollection();
        $this->equipements = new \Doctrine\Common\Collections\ArrayCollection();
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
   {
       return ucfirst($this->marque) . ' ' . ucfirst($this->modele) . ' ' . ucfirst($this->couleur) . ' - ' . strtoupper($this->matricule);
   }  


    /**
     * Set matricule
     *
     * @param string $matricule
     * @return Voiture
     */
    public function setMatricule($matricule)
    {
        $this->matricule = $matricule;

        return $this;
    }

    /**
     * Get matricule
     *
     * @return string 
     */
    public function getMatricule()
    {
        return $this->matricule;
    }

    /**
     * Set couleur
     *
     * @param string $couleur
     * @return Voiture
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * Get couleur
     *
     * @return string 
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Set puissanceFiscale
     *
     * @param integer $puissanceFiscale
     * @return Voiture
     */
    public function setPuissanceFiscale($puissanceFiscale)
    {
        $this->puissanceFiscale = $puissanceFiscale;

        return $this;
    }

    /**
     * Get puissanceFiscale
     *
     * @return integer 
     */
    public function getPuissanceFiscale()
    {
        return $this->puissanceFiscale;
    }

    /**
     * Set nbrVitesse
     *
     * @param integer $nbrVitesse
     * @return Voiture
     */
    public function setNbrVitesse($nbrVitesse)
    {
        $this->nbrVitesse = $nbrVitesse;

        return $this;
    }

    /**
     * Get nbrVitesse
     *
     * @return integer 
     */
    public function getNbrVitesse()
    {
        return $this->nbrVitesse;
    }

    /**
     * Set annee
     *
     * @param integer $annee
     * @return Voiture
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee
     *
     * @return integer 
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set nbrPorte
     *
     * @param integer $nbrPorte
     * @return Voiture
     */
    public function setNbrPorte($nbrPorte)
    {
        $this->nbrPorte = $nbrPorte;

        return $this;
    }

    /**
     * Get nbrPorte
     *
     * @return integer 
     */
    public function getNbrPorte()
    {
        return $this->nbrPorte;
    }

    /**
     * Add pieces
     *
     * @param \CERP\ClientBundle\Entity\Piece $pieces
     * @return Voiture
     */
    public function addPiece(\CERP\ClientBundle\Entity\Piece $pieces)
    {
        $this->pieces[] = $pieces;

        return $this;
    }

    /**
     * Remove pieces
     *
     * @param \CERP\ClientBundle\Entity\Piece $pieces
     */
    public function removePiece(\CERP\ClientBundle\Entity\Piece $pieces)
    {
        $this->pieces->removeElement($pieces);
    }

    /**
     * Get pieces
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPieces()
    {
        return $this->pieces;
    }

    /**
     * Add equipements
     *
     * @param \CERP\ClientBundle\Entity\Equipement $equipements
     * @return Voiture
     */
    public function addEquipement(\CERP\ClientBundle\Entity\Equipement $equipements)
    {
        $this->equipements[] = $equipements;

        $equipements->setVoiture($this); // set the relation on the owning-side

        return $this;
    }

    /**
     * Remove equipements
     *
     * @param \CERP\ClientBundle\Entity\Equipement $equipements
     */
    public function removeEquipement(\CERP\ClientBundle\Entity\Equipement $equipements)
    {
        $this->equipements->removeElement($equipements);
    }

    /**
     * Get equipements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEquipements()
    {
        return $this->equipements;
    }

    /**
     * Set energie
     *
     * @param \CERP\ClientBundle\Entity\Energie $energie
     * @return Voiture
     */
    public function setEnergie(\CERP\ClientBundle\Entity\Energie $energie = null)
    {
        $this->energie = $energie;

        return $this;
    }

    /**
     * Get energie
     *
     * @return \CERP\ClientBundle\Entity\Energie 
     */
    public function getEnergie()
    {
        return $this->energie;
    }

    /**
     * Set boitevitesse
     *
     * @param \CERP\ClientBundle\Entity\BoiteVitesse $boitevitesse
     * @return Voiture
     */
    public function setBoitevitesse(\CERP\ClientBundle\Entity\BoiteVitesse $boitevitesse = null)
    {
        $this->boitevitesse = $boitevitesse;

        return $this;
    }

    /**
     * Get boitevitesse
     *
     * @return \CERP\ClientBundle\Entity\BoiteVitesse 
     */
    public function getBoitevitesse()
    {
        return $this->boitevitesse;
    }

    /**
     * Set typeVehicule
     *
     * @param \CERP\ClientBundle\Entity\TypeVehicule $typeVehicule
     * @return Vehicule
     */
    public function setTypeVehicule(\CERP\ClientBundle\Entity\TypeVehicule $typeVehicule = null)
    {
        $this->typeVehicule = $typeVehicule;

        return $this;
    }

    /**
     * Get typeVehicule
     *
     * @return \CERP\ClientBundle\Entity\TypeVehicule 
     */
    public function getTypeVehicule()
    {
        return $this->typeVehicule;
    }

    /**
     * Add locations
     *
     * @param \CERP\ClientBundle\Entity\Location $locations
     * @return Voiture
     */
    public function addLocation(\CERP\ClientBundle\Entity\Location $locations)
    {
        $this->locations[] = $locations;

        return $this;
    }

    /**
     * Remove locations
     *
     * @param \CERP\ClientBundle\Entity\Location $locations
     */
    public function removeLocation(\CERP\ClientBundle\Entity\Location $locations)
    {
        $this->locations->removeElement($locations);
    }

    /**
     * Get locations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Set prixLocation
     *
     * @param float $prixLocation
     * @return Voiture
     */
    public function setPrixLocation($prixLocation)
    {
        $this->prixLocation = $prixLocation;

        return $this;
    }

    /**
     * Get prixLocation
     *
     * @return float 
     */
    public function getPrixLocation()
    {
        return $this->prixLocation;
    }

    /**
     * Set prixVoiture
     *
     * @param float $prixVoiture
     * @return Voiture
     */
    public function setPrixVoiture($prixVoiture)
    {
        $this->prixVoiture = $prixVoiture;

        return $this;
    }

    /**
     * Get prixVoiture
     *
     * @return float 
     */
    public function getPrixVoiture()
    {
        return $this->prixVoiture;
    }

    /**
     * Set user
     *
     * @param \CERP\UserBundle\Entity\User $user
     * @return Voiture
     */
    public function setUser(\CERP\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CERP\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set chassis
     *
     * @param string $chassis
     * @return Voiture
     */
    public function setChassis($chassis)
    {
        $this->chassis = $chassis;

        return $this;
    }

    /**
     * Get chassis
     *
     * @return string 
     */
    public function getChassis()
    {
        return $this->chassis;
    }

    /**
     * Set modele
     *
     * @param string $modele
     * @return Voiture
     */
    public function setModele($modele)
    {
        $this->modele = $modele;

        return $this;
    }

    /**
     * Get modele
     *
     * @return string 
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     * @return Voiture
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean 
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set kilometrage
     *
     * @param integer $kilometrage
     * @return Voiture
     */
    public function setKilometrage($kilometrage)
    {
        $this->kilometrage = $kilometrage;

        return $this;
    }

    /**
     * Get kilometrage
     *
     * @return integer 
     */
    public function getKilometrage()
    {
        return $this->kilometrage;
    }

    /**
     * Set marque
     *
     * @param \CERP\ClientBundle\Entity\Marque $marque
     * @return Voiture
     */
    public function setMarque(\CERP\ClientBundle\Entity\Marque $marque = null)
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get marque
     *
     * @return \CERP\ClientBundle\Entity\Marque 
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     * @return Voiture
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime 
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateMiseEnCirculation
     *
     * @param \DateTime $dateMiseEnCirculation
     * @return Voiture
     */
    public function setDateMiseEnCirculation($dateMiseEnCirculation)
    {
        $this->dateMiseEnCirculation = $dateMiseEnCirculation;

        return $this;
    }

    /**
     * Get dateMiseEnCirculation
     *
     * @return \DateTime 
     */
    public function getDateMiseEnCirculation()
    {
        return $this->dateMiseEnCirculation;
    }

    /**
     * Set vidange
     *
     * @param integer $vidange
     * @return Voiture
     */
    public function setVidange($vidange)
    {
        $this->vidange = $vidange;

        return $this;
    }

    /**
     * Get vidange
     *
     * @return integer 
     */
    public function getVidange()
    {
        return $this->vidange;
    }

    /**
     * Set dateAssurance
     *
     * @param \DateTime $dateAssurance
     * @return Voiture
     */
    public function setDateAssurance($dateAssurance)
    {
        $this->dateAssurance = $dateAssurance;

        return $this;
    }

    /**
     * Get dateAssurance
     *
     * @return \DateTime 
     */
    public function getDateAssurance()
    {
        return $this->dateAssurance;
    }


    /**
     * Set dateVisiteTechnique
     *
     * @param \DateTime $dateVisiteTechnique
     * @return Voiture
     */
    public function setDateVisiteTechnique($dateVisiteTechnique)
    {
        $this->dateVisiteTechnique = $dateVisiteTechnique;

        return $this;
    }

    /**
     * Get dateVisiteTechnique
     *
     * @return \DateTime 
     */
    public function getDateVisiteTechnique()
    {
        return $this->dateVisiteTechnique;
    }


    /**
     * Set disponibleEchange
     *
     * @param boolean $disponibleEchange
     * @return Voiture
     */
    public function setDisponibleEchange($disponibleEchange)
    {
        $this->disponibleEchange = $disponibleEchange;

        return $this;
    }

    /**
     * Get disponibleEchange
     *
     * @return boolean 
     */
    public function getDisponibleEchange()
    {
        return $this->disponibleEchange;
    }

    /**
     * Set disponibilite
     *
     * @param boolean $disponibilite
     *
     * @return Voiture
     */
    public function setDisponibilite($disponibilite)
    {
        $this->disponibilite = $disponibilite;

        return $this;
    }

    /**
     * Get disponibilite
     *
     * @return boolean
     */
    public function getDisponibilite()
    {
        return $this->disponibilite;
    }

    /**
     * Set prixLocationB2B
     *
     * @param float $prixLocationB2B
     *
     * @return Voiture
     */
    public function setPrixLocationB2B($prixLocationB2B)
    {
        $this->prixLocationB2B = $prixLocationB2B;

        return $this;
    }

    /**
     * Get prixLocationB2B
     *
     * @return float
     */
    public function getPrixLocationB2B()
    {
        return $this->prixLocationB2B;
    }
}
