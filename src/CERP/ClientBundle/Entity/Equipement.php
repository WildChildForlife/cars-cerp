<?php

namespace CERP\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Equipement
 *
 * @ORM\Table(name="Equipement")
 * @ORM\Entity(repositoryClass="CERP\ClientBundle\Entity\Repository\EquipementRepository")
 * @UniqueEntity(
 *     fields = "libelle", 
 *     message = "Cet équipement existe déjà")
 */
class Equipement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="Presence", type="string", length=255, nullable=true)
     */
    private $presence;

    /** @ORM\ManyToOne(targetEntity="Voiture", inversedBy="equipements") */
    protected $voiture;


    public function __toString()
    {
        return $this->getLibelle();
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Equipement
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set presence
     *
     * @param string $presence
     * @return Equipement
     */
    public function setPresence($presence)
    {
        $this->presence = $presence;

        return $this;
    }

    /**
     * Get presence
     *
     * @return string 
     */
    public function getPresence()
    {
        return $this->presence;
    }

    /**
     * Set voiture
     *
     * @param \CERP\ClientBundle\Entity\Voiture $voiture
     * @return Equipement
     */
    public function setVoiture(\CERP\ClientBundle\Entity\Voiture $voiture = null)
    {
        $this->voiture = $voiture;

        return $this;
    }

    /**
     * Get voiture
     *
     * @return \CERP\ClientBundle\Entity\Voiture 
     */
    public function getVoiture()
    {
        return $this->voiture;
    }
}
