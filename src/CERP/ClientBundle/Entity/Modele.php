<?php

namespace CERP\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Modele
 *
 * @ORM\Table(name="Modele")
 * @ORM\Entity(repositoryClass="CERP\ClientBundle\Entity\Repository\ModeleRepository")
 */
class Modele
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity="CERP\ClientBundle\Entity\Vehicule", mappedBy="marque" , cascade="all")
     */
    protected $voitures;


    public function __toString()
    {
        return $this->libelle;
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Modele
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Add vehicules
     *
     * @param \CERP\ClientBundle\Entity\Voiture $vehicules
     * @return Marque
     */
    public function addVoiture(\CERP\ClientBundle\Entity\Voiture $vehicules)
    {
        $this->vehicules[] = $vehicules;

        return $this;
    }

    /**
     * Remove vehicules
     *
     * @param \CERP\ClientBundle\Entity\Voiture $vehicules
     */
    public function removeVoiture(\CERP\ClientBundle\Entity\Voiture $vehicules)
    {
        $this->voitures->removeElement($vehicules);
    }

    /**
     * Get vehicules
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVoitures()
    {
        return $this->vehicules;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->voitures = new \Doctrine\Common\Collections\ArrayCollection();
    }
}
