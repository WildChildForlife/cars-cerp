<?php

namespace CERP\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Marque
 *
 * @ORM\Table(name="Marque")
 * @ORM\Entity(repositoryClass="CERP\ClientBundle\Entity\Repository\MarqueRepository")
 */
class Marque
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity="CERP\ClientBundle\Entity\Voiture", mappedBy="marque" , cascade="all")
     */
    protected $voitures;

    /** @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade="all") */
    protected $media;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
   {
       return $this->libelle;
   }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Marque
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vehicules = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add vehicules
     *
     * @param \CERP\ClientBundle\Entity\Voiture $vehicules
     * @return Marque
     */
    public function addVoiture(\CERP\ClientBundle\Entity\Voiture $vehicules)
    {
        $this->vehicules[] = $vehicules;

        return $this;
    }

    /**
     * Remove vehicules
     *
     * @param \CERP\ClientBundle\Entity\Voiture $vehicules
     */
    public function removeVoiture(\CERP\ClientBundle\Entity\Voiture $vehicules)
    {
        $this->voitures->removeElement($vehicules);
    }

    /**
     * Get vehicules
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVoitures()
    {
        return $this->vehicules;
    }

    /**
     * Set media
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $media
     * @return Modele
     */
    public function setMedia(\Application\Sonata\MediaBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media 
     */
    public function getMedia()
    {
        return $this->media;
    }
}
