<?php

namespace CERP\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BlackList
 *
 * @ORM\Table(name="BlackList")
 * @ORM\Entity(repositoryClass="CERP\ClientBundle\Entity\Repository\BlackListRepository")
 */
class BlackList
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Incident", cascade={"persist"})
     */
    protected $incidents;


    /**
    * @ORM\OneToOne(targetEntity="Locataire", cascade="all", inversedBy="blacklist")
    */
    private $locataire;

    
    // public function __toString()
    //    {
    //        return $this->incident;
    //    }   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Remove incidents
     *
     * @param \CERP\ClientBundle\Entity\Incident $incidents
     */
    public function removeIncident(\CERP\ClientBundle\Entity\Incident $incidents)
    {
        $this->incidents->removeElement($incidents);
    }

    /**
     * Get incidents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIncidents()
    {
        return $this->incidents;
    }

    /**
     * Add incidents
     *
     * @param \CERP\ClientBundle\Entity\Incident $incidents
     * @return BlackList
     */
    public function addIncident(\CERP\ClientBundle\Entity\Incident $incidents)
    {
        $this->incidents[] = $incidents;

        return $this;
    }

    public function deleteEmptyIncidents()
    {
        foreach ($this->getIncidents() as $iKey => $sValue) 
        {
            if ( empty($sValue->getDescription()) ) $this->removeIncident($sValue);   
        }
        return $this;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->incidents = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set locataire
     *
     * @param \CERP\ClientBundle\Entity\Locataire $locataire
     * @return BlackList
     */
    public function setLocataire(\CERP\ClientBundle\Entity\Locataire $locataire = null)
    {
        $this->locataire = $locataire;

        return $this;
    }

    /**
     * Get locataire
     *
     * @return \CERP\ClientBundle\Entity\Locataire 
     */
    public function getLocataire()
    {
        return $this->locataire;
    }
}
