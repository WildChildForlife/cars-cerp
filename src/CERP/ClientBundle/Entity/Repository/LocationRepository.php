<?php

namespace CERP\ClientBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * LocationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LocationRepository extends EntityRepository
{

// 		public function getLocationActif()
// 	{
// 		$locations = $this->createQueryBuilder('locations')
//             ->where('locations.actif = true')                                
//             ->getQuery();
//         $res = $locations->getResult();

//         return $res;
		
// 	}



public function findVoiturelouee($user, $currentDate)
{
    $q = $this
        ->createQueryBuilder('v')
        ->where('v.dateLocation <= :currentDate')
        ->andWhere('v.dateRetour >= :currentDate')
        ->andWhere('v.user = :user')
        ->setParameter('user', $user)
        ->setParameter('currentDate', $currentDate)
        ->getQuery();         

    return $q->getResult();
}

public function getDateDiff($dateLocation, $dateRetour)
{

    $oQuery = $this
        ->createQueryBuilder('v')
        ->select('DATEDIFF(v.dateLocation, v.dateRetour)')
        ->getQuery();         

    return $oQuery->getResult();
}



}
