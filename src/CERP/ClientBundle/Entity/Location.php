<?php

namespace CERP\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Location
 *
 * @ORM\Table(name="Location")
 * @ORM\Entity(repositoryClass="CERP\ClientBundle\Entity\Repository\LocationRepository")
 */
class Location
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Libelle", type="string", length=255, nullable=true)
     */
    private $libelle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enCours", type="boolean")
     */
    private $enCours = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateRetour", type="datetime")
     */
    private $dateRetour;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateLocation", type="datetime")
     */
    private $dateLocation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation", type="datetime")
     */
    private $dateCreation;


    /** @ORM\ManyToOne(targetEntity="Voiture", inversedBy="locations") */
    protected $voiture;


    /** @ORM\ManyToOne(targetEntity="Locataire", inversedBy="locations") */
    protected $locataire;

    /** @ORM\ManyToOne(targetEntity="CERP\UserBundle\Entity\User", inversedBy="locations") */
    protected $user; // MASERATI





    public function __construct()
    {
        $this->setDateCreation(new \DateTime());
        $this->setDateLocation(new \DateTime());
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Location
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set voiture
     *
     * @param \CERP\ClientBundle\Entity\Voiture $voiture
     * @return Location
     */
    public function setVoiture(\CERP\ClientBundle\Entity\Voiture $voiture = null)
    {
        $this->voiture = $voiture;

        return $this;
    }

    /**
     * Get voiture
     *
     * @return \CERP\ClientBundle\Entity\Voiture 
     */
    public function getVoiture()
    {
        return $this->voiture;
    }

    /**
     * Set locataire
     *
     * @param \CERP\ClientBundle\Entity\Locataire $locataire
     * @return Location
     */
    public function setLocataire(\CERP\ClientBundle\Entity\Locataire $locataire = null)
    {
        $this->locataire = $locataire;

        return $this;
    }

    /**
     * Get locataire
     *
     * @return \CERP\ClientBundle\Entity\Locataire 
     */
    public function getLocataire()
    {
        return $this->locataire;
    }



    /**
     * Set user
     *
     * @param \CERP\UserBundle\Entity\User $user
     * @return Location
     */
    public function setUser(\CERP\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CERP\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     * @return Location
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime 
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateRetour
     *
     * @param \DateTime $dateRetour
     * @return Location
     */
    public function setDateRetour($dateRetour)
    {
        $this->dateRetour = $dateRetour;

        return $this;
    }

    /**
     * Get dateRetour
     *
     * @return \DateTime 
     */
    public function getDateRetour()
    {
        return $this->dateRetour;
    }

    /**
     * Set dateLocation
     *
     * @param \DateTime $dateLocation
     * @return Location
     */
    public function setDateLocation($dateLocation)
    {
        $this->dateLocation = $dateLocation;

        return $this;
    }

    /**
     * Get dateLocation
     *
     * @return \DateTime 
     */
    public function getDateLocation()
    {
        return $this->dateLocation;
    }

    /**
     * Set enCours
     *
     * @param boolean $enCours
     *
     * @return Location
     */
    public function setEnCours($enCours)
    {
        $this->enCours = $enCours;

        return $this;
    }

    /**
     * Get enCours
     *
     * @return boolean
     */
    public function getEnCours()
    {
        return $this->enCours;
    }
}
