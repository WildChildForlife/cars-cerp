<?php

namespace CERP\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CERP\ClientBundle\Entity\BlackList;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Locataire
 *
 * @ORM\Table(name="Locataire")
 * @ORM\Entity(repositoryClass="CERP\ClientBundle\Entity\Repository\LocataireRepository")
 * @UniqueEntity(
 *     fields = "cin", 
 *     message = "Ce locataire existe déjà sous ce numéro de CIN")
 * @UniqueEntity(
 *     fields = "permis",
 *     message = "Ce locataire existe déjà sous ce numéro de Permis")
 */
class Locataire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="actif", type="boolean", nullable=true)
     */
    private $actif=true;


    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="CIN", type="string", length=255, unique=true)
     */
    private $cin;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="permis", type="string", length=255, unique=true)
     */
    private $permis;

    /**
     * @var string
     *
     * @ORM\Column(name="Adresse", type="string", length=255)
     */
    private $adresse;


    /**
     * @ORM\OneToOne(targetEntity="CERP\ClientBundle\Entity\BlackList", mappedBy="locataire" , cascade="all")
     */
    protected $blacklist;

    /**
     * @ORM\OneToMany(targetEntity="CERP\ClientBundle\Entity\Location", mappedBy="locataire" , cascade="all")
     */
    protected $locations;

    /** @ORM\ManyToOne(targetEntity="CERP\UserBundle\Entity\User", inversedBy="locataires") */
    protected $user;


    /**
     * Constructor
     */
    public function __construct()
    {       
        $this->blacklist = new BlackList();
    }

    public function __toString()
   {
       return $this->nom;
   }  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Locataire
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Locataire
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set cin
     *
     * @param string $cin
     * @return Locataire
     */
    public function setCIN($cin)
    {
        $this->cin = $cin;

        return $this;
    }

    /**
     * Get cin
     *
     * @return string 
     */
    public function getCIN()
    {
        return $this->cin;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Locataire
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set permis
     *
     * @param string $permis
     * @return Locataire
     */
    public function setPermis($permis)
    {
        $this->permis = $permis;

        return $this;
    }

    /**
     * Get permis
     *
     * @return string 
     */
    public function getPermis()
    {
        return $this->permis;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Locataire
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }


    /**
     * Add locations
     *
     * @param \CERP\ClientBundle\Entity\Location $locations
     * @return Locataire
     */
    public function addLocation(\CERP\ClientBundle\Entity\Location $locations)
    {
        $this->locations[] = $locations;

        return $this;
    }

    /**
     * Remove locations
     *
     * @param \CERP\ClientBundle\Entity\Location $locations
     */
    public function removeLocation(\CERP\ClientBundle\Entity\Location $locations)
    {
        $this->locations->removeElement($locations);
    }

    /**
     * Get locations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Set blacklist
     *
     * @param \CERP\ClientBundle\Entity\BlackList $blacklist
     * @return Locataire
     */
    public function setBlacklist(\CERP\ClientBundle\Entity\BlackList $blacklist = null)
    {
        $this->blacklist = $blacklist;

        return $this;
    }

    /**
     * Get blacklist
     *
     * @return \CERP\ClientBundle\Entity\BlackList 
     */
    public function getBlacklist()
    {
        return $this->blacklist;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     * @return Locataire
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean 
     */
    public function getActif()
    {
        return $this->actif;
    }


    /**
     * Set user
     *
     * @param \CERP\UserBundle\Entity\User $user
     * @return Locataire
     */
    public function setUser(\CERP\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CERP\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
