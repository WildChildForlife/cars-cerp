<?php

namespace CERP\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use CERP\ClientBundle\Entity\Voiture;
use CERP\ClientBundle\Entity\HistoriqueDispoVoiture;
use CERP\UserBundle\Entity\User;
use CERP\ClientBundle\Entity\Repository\BlackListRepository;
use CERP\ClientBundle\Form\VoitureType;
use Symfony\Component\HttpFoundation\RedirectResponse;



class VoitureController extends Controller
{

	public function createAction(Request $oRequest)
    {
        $oVoiture = new Voiture();        
        $oFormVoiture = new VoitureType();

        $oFormVoiture = $this->createForm($oFormVoiture, $oVoiture);
        $oFormVoiture->handleRequest($oRequest);

        $oEm = $this->getDoctrine()->getManager();

        $oMarque = $oEm->getRepository('CERPClientBundle:Marque')->find((int)$oRequest->get('marque-select'));



        if ( $oFormVoiture->isValid() && is_object($oMarque) )
        {
            // $a = $oRequest->get('marques-select');
            // ladybug_dump($a);
            // die();

            $oVoiture->setMarque($oMarque);

            $oUser = $this->get('security.context')->getToken()->getUser();
            $iUserId = $oUser->getId();
            $oUser = $oEm->getRepository('CERPUserBundle:User')->find($iUserId);

            $oVoiture->setUser($oUser);

            $oEm->persist($oVoiture);
            $oEm->persist($oMarque);
            $oEm->flush();

            return new RedirectResponse($this->get('globalfunctions')->returnToClicked(
                                            $oRequest, 
                                            'CERP_client_voiture'));    

        }

        $aMarques = $this->getDoctrine()->getRepository('CERPClientBundle:Marque')->findAll();

        return $this->render('CERPClientBundle:Pages/Voiture:add-edit-voiture.html.twig', array(
                "oFormVoiture" => $oFormVoiture->createView(),
                "aMarques" => $aMarques
            )
        );  

    }


    public function editAction(Request $oRequest, $id)
    {
        $oEm = $this->getDoctrine()->getManager();

        $oVoiture = $oEm->getRepository('CERPClientBundle:Voiture')->find($id);
        $iMarqueID = ( is_object($oVoiture->getMarque()) ) ? $oVoiture->getMarque()->getId() : null;

        
        $oFormVoiture = $this->createForm(new VoitureType(), $oVoiture);        
        
        if ($oRequest->getMethod() == 'POST') 
        {
            // if ($oFormVoiture->get('send-addnew')->isClicked()) {
            //     die('yes');
            // }
            // else die('no');
            $oFormVoiture->handleRequest($oRequest);            

            $oMarque = $oEm->getRepository('CERPClientBundle:Marque')->find((int)$oRequest->get('marque-select'));

            if ( $oFormVoiture->isValid() && is_object($oMarque) )
            {
                $oVoiture->setMarque($oMarque);

                //$oVoiture->setVoiture($oVoiture);                
                
                $oEm->persist($oVoiture);
                $oEm->persist($oMarque);
                $oEm->flush();

                return new RedirectResponse($this->get('globalfunctions')->returnToClicked(
                                                $oRequest, 
                                                'CERP_client_voiture', 
                                                array('id' => $id)));    
                
            }
        }

        $aMarques = $this->getDoctrine()->getRepository('CERPClientBundle:Marque')->findAll();

        return $this->render('CERPClientBundle:Pages/Voiture:add-edit-voiture.html.twig', array(            
            'oFormVoiture' => $oFormVoiture->createView(),
            "aMarques" => $aMarques,
            'iMarqueID' => $iMarqueID
        ));
    }


    public function deleteAction($id)
    {
        $oEm = $this->getDoctrine()->getManager();

        $oVoiture = $oEm->getRepository('CERPClientBundle:Voiture')->find($id);        
      
        $oEm->remove($oVoiture);
        $oEm->flush();
         
        return $this->redirect($this->generateUrl('CERP_client_voiture_list'));
    }


    public function listAction()
    {

        $oEm = $this->getDoctrine()->getManager();
        $oUser = $this->get('security.context')->getToken()->getUser();
        $iUserID = $oUser->getId();        
        $aListVoitures = $oEm->getRepository('CERPClientBundle:Voiture')->findBy(array('user' => $iUserID));

        $aListVoitures = ( count($aListVoitures) == 0 ) ? new Voiture() : $aListVoitures;

        return $this->render('CERPClientBundle:Pages/Voiture:list-voiture.html.twig', array(
            'aListVoitures' => $aListVoitures ));
    }

    public function listMesVoitureDispoLocationAction()
    {

        $oEm = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();        
        $aListVoitures = $oEm->getRepository('CERPClientBundle:Voiture')->findBy(array('user' => $userId, 'disponibilite' => true));

        $aListVoitures = ( count($aListVoitures) == 0 ) ? new Voiture() : $aListVoitures;


        return $this->render('CERPClientBundle:Pages/Voiture:list-mes-voiture-dispo-location.html.twig', array(
            'aListVoitures' => $aListVoitures ));
    }

    public function listMesVoitureNonDispoLocationAction()
    {

        $oEm = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();        
        $aListVoitures = $oEm->getRepository('CERPClientBundle:Voiture')->findBy(array('user' => $userId, 'disponibilite' => false));

        $aListVoitures = ( count($aListVoitures) == 0 ) ? new Voiture() : $aListVoitures;


        return $this->render('CERPClientBundle:Pages/Voiture:list-mes-voiture-non-dispo-location.html.twig', array(
            'aListVoitures' => $aListVoitures ));
    }     

    public function listMesVoitureDisponibleEchangeAction()
    {

        $oEm = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();        
        $aListVoitures = $oEm->getRepository('CERPClientBundle:Voiture')->findBy(array('user' => $userId, 'disponibleEchange'=> true));

        $aListVoitures = ( count($aListVoitures) == 0 ) ? new Voiture() : $aListVoitures;


        return $this->render('CERPClientBundle:Pages/Voiture:list-mes-voiture-dispo-echange.html.twig', array(
            'aListVoitures' => $aListVoitures ));
    }

    public function ListVoitureDisponibleEchangeAction()
    {

        $oEm = $this->getDoctrine()->getManager();      
        $aListVoitures = $oEm->getRepository('CERPClientBundle:Voiture')->findBy(array('disponibleEchange'=> true));

        $aListVoitures = ( count($aListVoitures) == 0 ) ? new Voiture() : $aListVoitures;


        return $this->render('CERPClientBundle:Pages/Voiture:list-voiture-dispo-echange.html.twig', array(
            'aListVoitures' => $aListVoitures ));
    }    


    // public function editDisponibleEchangeAction($id, $bValue)
    // {

    //     if (is_integer($id) and is_bool($bValue) )
    //     {
    //         $oEm = $this->getDoctrine()->getManager();

    //         $oVoiture = $oEm->getRepository('CERPClientBundle:Voiture')->find($id);
            
    //         $oUser = $this->get('security.context')->getToken()->getUser();

    //         $bSuccess = false;

    //         if($oVoiture != null)
    //         {
    //             $oHistoriqueDispoVoiture = new HistoriqueDispoVoiture();
    //             $oHistoriqueDispoVoiture->setValue($bValue);
    //             $oHistoriqueDispoVoiture->setUser($oUser);
    //             $oHistoriqueDispoVoiture->setVoiture($oVoiture);

    //             $oVoiture->setDisponibleEchange($bValue);

    //             $oEm->persist($oHistoriqueDispoVoiture);
    //             $oEm->persist($oVoiture);            
    //             $oEm->flush();

    //             $bSuccess = true;
    //         }   
    //     }

    //     else $bSuccess = false; 
        
    //     return new JsonResponse(array('success' => $bSuccess)); 
       
    // }

}