<?php

namespace CERP\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use CERP\ClientBundle\Entity\Location;
use CERP\ClientBundle\Entity\Voiture;
use CERP\ClientBundle\Entity\Locataire;
use CERP\ClientBundle\Entity\BlackList;
use CERP\ClientBundle\Form\LocationType;
use CERP\ClientBundle\Form\LocataireType;
use CERP\ClientBundle\Form\VoitureType;

use Symfony\Component\HttpFoundation\RedirectResponse;


class LocationController extends Controller
{

	public function createAction(Request $oRequest)
    {
        $oEm = $this->getDoctrine()->getManager();
        $oUser = $this->get('security.context')->getToken()->getUser();
        $iUserID = $oUser->getId(); 

        //Get Current User Voitures
        $aListVoitures = $oEm->getRepository('CERPClientBundle:Voiture')->findBy(array('actif' => true, 'user' => $iUserID));
        //Get Current User Locataires
        $aListLocataires = $oEm->getRepository('CERPClientBundle:Locataire')->findBy(array('actif' => true, 'user' => $iUserID));

        $oVoiture = new Voiture();
        $oFormVoiture = new VoitureType();
        $oFormVoiture = $this->createForm($oFormVoiture, $oVoiture);

        $oLocataire = new Locataire();
        $oFormLocataire = new LocataireType();
        $oFormLocataire = $this->createForm($oFormLocataire, $oLocataire);
        //Fin Get Current User Voitures

        $oLocation = new Location();
        $oFormLocation = new LocationType($oUser);
               

        $oFormLocation = $this->createForm($oFormLocation, $oLocation);
        $oFormLocation->handleRequest($oRequest);

        if ( $oFormLocation->isValid() ) 
        {
            $oEm = $this->getDoctrine()->getManager();

            $oUser = $this->get('security.context')->getToken()->getUser();
            $iUserId = $oUser->getId();
            $oUser = $oEm->getRepository('CERPUserBundle:User')->find($iUserId);

            $oLocation->setUser($oUser);

            $oLocation->setActif(true);          
            $oEm->persist($oLocation);
            $oEm->flush();

            return new RedirectResponse($this->get('globalfunctions')->returnToClicked(
                                            $oRequest, 
                                            'CERP_client_location'));    

        }

        return $this->render('CERPClientBundle:Pages/location:add-edit-location.html.twig', array(
            "oLocation" => $oLocation, 
            "oFormVoiture" => $oFormVoiture->createView(), 
            "oFormLocataire" => $oFormLocataire->createView(), 
            "oFormLocation" => $oFormLocation->createView(),
            "aListLocataires" => $aListLocataires)
        );

    }


    public function editAction(Request $oRequest, $id)
    {
        $oEm = $this->getDoctrine()->getEntityManager();

        $oLocation = $oEm->getRepository('CERPClientBundle:Location')->find($id);

        $oFormLocation = $this->createForm(new LocationType($this->get('security.context')->getToken()->getUser()), $oLocation);        
        
        if ($oRequest->getMethod() == 'POST') 
        {            
            $oFormLocation->handleRequest($oRequest);            

            if ( $oFormLocation->isValid())
            {                                    
                $oEm->persist($oLocation);
                $oEm->flush();

                return new RedirectResponse($this->get('globalfunctions')->returnToClicked(
                                            $oRequest, 
                                            'CERP_client_location', 
                                            array('id' => $id))); 
            }
        }


        return $this->render('CERPClientBundle:Pages/Location:add-edit-location.html.twig', array(            
            'oFormLocation' => $oFormLocation->createView()
        ));
        

    }


    public function deleteAction($id)
    { 

     $oEm = $this->getDoctrine()->getEntityManager();

        $oLocation = $oEm->getRepository('CERPClientBundle:Location')->find($id);
        $oLocation->setActif(false);
      
        $oEm->persist($oLocation);
        $oEm->flush();
         
        return $this->redirectToRoute('CERP_client_location_list');
    }


    public function listAction()
    {
        $oEm = $this->getDoctrine()->getEntityManager();
        $oUser = $this->get('security.context')->getToken()->getUser();
        $iUserId = $oUser->getId();        
        $aListLocations = $oEm->getRepository('CERPClientBundle:Location')->findBy(array('user' => $iUserId));

        $aListLocations = ( count($aListLocations) == 0 ) ? new Location() : $aListLocations;

        return $this->render('CERPClientBundle:Pages/Location:list-location.html.twig', array(
            'aListLocations' => $aListLocations ));

    }


    public function enCoursAction()
    {        
        $oEm = $this->getDoctrine()->getEntityManager();
        $oUser = $this->get('security.context')->getToken()->getUser();  

        $dCurrentDate = date('Y-m-d H:i:s'); 

        // ladybug_dump($currentDate);die();

        $aListLocations = $oEm->getRepository('CERPClientBundle:Location')->findVoiturelouee($oUser, $dCurrentDate);

        $aListLocations = ( count($aListLocations) == 0 ) ? new Location() : $aListLocations;


        return $this->render('CERPClientBundle:Pages/Location:list-location-encours.html.twig', array(
            'aListLocations' => $aListLocations ));
    }

    public function clotureAction($id)
    {
        $oEm = $this->getDoctrine()->getManager();      
        $oLocation = $oEm->getRepository('CERPClientBundle:Location')->findOneBy(array('id' => $id));

        $oLocation->setEnCours(false);

        $oEm->persist($oLocation);
        $oEm->flush();
       
        return $this->redirectToRoute('CERP_client_location_list');

    }     

}