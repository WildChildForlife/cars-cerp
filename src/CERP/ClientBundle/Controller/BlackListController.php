<?php

namespace CERP\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use CERP\ClientBundle\Entity\BlackList;
use CERP\ClientBundle\Form\BlackListType;

class BlackListController extends Controller
{

	 public function createAction(Request $request, $idlocataire)
    {
        $oBlackList = new BlackList();
        
        $oForm = new BlackListType(); 
        $oForm = $this->createForm($oForm,$oBlackList);
        $oForm->handleRequest($request);

        if ($oForm->isValid()) 
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($oBlackList);
            $em->flush();    
                  
        }

        return $this->render('CERPClientBundle:Pages\BlackList:add-blacklist.html.twig', array("oBlackList" => $oBlackList, "oForm" => $oForm->createView()));  

    }


      public function listAction()
    {
        $oEm = $this->getDoctrine()->getEntityManager();
        $aListBlackList = $oEm->getRepository('CERPClientBundle:BlackList')->findAll();

        $aListBlackList = ( count($aListBlackList) == 0 ) ? new BlackList() : $aListBlackList;


        return $this->render('CERPClientBundle:Pages/BlackList:list-blacklist.html.twig', array(
            'aListBlackList' => $aListBlackList ));
    }

}