<?php

namespace CERP\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CERP\ClientBundle\Entity\Voiture;

class AdminController extends Controller
{
    public function indexAction()
    {
    	$oEm = $this->getDoctrine()->getManager();

    	$oUser = $this->get('security.context')->getToken()->getUser();      
    	$aVoitures = $oEm->getRepository('CERPClientBundle:Voiture')->findBy(array('actif' => true, 'user' => $oUser));

    	$aLocataires = $oEm->getRepository('CERPClientBundle:Locataire')->findBy(array('actif' => true, 'user' => $oUser));

    	$aLocations = $oEm->getRepository('CERPClientBundle:Location')->findBy(array('enCours' => true, 'user' => $oUser));



        return $this->render('CERPClientBundle:Admin:dashboard.html.twig', array(
            'aVoitures' => $aVoitures,            
            'aLocataires' => $aLocataires,            
            'aLocations' => $aLocations,            
             ));        
    }
}
