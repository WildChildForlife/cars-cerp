<?php

namespace CERP\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use CERP\ClientBundle\Entity\Locataire;
use CERP\ClientBundle\Entity\BlackList;
use CERP\ClientBundle\Entity\Repository\BlackListRepository;
use CERP\ClientBundle\Form\LocataireType;
use CERP\ClientBundle\Form\BlackListType;

use Symfony\Component\HttpFoundation\RedirectResponse;


class LocataireController extends Controller
{

	public function createAction(Request $oRequest)
    {
        $oLocataire = new Locataire();
        $oFormLocataire = new LocataireType();

        $oFormLocataire = $this->createForm($oFormLocataire, $oLocataire);
        $oFormLocataire->handleRequest($oRequest);

        if ( $oFormLocataire->isValid() ) 
        {
            $oEm = $this->getDoctrine()->getManager();

            $user = $this->get('security.context')->getToken()->getUser();
            $userId = $user->getId();
            $oUser = $oEm->getRepository('CERPUserBundle:User')->find($userId);

            $oLocataire->setUser($oUser);


            $oLocataire->setActif(true);
            $oEm->persist($oLocataire);
            $oEm->flush();

            return new RedirectResponse($this->get('globalfunctions')->returnToClicked(
                                            $oRequest, 
                                            'CERP_client_locataire'));    
        }

        return $this->render('CERPClientBundle:Pages/Locataire:add-edit-locataire.html.twig', array(
            "oLocataire" => $oLocataire, 
            "oFormLocataire" => $oFormLocataire->createView())
        );  
    }


    public function editAction($id)
    {
        

        $oEm = $this->getDoctrine()->getEntityManager();

        $oLocataire = $oEm->getRepository('CERPClientBundle:Locataire')->find($id);

        $oBlackList = $oLocataire->getBlacklist();
        $oBlackList = ( $oBlackList == null ) ? new BlackList() : $oBlackList;

        // ladybug_dump($oLocataire);
        // ladybug_dump($oBlackList);
        //         die();
        
        $oFormLocataire = $this->createForm(new LocataireType(), $oLocataire);

        $oFormBlackList = $this->createForm(new BlackListType(), $oBlackList);


        $oRequest = $this->get('request');
        
        if ($oRequest->getMethod() == 'POST') 
        {
            
            $oFormLocataire->handleRequest($oRequest);
            $oFormBlackList->handleRequest($oRequest);

            if ( $oFormLocataire->isValid() && $oFormBlackList->isValid() )
            {

                $oBlackList->deleteEmptyIncidents();
                $oBlackList->setLocataire($oLocataire);
                $oLocataire->setBlackList($oBlackList);
                
                $oEm->persist($oLocataire);
                $oEm->flush();

                return new RedirectResponse($this->get('globalfunctions')->returnToClicked(
                                                $oRequest, 
                                                'CERP_client_locataire', 
                                                array('id' => $id)));    
            }
           
        }


        return $this->render('CERPClientBundle:Pages/Locataire:add-edit-locataire.html.twig', array(
            'oFormLocataire' => $oFormLocataire->createView(), 
            'oFormBlackList' => $oFormBlackList->createView()
        ));
    }


    public function deleteAction($id)
    {        

        $oEm = $this->getDoctrine()->getEntityManager();

        $oLocataire = $oEm->getRepository('CERPClientBundle:Locataire')->find($id);
        $oLocataire->setActif(false);
      
        $oEm->persist($oLocataire);
        $oEm->flush();
 

        return $this->redirect($this->generateUrl('CERP_client_locataire_list'));            
    }


    public function listAction()
    {
        $oEm = $this->getDoctrine()->getEntityManager();
       // $aListLocataires = $oEm->getRepository('CERPClientBundle:Locataire')->getLocataireActif();
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();
        $aListLocataires = $oEm->getRepository('CERPClientBundle:Locataire')->findBy(array('actif' => true, 'user' => $userId));

        $aListLocataires = ( count($aListLocataires) == 0 ) ? new Locataire() : $aListLocataires;


        return $this->render('CERPClientBundle:Pages/Locataire:list-locataire.html.twig', array(
            'aListLocataires' => $aListLocataires ));
    }

}