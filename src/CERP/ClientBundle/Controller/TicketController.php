<?php

namespace CERP\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use CERP\ClientBundle\Entity\Ticket;
use CERP\UserBundle\Entity\User;
use CERP\ClientBundle\Form\TicketType;



class TicketController extends Controller
{

	public function createAction(Request $oRequest)
    { 

        $oTicket = new Ticket();        
        $oFormTicket = new TicketType();

        $oFormTicket = $this->createForm($oFormTicket, $oTicket);
        $oFormTicket->handleRequest($oRequest);

        if ( $oFormTicket->isValid() ) 
        {

            $oEm = $this->getDoctrine()->getManager();
            $user = $this->get('security.context')->getToken()->getUser();
            $userId = $user->getId();
            $oUser = $oEm->getRepository('CERPUserBundle:User')->find($userId);

            $oTicket->setUser($oUser);

            $oEm->persist($oTicket);
            $oEm->flush();
            
            return $this->redirect($this->generateUrl('CERP_client_ticket_list'));

        }


        return $this->render('CERPClientBundle:Pages/Ticket:add-edit-ticket.html.twig', array(
            "oTicket" => $oTicket, 
            "oFormTicket" => $oFormTicket->createView())
        ); 

    }


    public function editAction($id)
    {

        $oEm = $this->getDoctrine()->getEntityManager();

        $oTicket = $oEm->getRepository('CERPClientBundle:Ticket')->find($id);

        
        $oFormTicket = $this->createForm(new TicketType(), $oTicket);        

        $oRequest = $this->get('request');
        
        if ($oRequest->getMethod() == 'POST') 
        {
            
            $oFormTicket->handleRequest($oRequest);            

            if ( $oFormTicket->isValid() )
            {                            
                
                $oEm->persist($oTicket);
                $oEm->flush();

                return $this->redirect($this->generateUrl('CERP_client_ticket_list'));
            }
        }


        return $this->render('CERPClientBundle:Pages/Ticket:add-edit-ticket.html.twig', array(            
            'oFormTicket' => $oFormTicket->createView()
        ));

    }


    // public function deleteAction($id)
    // {

    //     $oEm = $this->getDoctrine()->getEntityManager();

    //     $oTicket = $oEm->getRepository('CERPClientBundle:Ticket')->find($id);        
      
    //             $oEm->remove($oTicket);
    //             $oEm->flush();
         
    //             return $this->redirect($this->generateUrl('CERP_client_ticket_list'));        

    // }


    public function listAction()
    {

        $oEm = $this->getDoctrine()->getEntityManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();        
        $aListTickets = $oEm->getRepository('CERPClientBundle:Ticket')->findBy(array('user' => $userId));

        $aListTickets = ( count($aListTickets) == 0 ) ? new Ticket() : $aListTickets;


        return $this->render('CERPClientBundle:Pages/Ticket:list-ticket.html.twig', array(
            'aListTickets' => $aListTickets ));

    }

}