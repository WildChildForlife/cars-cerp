<?php

namespace CERP\UserBundle\Entity;

// use Sonata\UserBundle\Entity\BaseUser as BaseUser;
// use Application\Sonata\UserBundle\Entity\User as BaseUser;
// use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use FOS\UserBundle\Model\User as AbstractedUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\UserBundle\Model\UserInterface;

/**
 * @ORM\Entity(repositoryClass="CERP\UserBundle\Entity\Repository\UserRepository")
 * @ORM\Table(name="users")
 */
class User extends AbstractedUser implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(name="facebook_id", type="string", length=255, nullable=true) */
    protected $facebook_id;

    /** @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true) */
    protected $facebook_access_token;

    /** @ORM\Column(name="google_id", type="string", length=255, nullable=true) */
    protected $google_id;

    /** @ORM\Column(name="google_access_token", type="string", length=255, nullable=true) */
    protected $google_access_token;

    /** @ORM\Column(name="first_name", type="string", length=255, nullable=true) */
    protected $first_name;

    /** @ORM\Column(name="middle_name", type="string", length=255, nullable=true) */
    protected $middle_name;

    /** @ORM\Column(name="last_name", type="string", length=255, nullable=true) */
    protected $last_name;

    /** @ORM\Column(name="birthday", type="date", nullable=true) */
    protected $birthday;

    protected $roles = array('ROLE_USER');

  

   /** @ORM\Column(name="created_at", type="datetime", nullable=false) */
    protected $createdAt;

    /** @ORM\Column(name="updated_at", type="datetime", nullable=false) */
    protected $updatedAt;

    /** @ORM\Column(name="gender", type="string", length=1, nullable=true) */
    protected $gender = UserInterface::GENDER_UNKNOWN; // set the default to unknown

    /** @ORM\Column(name="locale", type="string", length=8, nullable=true) */
    protected $locale;

    /** @ORM\Column(name="timezone", type="string", length=64, nullable=true) */
    protected $timezone;

    /** @ORM\Column(name="phone", type="string", length=64, nullable=true) */
    protected $phone;

    /** @ORM\Column(name="twoStepVerificationCode", type="string", length=255, nullable=true) */
    protected $twoStepVerificationCode;

    /** @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade="all") */
    protected $media;

    /**
     * @ORM\OneToMany(targetEntity="CERP\ClientBundle\Entity\Locataire", mappedBy="user" , cascade="all")
     */
    protected $locataires;

    /**
     * @ORM\OneToMany(targetEntity="CERP\ClientBundle\Entity\Voiture", mappedBy="user" , cascade="all")
     */
    protected $voitures;

    /**
     * @ORM\OneToMany(targetEntity="CERP\ClientBundle\Entity\Ticket", mappedBy="user" , cascade="all")
     */
    protected $tickets;

    /**
     * @ORM\OneToMany(targetEntity="CERP\ClientBundle\Entity\Ticket", mappedBy="consultant" , cascade="all")
     */
    protected $ticketsTraites; 


    /**
     * @ORM\OneToMany(targetEntity="CERP\ClientBundle\Entity\Location", mappedBy="user" , cascade="all")
     */
    protected $locations;

    /**
     * @return array
     */
    public function getRealRoles()
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     */
    public function setRealRoles(array $roles)
    {
        $this->setRoles($roles);
    }

    /**
     * Returns the gender list.
     *
     * @return array
     */
    public static function getGenderList()
    {
        return array(
            UserInterface::GENDER_UNKNOWN => 'gender_unknown',
            UserInterface::GENDER_FEMALE  => 'gender_female',
            UserInterface::GENDER_MALE    => 'gender_male',
        );
    }

    /**
     * Sets the two-step verification code
     *
     * @param string $twoStepVerificationCode
     */
    public function setTwoStepVerificationCode($twoStepVerificationCode)
    {
        $this->twoStepVerificationCode = $twoStepVerificationCode;
    }

    /**
     * Returns the two-step verification code
     *
     * @return string
     */
    public function getTwoStepVerificationCode()
    {
        return $this->twoStepVerificationCode;
    }

    /**
     * Sets the creation date
     *
     * @param \DateTime|null $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt = null)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Returns the creation date
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Sets the last update date
     *
     * @param \DateTime|null $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt = null)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Returns the last update date
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Returns a string representation
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getUsername() ?: '-';
    }

    /**
     * @return string
     */
    public function getFullname()
    {
        return sprintf("%s %s %s", $this->getFirstname(), $this->getMiddleName(), $this->getLastname());
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set facebook_id
     *
     * @param string $facebookId
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebook_id = $facebookId;

        return $this;
    }

    /**
     * Get facebook_id
     *
     * @return string 
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * Set facebook_access_token
     *
     * @param string $facebookAccessToken
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebook_access_token = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebook_access_token
     *
     * @return string 
     */
    public function getFacebookAccessToken()
    {
        return $this->facebook_access_token;
    }

    /**
     * Set google_id
     *
     * @param string $googleId
     * @return User
     */
    public function setGoogleId($googleId)
    {
        $this->google_id = $googleId;

        return $this;
    }

    /**
     * Get google_id
     *
     * @return string 
     */
    public function getGoogleId()
    {
        return $this->google_id;
    }

    /**
     * Set google_access_token
     *
     * @param string $googleAccessToken
     * @return User
     */
    public function setGoogleAccessToken($googleAccessToken)
    {
        $this->google_access_token = $googleAccessToken;

        return $this;
    }

    /**
     * Get google_access_token
     *
     * @return string 
     */
    public function getGoogleAccessToken()
    {
        return $this->google_access_token;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->roles = array('ROLE_USER');
       
    }


    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set first_name
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get first_name
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set middle_name
     *
     * @param string $middleName
     * @return User
     */
    public function setMiddleName($middleName)
    {
        $this->middle_name = $middleName;

        return $this;
    }

    /**
     * Get middle_name
     *
     * @return string 
     */
    public function getMiddleName()
    {
        return $this->middle_name;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /** @ORM\PrePersist  */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /** @ORM\PreUpdate  */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return User
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string 
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     * @return User
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return string 
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set media
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $media
     * @return User
     */
    public function setMedia(\Application\Sonata\MediaBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media 
     */
    public function getMedia()
    {
        return $this->media;
    }




    /**
     * Add locataires
     *
     * @param \CERP\ClientBundle\Entity\Locataire $locataires
     * @return User
     */
    public function addLocataire(\CERP\ClientBundle\Entity\Locataire $locataires)
    {
        $this->locataires[] = $locataires;

        return $this;
    }

    /**
     * Remove locataires
     *
     * @param \CERP\ClientBundle\Entity\Locataire $locataires
     */
    public function removeLocataire(\CERP\ClientBundle\Entity\Locataire $locataires)
    {
        $this->locataires->removeElement($locataires);
    }

    /**
     * Get locataires
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocataires()
    {
        return $this->locataires;
    }

    /**
     * Add voitures
     *
     * @param \CERP\ClientBundle\Entity\Voiture $voitures
     * @return User
     */
    public function addVoiture(\CERP\ClientBundle\Entity\Voiture $voitures)
    {
        $this->voitures[] = $voitures;

        return $this;
    }

    /**
     * Remove voitures
     *
     * @param \CERP\ClientBundle\Entity\Voiture $voitures
     */
    public function removeVoiture(\CERP\ClientBundle\Entity\Voiture $voitures)
    {
        $this->voitures->removeElement($voitures);
    }

    /**
     * Get voitures
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVoitures()
    {
        return $this->voitures;
    }

    /**
     * Add locations
     *
     * @param \CERP\ClientBundle\Entity\Location $locations
     * @return User
     */
    public function addLocation(\CERP\ClientBundle\Entity\Location $locations)
    {
        $this->locations[] = $locations;

        return $this;
    }

    /**
     * Remove locations
     *
     * @param \CERP\ClientBundle\Entity\Location $locations
     */
    public function removeLocation(\CERP\ClientBundle\Entity\Location $locations)
    {
        $this->locations->removeElement($locations);
    }

    /**
     * Get locations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Add tickets
     *
     * @param \CERP\ClientBundle\Entity\Ticket $tickets
     * @return User
     */
    public function addTicket(\CERP\ClientBundle\Entity\Ticket $tickets)
    {
        $this->tickets[] = $tickets;

        return $this;
    }

    /**
     * Remove tickets
     *
     * @param \CERP\ClientBundle\Entity\Ticket $tickets
     */
    public function removeTicket(\CERP\ClientBundle\Entity\Ticket $tickets)
    {
        $this->tickets->removeElement($tickets);
    }

    /**
     * Get tickets
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * Add ticketsTraites
     *
     * @param \CERP\ClientBundle\Entity\Ticket $ticketsTraites
     * @return User
     */
    public function addTicketsTraite(\CERP\ClientBundle\Entity\Ticket $ticketsTraites)
    {
        $this->ticketsTraites[] = $ticketsTraites;

        return $this;
    }

    /**
     * Remove ticketsTraites
     *
     * @param \CERP\ClientBundle\Entity\Ticket $ticketsTraites
     */
    public function removeTicketsTraite(\CERP\ClientBundle\Entity\Ticket $ticketsTraites)
    {
        $this->ticketsTraites->removeElement($ticketsTraites);
    }

    /**
     * Get ticketsTraites
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTicketsTraites()
    {
        return $this->ticketsTraites;
    }
}
