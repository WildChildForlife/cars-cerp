<?php

namespace CERP\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // add your custom field
        $builder->add('plainPassword', 'password', array(
                'required' => true,
                'label' => array('label' => 'form.password', 'translation_domain' => 'FOSUserBundle'),
                'invalid_message' => 'fos_user.password.mismatch'
            ));
    }

    public function getParent()
    {
        return 'fos_user_registration';
    }

    public function getName()
    {
        return 'cerp_user_registration';
    }
}