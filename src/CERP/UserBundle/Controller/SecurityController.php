<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CERP\UserBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\Security\Core\SecurityContextInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;

class SecurityController extends BaseController
{
    public function loginAction(Request $oRequest)
    {
        // Si authentifié, rediriger vers le dashboard
        $oSecurityContext = $this->container->get('security.context');
        if ($oSecurityContext->isGranted('IS_AUTHENTICATED_FULLY')) 
        {
            //return new RedirectResponse($this->container->get('router')->generate('cerp_site_dashboard'));
        }
        // REGISTRATION
        $oFormFactory = $this->container->get('fos_user.registration.form.factory');
        $oUserManager = $this->container->get('fos_user.user_manager');
        $oDispatcher = $this->container->get('event_dispatcher');

        $oUser = $oUserManager->createUser();

        $oUser->setEnabled(true);

        $oEvent = new GetResponseUserEvent($oUser, $oRequest);
        $oDispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $oEvent);

        if (null !== $oEvent->getResponse()) {
            return $oEvent->getResponse();
        }

        $oFormRegistration = $oFormFactory->createForm();
        $oFormRegistration->setData($oUser);


        if ('POST' === $oRequest->getMethod()) {

            $oFormRegistration->bind($oRequest);

            if ($oFormRegistration->isValid()) {
                $oEvent = new FormEvent($oFormRegistration, $oRequest);
                $oDispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $oEvent);

                $oUserManager->updateUser($oUser);

                if (null === $oResponse = $oEvent->getResponse()) {
                    $sUrl = $this->container->get('router')->generate('fos_user_registration_confirmed');
                    $oResponse = new RedirectResponse($sUrl);
                }

                $oDispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($oUser, $oRequest, $oResponse));

                return $oResponse;
            }
        }

        // LOGIN
        $oSession = $oRequest->getSession();

        // get the error if any (works with forward and redirect -- see below)
        if ($oRequest->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $aErrors = $oRequest->attributes->get(SecurityContextInterface::AUTHENTICATION_ERROR);
        } elseif (null !== $oSession && $oSession->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $aErrors = $oSession->get(SecurityContextInterface::AUTHENTICATION_ERROR);
            $oSession->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        } else {
            $aErrors = '';
        }

        if ($aErrors) {
            // TODO: this is a potential security risk (see http://trac.symfony-project.org/ticket/9523)
            $aErrors = $aErrors->getMessage();
        }
        // last username entered by the user
        $lastUsername = (null === $oSession) ? '' : $oSession->get(SecurityContextInterface::LAST_USERNAME);

        $csrfToken = $this->container->has('form.csrf_provider')
            ? $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate')
            : null;

        return $this->renderLogin(array(
            'last_username' => $lastUsername,
            'error'         => $aErrors,
            'csrf_token' => $csrfToken,
            'oFormRegistration' => $oFormRegistration->createView()
        ));
    }

    /**
     * Renders the login template with the given parameters. Overwrite this function in
     * an extended controller to provide additional data for the login template.
     *
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderLogin(array $data)
    {
        $template = sprintf('FOSUserBundle:Security:login.html.twig');

        return $this->container->get('templating')->renderResponse($template, $data);
    }

    public function checkAction()
    {
        throw new \RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
    }

    public function logoutAction()
    {
        throw new \RuntimeException('You must activate the logout in your security firewall configuration.');
    }
}
