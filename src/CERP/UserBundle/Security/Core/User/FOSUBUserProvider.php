<?php 

namespace CERP\UserBundle\Security\Core\User;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\Security\Core\User\UserInterface;
use CERP\SiteBundle\Entity\Page;

class FOSUBUserProvider extends BaseClass
{

    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {

        $property = $this->getProperty($response);
        $aResponse = $response->getResponse();
        $username = $aResponse['id'];


    
        //on connect - get the access token and the user ID
        $service = $response->getResourceOwner()->getName();

        $setter = 'set'.ucfirst($service);
        $setter_id = $setter.'Id';
        $setter_token = $setter.'AccessToken';

        //we "disconnect" previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) 
        {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $this->userManager->updateUser($previousUser);
        }

        //we connect current user
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());

        $this->userManager->updateUser($user);

    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {

    	global $kernel;

        if ('AppCache' == get_class($kernel)) {
            $kernel = $kernel->getKernel();
        }

        $aResponse = $response->getResponse();

        $oUser = $this->userManager->findUserBy(array($this->getProperty($response) => $aResponse['id']));
        //when the user is registrating
        if (null === $oUser) {
            $service = $response->getResourceOwner()->getName();
            $setter = 'set'.ucfirst($service);
            $setter_id = $setter.'Id';
            $setter_token = $setter.'AccessToken';
            // create new user here
            $oUser = $this->userManager->createUser();
            $oUser->$setter_id($aResponse['id']);
            $oUser->$setter_token($response->getAccessToken());
            //I have set all requested data with the user's username
            //modify here with relevant data
            $oUser->setUsername(isset($aResponse['id']) ? $aResponse['id'] : '');
            

            $oUser->setFirstName(isset($aResponse['first_name']) ? $aResponse['first_name'] : '');
            $oUser->setMiddleName(isset($aResponse['middle_name']) ? $aResponse['middle_name'] : '');
            $oUser->setLastName(isset($aResponse['last_name']) ? $aResponse['last_name'] : '');
            $oUser->setBirthday(isset($aResponse['birthday']) ? new \DateTime($aResponse['birthday']) : '');
            $oUser->setEmail(isset($aResponse['email']) ? $aResponse['email'] : '');
            $oUser->setPlainPassword(isset($aResponse['id']) ? $aResponse['id'] : '');
            $aMusic = $aResponse['music'];
            $oPageRepository = $kernel->getContainer()->get('doctrine')->getRepository('CERPSiteBundle:Page');
            $oManager = $kernel->getContainer()->get('doctrine')->getManager();
            foreach ($aMusic['data'] as $iKey => $sValue) 
            {
            	$oPage = $oPageRepository->findOneByName(trim($sValue['id']));
            	if ( $oPage == NULL )
            	{
                    $oPage = new Page();
                    $oPage->setName((isset($sValue['name'])) ? $sValue['name'] : '');
                    $oPage->setFacebookId((isset($sValue['id'])) ? $sValue['id'] : '');
                    $oPage->setGenre((isset($sValue['genre'])) ? $sValue['genre'] : '');
                    $oItunes = json_decode(file_get_contents("https://itunes.apple.com/search?term=" . urlencode($sValue['name']) . "&media=music&limit=1"));
                    $oItunes = (array)$oItunes;
                    $oItunes = $oItunes['results'];
                    if ( count($oItunes) != 0 )
                    {
                        $oItunes = (array)$oItunes;
                        if ( count($oItunes) != 0 )
                        {
                            $oItunes = $oItunes[0];
                            $oItunes = (array)$oItunes;

                            $oPage->setMedia((isset($oItunes['artworkUrl100'])) ? $oItunes['artworkUrl100'] : null);
                            $oPage->setGenre((isset($oItunes['primaryGenreName'])) ? $oItunes['primaryGenreName'] : null);
                        }
                    }

	            	$oManager->persist($oPage);
            	}
	            $oUser->addPage($oPage);
            }

            $oUser->setEnabled(true);
            $this->userManager->updateUser($oUser);
            return $oUser;
        }

        //if user exists - go with the HWIOAuth way
        $user = parent::loadUserByOAuthUserResponse($response);

        $serviceName = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($serviceName) . 'AccessToken';

        //update access token
        $user->$setter($response->getAccessToken());

        return $user;
    }

}


 ?>