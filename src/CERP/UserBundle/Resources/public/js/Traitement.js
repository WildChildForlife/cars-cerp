$(document).ready(function(){
    if( $('#map').length ){
       $('#map').gmap3({
          map:{
              options:{
                center:[48.861693,2.3412025],
                zoom: 17,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeId: "monTheme",
                scrollwheel : false,
                mapTypeControl: false,
                panControl: false,
                zoomControl: false,
                scaleControl: false,
                streetViewControl: false,
                rotateControl: false,
                rotateControlOptions: false,
                overviewMapControl: false,
                OverviewMapControlOptions: false,
                draggable: false,
            }
            },
          styledmaptype:{
            id: "monTheme",
                options:{
                name: "monTheme"
            },
            styles: [
                        {
                            featureType: 'all',
                            stylers: [
                                {saturation: -200},
                                {gamma: 0.80}
                            ]
                        },
                        
                        {
                            featureType: 'road.local',
                            stylers: [
                                {hue: "#5F5F5F"},
                                {visibility: 'simplified'}
                            ]
                        },
                    ]
            },
             marker:{
                values:[
                    //{latLng:[48.861693,2.3412025], address:"3 Rue du Louvre, 75001 Paris", options:{icon: "img/global/marker.png"}},
                ],
                options:{
                  draggable: false
                }
              }
        }); // map 
    }
	


    /******* Page Historique ******/
    if( $('#timeline').length ){
        $(function(){
            $().timelinr({
                arrowKeys: 'true'
            })
        });
    }


    /****** Page Equipe *******/
    if( $('.page-equipe').length ){
        var owl = $('#owl-team');
        owl.owlCarousel({
            items : 3,
            autoPlay:false,
            loop:true,
            navigation:true,
            onTranslated: onTranslatedEvent
        });

        function onTranslatedEvent(event)
        {
            $(".bloc-team").stop().animate({ opacity:0 }, function(){
                $(this).css('display', 'none');
                $("#owl-team .owl-item .slide-owl").removeClass('current-item');
                $("#owl-team .owl-item.active").not(":eq(1)").removeClass('current-item');
                $("#owl-team .owl-item.active:eq(1)").addClass('current-item');
                $(".bloc-team[data-team='" + $("#owl-team .owl-item.active:eq(1) a").data("target") + "']").css({ display:"block" }).stop().animate({ opacity: 1 });
            });
        }
    }



    /****** Scroll Bloc Concept Podz ********/
    if( $('.ContentTxtConceptPodz').length ){
        $('.ContentTxtConceptPodz').niceScroll({
            touchbehavior:false,
            cursorcolor:"#999999",
            cursoropacitymax:1,
            cursorwidth:3,
            background:"#f6f6f6",
            autohidemode:false
        });
    }

    /****** Scroll Bloc Concept Podz ********/
    if( $('.scrollTxtTP').length ){
        $('.scrollTxtTP').niceScroll({
            touchbehavior:false,
            cursorcolor:"#999999",
            cursoropacitymax:1,
            cursorwidth:3,
            background:"#f6f6f6",
            autohidemode:false
        });
    }


    /***** Video Page Concept Podz ******/
    if( $('#videoPlayer').length ){
        // global variable for the player
        var player;
        function onYouTubePlayerAPIReady() {
          player = new YT.Player('videoPlayer', {
            events: {
              'onReady': onPlayerReady
            }
          });
        }

        function onPlayerReady(event) {
          var playButton = document.getElementById("play-video");
          playButton.addEventListener("click", function() {
            player.playVideo();
          });
          
          // var pauseButton = document.getElementById("pause-button");
          // pauseButton.addEventListener("click", function() {
          //   player.pauseVideo();
          // });
          
        }

        // Inject YouTube API script
        var tag = document.createElement('script');
        tag.src = "//www.youtube.com/player_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }



    /**** BlackAndWhite list Guides Pothologies ******/
    if( $('.bwWrapper').length ){
        $('.bwWrapper:not(.active-img)').BlackAndWhite({
            hoverEffect : true, 
            webworkerPath : false,
            invertHoverEffect: false,
            intensity:1,
            speed: { 
                fadeIn: 200,
                fadeOut: 800
            }
        });
    }



    /****** Accordion *********/
    if( $('#accordion').length ){
        $( "#accordion" ).accordion({
            collapsible: true
        });
    }


    /****** Slider Bloc Podologie *********/
    if( $('.royalSliderBloc').length ){
        $('.royalSliderBloc').royalSlider({
            arrowsNav: true,
            loop: true,
            keyboardNavEnabled: true,
            controlsInside: false,
            imageScaleMode: 'fill',
            arrowsNavAutoHide: false,
            autoScaleSliderWidth: 490,     
            autoScaleSliderHeight: 340,
            controlNavigation: 'none',
            thumbsFitInViewport: false,
            navigateByClick: true,
            startSlideId: 0,
            autoScaleSlider: true,
            // autoPlay: {
            //     enabled: true,
            //     pauseOnHover: false
            // },
            transitionType:'move',
            globalCaption: true
        });
    }



    /****** Erreur Contact *******/
    if( $('#FormContact').length ){
        $('#FormContact').validate({
            rules: {
                nom: "required",
                prenom: "required",
                email: {
                    required: true,
                    email: true
                }
            },
            errorPlacement: function(error, element) {
                element.parent().find('label').addClass('sError');
                error.appendTo($("#message-error .content-msg-error"));
                $('.content-msg-error').css('left', '0');
                $('.content-msg-error').html('<span for="" class="error">'+REQUIRED_FIELDS+'</span>');
            },
            success: function(label, element) {
                $(element).parent().find('label').removeClass('sError');
                // label.addClass("valid").text("Ok!")
            },
            messages: {
                nom: REQUIRED_FIELDS,
                prenom: REQUIRED_FIELDS,
                email: REQUIRED_FIELDS
            }
        });
    }
	$(document).on('click', '.closePopupValidation, #overlay-contact', function(event) {
        $('#overlay-contact').fadeOut();
        $('#messageValidation').hide();
        event.preventDefault();
    });
    if( $('#messageValidation').length ){
        $('#messageValidation, #overlay-contact').removeClass('hidden');
    }



    /***** Validation Newsletter ********/
    $(document).on('click', '#newsletterSend', function(event) {
        var mailNewsletter = $('#email_newsletter').val();
        if (mailNewsletter=='' || mailNewsletter==undefined || !isValidEmailAddress(mailNewsletter) ) {
            $('#email_newsletter').addClass('error');
            $('#email_newsletter').attr('value', INVAlID_FIELD);
            return false;
        }else{
            $('#FormNewsletterFooter').submit();
        }
        event.preventDefault();
    });

    $(document).on('click', '#email_newsletter', function(event) {
        if( $(this).val() == INVAlID_FIELD ){
            console.log("FOCUS");
            if( $(this).hasClass('firstFocus') ){
                $(this).removeClass('firstFocus');
            }else{
                $(this).attr('value', '');
            }
        }
    });
    
    $(document).on('click', '.closePopupValidationNews, #overlay-newsletter', function(event) {
        $('#overlay-newsletter').fadeOut();
        $('#newslettereValidation').hide();
        event.preventDefault();
    });








    /****** Scroll Page Footer ********/
    if( $('.ScrollPageStatic').length ){
        $('.ScrollPageStatic').niceScroll({
            touchbehavior:false,
            cursorcolor:"#999999",
            cursoropacitymax:1,
            cursorwidth:3,
            background:"#f6f6f6",
            autohidemode:false
        });
    }
});



function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};