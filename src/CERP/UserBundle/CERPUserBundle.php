<?php

namespace CERP\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CERPUserBundle extends Bundle
{
	public function getParent()
    {
        return 'ApplicationSonataUserBundle';
    }
}
