SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

DROP TABLE IF EXISTS `acl_classes`;
CREATE TABLE `acl_classes` (
  `id` int(10) unsigned NOT NULL,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `acl_entries`;
CREATE TABLE `acl_entries` (
  `id` int(10) unsigned NOT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identity_id` int(10) unsigned DEFAULT NULL,
  `security_identity_id` int(10) unsigned NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) unsigned NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `acl_object_identities`;
CREATE TABLE `acl_object_identities` (
  `id` int(10) unsigned NOT NULL,
  `parent_object_identity_id` int(10) unsigned DEFAULT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `acl_object_identity_ancestors`;
CREATE TABLE `acl_object_identity_ancestors` (
  `object_identity_id` int(10) unsigned NOT NULL,
  `ancestor_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `acl_security_identities`;
CREATE TABLE `acl_security_identities` (
  `id` int(10) unsigned NOT NULL,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `anomalie`;
CREATE TABLE `anomalie` (
  `id` int(11) NOT NULL,
  `Libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Anomalie_audit`;
CREATE TABLE `Anomalie_audit` (
  `id` int(11) NOT NULL,
  `rev` int(11) NOT NULL,
  `Libelle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `revtype` varchar(4) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `blacklist`;
CREATE TABLE `blacklist` (
  `id` int(11) NOT NULL,
  `locataire_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `blacklist` (`id`, `locataire_id`) VALUES
(1, NULL),
(3, NULL),
(4, NULL),
(5, NULL),
(6, NULL),
(7, NULL),
(9, NULL),
(2, 1),
(8, 5);

DROP TABLE IF EXISTS `BlackList_audit`;
CREATE TABLE `BlackList_audit` (
  `id` int(11) NOT NULL,
  `rev` int(11) NOT NULL,
  `locataire_id` int(11) DEFAULT NULL,
  `revtype` varchar(4) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `BlackList_audit` (`id`, `rev`, `locataire_id`, `revtype`) VALUES
(9, 1, NULL, 'INS');

DROP TABLE IF EXISTS `blacklist_incident`;
CREATE TABLE `blacklist_incident` (
  `blacklist_id` int(11) NOT NULL,
  `incident_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `blacklist_incident` (`blacklist_id`, `incident_id`) VALUES
(2, 1),
(2, 14),
(2, 15),
(8, 16),
(8, 17),
(8, 18),
(8, 19);

DROP TABLE IF EXISTS `block`;
CREATE TABLE `block` (
  `id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `contenu` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  `date` date NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `groupe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `boitevitesse`;
CREATE TABLE `boitevitesse` (
  `id` int(11) NOT NULL,
  `Libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `boitevitesse` (`id`, `Libelle`) VALUES
(1, 'Manuelle'),
(2, 'Automatique');

DROP TABLE IF EXISTS `classification__category`;
CREATE TABLE `classification__category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `classification__category` (`id`, `parent_id`, `context`, `media_id`, `name`, `enabled`, `slug`, `description`, `position`, `created_at`, `updated_at`) VALUES
(1, NULL, 'General', NULL, 'General', 1, 'general', 'General', NULL, '2016-02-05 23:46:53', '2016-02-05 23:46:53');

DROP TABLE IF EXISTS `classification__collection`;
CREATE TABLE `classification__collection` (
  `id` int(11) NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `classification__context`;
CREATE TABLE `classification__context` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `classification__context` (`id`, `name`, `enabled`, `created_at`, `updated_at`) VALUES
('General', 'General', 1, '2016-02-05 23:46:53', '2016-02-05 23:46:53');

DROP TABLE IF EXISTS `classification__tag`;
CREATE TABLE `classification__tag` (
  `id` int(11) NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `energie`;
CREATE TABLE `energie` (
  `id` int(11) NOT NULL,
  `Libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `energie` (`id`, `Libelle`) VALUES
(1, 'Diesel'),
(2, 'Essence');

DROP TABLE IF EXISTS `equipement`;
CREATE TABLE `equipement` (
  `id` int(11) NOT NULL,
  `voiture_id` int(11) DEFAULT NULL,
  `Libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Presence` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `equipement` (`id`, `voiture_id`, `Libelle`, `Presence`) VALUES
(1, NULL, 'Climatisation', NULL),
(2, NULL, 'Bluetooth', NULL),
(3, NULL, 'Radio', NULL),
(4, NULL, 'CD / MP3', NULL),
(5, NULL, 'Airbag', NULL),
(6, NULL, 'Verrouillage télécommandé', NULL);

DROP TABLE IF EXISTS `ext_log_entries`;
CREATE TABLE `ext_log_entries` (
  `id` int(11) NOT NULL,
  `action` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `loggedAt` datetime NOT NULL,
  `objectId` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `objectClass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` int(11) NOT NULL,
  `data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `ext_translations`;
CREATE TABLE `ext_translations` (
  `id` int(11) NOT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `objectClass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `foreignKey` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `fos_user_group`;
CREATE TABLE `fos_user_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `fos_user_user`;
CREATE TABLE `fos_user_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `fos_user_user_group`;
CREATE TABLE `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `incident`;
CREATE TABLE `incident` (
  `id` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `incident` (`id`, `description`, `type`) VALUES
(1, 'broublim', 'Dommage Voiture'),
(2, '', 'Paiement'),
(3, '', 'Mauvais comportement'),
(4, '', 'Dommage Voiture'),
(5, '', 'Dommage Voiture'),
(6, '', 'Dommage Voiture'),
(7, '', 'Dommage Voiture'),
(8, '', 'Vol'),
(9, '', 'Mauvais comportement'),
(10, '', 'Dommage Voiture'),
(11, '', 'Paiement'),
(12, '', 'Dommage Voiture'),
(13, '', 'Vol'),
(14, 'fdfdsf', 'Vol'),
(15, 'fsdfsdfs', 'Mauvais comportement'),
(16, 'fdsfsdf', 'Mauvais comportement'),
(17, 'fdsfsd', 'Paiement'),
(18, 'fdsfsdf', 'Vol'),
(19, 'edsdsd', 'Dommage Voiture');

DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `blank` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `item_translations`;
CREATE TABLE `item_translations` (
  `id` int(11) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `foreign_key` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `objectClass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `lexik_translation_file`;
CREATE TABLE `lexik_translation_file` (
  `id` int(11) NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `extention` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `lexik_trans_unit`;
CREATE TABLE `lexik_trans_unit` (
  `id` int(11) NOT NULL,
  `key_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `lexik_trans_unit_translations`;
CREATE TABLE `lexik_trans_unit_translations` (
  `id` int(11) NOT NULL,
  `file_id` int(11) DEFAULT NULL,
  `trans_unit_id` int(11) DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `local`;
CREATE TABLE `local` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) DEFAULT NULL,
  `actif` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `locataire`;
CREATE TABLE `locataire` (
  `id` int(11) NOT NULL,
  `actif` int(11) DEFAULT NULL,
  `Nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CIN` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `locataire` (`id`, `actif`, `Nom`, `Prenom`, `CIN`, `telephone`, `permis`, `Adresse`, `user_id`) VALUES
(1, 0, 'Touzani', 'Seddik', 'ee564', '(15) 10 55 63 54', '654z', 'Adresse de test', 1),
(2, 0, 'EL Gharbaoui', 'Youssef', 'hmm555', '(06) 66 66 66 66', '65zd', 'Test adresse', 1),
(3, 1, 'fh', 'rg', '321rg', '(00) 00 00 00 00', '0erg', 'erg', 2),
(4, 0, 'El Gharbaoui', 'Youssef', 'tyhjklm', '(06) 49 74 04 04', 'klfdsjklfjsdklfjskdfjkl', 'Hay Mly Abdellah', 1),
(5, 1, 'El Gharbaoui', 'Youssef', 'BK258694', '(06) 49 47 49 04', 'fdsfdsfdsf', 'Hay Mly Abdellah', 1),
(6, 0, 'El Gharbaoui2', 'Youssef', 'BK2586948', '(98) 76 56 78 90', 'HJHFJD878', 'Hay Mly Abdellah2', 1),
(7, NULL, 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', NULL);

DROP TABLE IF EXISTS `Locataire_audit`;
CREATE TABLE `Locataire_audit` (
  `id` int(11) NOT NULL,
  `rev` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `actif` int(11) DEFAULT NULL,
  `Nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Prenom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CIN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adresse` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `revtype` varchar(4) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `Locataire_audit` (`id`, `rev`, `user_id`, `actif`, `Nom`, `Prenom`, `CIN`, `telephone`, `permis`, `Adresse`, `revtype`) VALUES
(7, 1, NULL, NULL, 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'INS');

DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `voiture_id` int(11) DEFAULT NULL,
  `locataire_id` int(11) DEFAULT NULL,
  `Libelle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `actif` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `dateCreation` datetime NOT NULL,
  `dateRetour` datetime NOT NULL,
  `dateLocation` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `marque`;
CREATE TABLE `marque` (
  `id` int(11) NOT NULL,
  `Libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `media_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `marque` (`id`, `Libelle`, `media_id`) VALUES
(5, 'BMW', 5),
(6, 'Alfa Romeo', 6),
(7, 'Subaru', 7),
(8, 'Volvo', 8),
(9, 'Volkswagen', 9),
(10, 'Ford', 10),
(11, 'Toyora', 11),
(12, 'Skoda', 12),
(13, 'Porsche', 13),
(14, 'Seat', 14),
(15, 'Renault', 15),
(16, 'Peugeot', 16),
(17, 'Opel', 17),
(18, 'Nissan', 18),
(19, 'MG', 19),
(20, 'Mitsubishi', 20),
(21, 'Mercedes Benz', 21),
(22, 'Mazda', 22),
(23, 'Land Rover', 23),
(24, 'MINI', 24),
(25, 'Lexus', 25),
(26, 'Infiniti', 26),
(27, 'Hyundai', 27),
(28, 'Chevrolet', 28),
(29, 'Lancia', 29),
(30, 'KIA', 30),
(31, 'Jeep', 31),
(32, 'Jaguar', 32),
(33, 'Honda', 33),
(34, 'Ferrari', 34),
(35, 'Audi', 35),
(36, 'Citroen', 36),
(37, 'Fiat', 37);

DROP TABLE IF EXISTS `Marque_audit`;
CREATE TABLE `Marque_audit` (
  `id` int(11) NOT NULL,
  `rev` int(11) NOT NULL,
  `media_id` int(11) DEFAULT NULL,
  `Libelle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `revtype` varchar(4) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `Marque_audit` (`id`, `rev`, `media_id`, `Libelle`, `revtype`) VALUES
(9, 6, 5, 'Volkswagen', 'UPD'),
(9, 7, 9, 'Volkswagen', 'UPD');

DROP TABLE IF EXISTS `media__gallery`;
CREATE TABLE `media__gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `default_format` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `media__gallery_media`;
CREATE TABLE `media__gallery_media` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `media__media`;
CREATE TABLE `media__media` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) NOT NULL,
  `provider_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_status` int(11) NOT NULL,
  `provider_reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_metadata` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `length` decimal(10,0) DEFAULT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_size` int(11) DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_is_flushable` tinyint(1) DEFAULT NULL,
  `cdn_flush_identifier` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_flush_at` datetime DEFAULT NULL,
  `cdn_status` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `media__media` (`id`, `category_id`, `name`, `description`, `enabled`, `provider_name`, `provider_status`, `provider_reference`, `provider_metadata`, `width`, `height`, `length`, `content_type`, `content_size`, `copyright`, `author_name`, `context`, `cdn_is_flushable`, `cdn_flush_identifier`, `cdn_flush_at`, `cdn_status`, `updated_at`, `created_at`) VALUES
(1, 1, 'ssangyong-official-logo-of-the-company-150x90.jpg', NULL, 0, 'sonata.media.provider.image', 1, '205628026b89878b78574f2443504fec99b9078c.jpeg', '{"filename":"ssangyong-official-logo-of-the-company-150x90.jpg"}', 150, 90, NULL, 'image/jpeg', 5983, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-05 23:54:25', '2016-02-05 23:54:25'),
(2, 1, 'maybach-official-logo-of-the-company-150x90.jpg', NULL, 0, 'sonata.media.provider.image', 1, '9aea9d4d4b2926ce24bcf46eaa784df9f342ef60.jpeg', '{"filename":"maybach-official-logo-of-the-company-150x90.jpg"}', 150, 90, NULL, 'image/jpeg', 4577, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-05 23:55:01', '2016-02-05 23:55:01'),
(3, 1, 'Pontiac-Official-Logo-of-the-Company-150x90.jpg', NULL, 0, 'sonata.media.provider.image', 1, '48f2fb5fc2e13e32401e5ef0bb48ba54245226b9.jpeg', '{"filename":"Pontiac-Official-Logo-of-the-Company-150x90.jpg"}', 150, 90, NULL, 'image/jpeg', 2900, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-05 23:55:38', '2016-02-05 23:55:38'),
(4, 1, 'Morris-Official-Logo-of-the-Company-150x90.jpg', NULL, 0, 'sonata.media.provider.image', 1, '173799460f7d26901ea1c01de9e1c968e39f1647.jpeg', '{"filename":"Morris-Official-Logo-of-the-Company-150x90.jpg"}', 150, 90, NULL, 'image/jpeg', 5574, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-05 23:56:38', '2016-02-05 23:56:38'),
(5, 1, 'bmw.png', NULL, 0, 'sonata.media.provider.image', 1, 'fd73c9cec85ac389cad4120142e6d3d22b4018f3.png', '{"filename":"bmw.png"}', 1175, 1175, NULL, 'image/png', 333239, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:36:05', '2016-02-06 23:36:05'),
(6, 1, 'car_logo_PNG1639.png', NULL, 0, 'sonata.media.provider.image', 1, 'bf4fa99b66b21ee1c5c58e2ca4f329115cd15940.png', '{"filename":"car_logo_PNG1639.png"}', 1311, 1311, NULL, 'image/png', 312709, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:36:41', '2016-02-06 23:36:41'),
(7, 1, 'car_logo_PNG1669.png', NULL, 0, 'sonata.media.provider.image', 1, '0336f729183eca385a7af4ed1c6b7f3140d5d467.png', '{"filename":"car_logo_PNG1669.png"}', 2000, 1200, NULL, 'image/png', 1641038, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:37:19', '2016-02-06 23:37:19'),
(8, 1, 'car_logo_PNG1668.png', NULL, 0, 'sonata.media.provider.image', 1, '0e531ef963dfbd4427fc680de11c95bbead51c63.png', '{"filename":"car_logo_PNG1668.png"}', 2278, 1909, NULL, 'image/png', 474622, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:38:02', '2016-02-06 23:38:02'),
(9, 1, 'car_logo_PNG1667.png', NULL, 0, 'sonata.media.provider.image', 1, 'b9ae1dd78a5ab1d89007141463696ae803e0aaf8.png', '{"filename":"car_logo_PNG1667.png"}', 1817, 1814, NULL, 'image/png', 1023344, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:38:36', '2016-02-06 23:38:36'),
(10, 1, 'car_logo_PNG1666.png', NULL, 0, 'sonata.media.provider.image', 1, '979211b894b849212cf4baf1ae83b42606ce66af.png', '{"filename":"car_logo_PNG1666.png"}', 2302, 883, NULL, 'image/png', 535336, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:39:21', '2016-02-06 23:39:21'),
(11, 1, 'car_logo_PNG1665.png', NULL, 0, 'sonata.media.provider.image', 1, '7c0f0bef8195c8bd2dec8180f33719c5bb1f9ffc.png', '{"filename":"car_logo_PNG1665.png"}', 1220, 1017, NULL, 'image/png', 387291, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:40:16', '2016-02-06 23:40:16'),
(12, 1, 'car_logo_PNG1664.png', NULL, 0, 'sonata.media.provider.image', 1, 'fba7caa653a18acb32ee74686114a9d249ecebba.png', '{"filename":"car_logo_PNG1664.png"}', 1300, 1300, NULL, 'image/png', 464326, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:41:45', '2016-02-06 23:41:45'),
(13, 1, 'car_logo_PNG1663.png', NULL, 0, 'sonata.media.provider.image', 1, 'fc4077257505087eb6ff95a94490adf9c9f09efd.png', '{"filename":"car_logo_PNG1663.png"}', 781, 1020, NULL, 'image/png', 804741, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:42:15', '2016-02-06 23:42:15'),
(14, 1, 'car_logo_PNG1662.png', NULL, 0, 'sonata.media.provider.image', 1, 'd54be07f3594351a7b42e6c4a43cc63213d67eb3.png', '{"filename":"car_logo_PNG1662.png"}', 800, 800, NULL, 'image/png', 91207, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:42:48', '2016-02-06 23:42:48'),
(15, 1, 'car_logo_PNG1661.png', NULL, 0, 'sonata.media.provider.image', 1, 'ecd3e0f78e26d1baa1b6dcfa7edfd387652207bb.png', '{"filename":"car_logo_PNG1661.png"}', 1000, 1000, NULL, 'image/png', 203401, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:43:25', '2016-02-06 23:43:25'),
(16, 1, 'car_logo_PNG1660.png', NULL, 0, 'sonata.media.provider.image', 1, '28dbd0a76c845c859636f10c0f69695049c71812.png', '{"filename":"car_logo_PNG1660.png"}', 2200, 2200, NULL, 'image/png', 719628, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:43:54', '2016-02-06 23:43:54'),
(17, 1, 'car_logo_PNG1659.png', NULL, 0, 'sonata.media.provider.image', 1, '0b010583a5af51a4e17ab48f1f5279e35f7a4e2c.png', '{"filename":"car_logo_PNG1659.png"}', 1215, 1044, NULL, 'image/png', 479107, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:44:27', '2016-02-06 23:44:27'),
(18, 1, 'car_logo_PNG1658.png', NULL, 0, 'sonata.media.provider.image', 1, '6385714ff7bc54af382192cf23ce46ac96003016.png', '{"filename":"car_logo_PNG1658.png"}', 1575, 1348, NULL, 'image/png', 526945, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:45:04', '2016-02-06 23:45:04'),
(19, 1, 'car_logo_PNG1657.png', NULL, 0, 'sonata.media.provider.image', 1, '7f8378713c2564af4625b60dbe8cb8e75cb02a6a.png', '{"filename":"car_logo_PNG1657.png"}', 701, 701, NULL, 'image/png', 702144, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:45:40', '2016-02-06 23:45:40'),
(20, 1, 'car_logo_PNG1656.png', NULL, 0, 'sonata.media.provider.image', 1, 'bb3bcc013ae08c048b0506e44349ae9092f2047f.png', '{"filename":"car_logo_PNG1656.png"}', 1600, 1401, NULL, 'image/png', 66531, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:46:07', '2016-02-06 23:46:07'),
(21, 1, 'car_logo_PNG1655.png', NULL, 0, 'sonata.media.provider.image', 1, 'ae94b9a4ffcafaa8b01a1beabaeb92f7cf0d0a01.png', '{"filename":"car_logo_PNG1655.png"}', 1024, 768, NULL, 'image/png', 411215, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:46:36', '2016-02-06 23:46:36'),
(22, 1, 'car_logo_PNG1654.png', NULL, 0, 'sonata.media.provider.image', 1, 'a55435aafeb1d11b935c3745f370d07702ee3f23.png', '{"filename":"car_logo_PNG1654.png"}', 3141, 2490, NULL, 'image/png', 596129, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:47:13', '2016-02-06 23:47:13'),
(23, 1, 'car_logo_PNG1653.png', NULL, 0, 'sonata.media.provider.image', 1, '51d42fc941cd8d8efb6275c39d58d1cf9c00ccc0.png', '{"filename":"car_logo_PNG1653.png"}', 2476, 1369, NULL, 'image/png', 1261827, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:47:43', '2016-02-06 23:47:43'),
(24, 1, 'car_logo_PNG1652.png', NULL, 0, 'sonata.media.provider.image', 1, 'b2fdfec22e3699276e75335150e954bee40310cc.png', '{"filename":"car_logo_PNG1652.png"}', 1600, 674, NULL, 'image/png', 220937, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:48:13', '2016-02-06 23:48:13'),
(25, 1, 'car_logo_PNG1651.png', NULL, 0, 'sonata.media.provider.image', 1, 'd1e40a6b693b297e8752d4cdca98971275b99e96.png', '{"filename":"car_logo_PNG1651.png"}', 2200, 1100, NULL, 'image/png', 489431, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:48:51', '2016-02-06 23:48:51'),
(26, 1, 'car_logo_PNG1646.png', NULL, 0, 'sonata.media.provider.image', 1, 'a0cf2acdf20f9209ae762d0f6a95c5a8a43c967a.png', '{"filename":"car_logo_PNG1646.png"}', 1540, 818, NULL, 'image/png', 186242, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:50:00', '2016-02-06 23:50:00'),
(27, 1, 'car_logo_PNG1645.png', NULL, 0, 'sonata.media.provider.image', 1, '907dde3668671046095f558a705374145e0e68eb.png', '{"filename":"car_logo_PNG1645.png"}', 1749, 912, NULL, 'image/png', 408536, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:50:30', '2016-02-06 23:50:30'),
(28, 1, 'car_logo_PNG1644.png', NULL, 0, 'sonata.media.provider.image', 1, '0a05dcce53ac41a3926341efa5936c34708ef3e6.png', '{"filename":"car_logo_PNG1644.png"}', 2048, 775, NULL, 'image/png', 918784, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:50:58', '2016-02-06 23:50:58'),
(29, 1, 'car_logo_PNG1650.png', NULL, 0, 'sonata.media.provider.image', 1, '41eaf7bc5b590901b3e38dd7c9026a4040e9a2eb.png', '{"filename":"car_logo_PNG1650.png"}', 731, 750, NULL, 'image/png', 691008, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:51:27', '2016-02-06 23:51:27'),
(30, 1, 'car_logo_PNG1649.png', NULL, 0, 'sonata.media.provider.image', 1, '72c10d0daafa1fe77f34c1eb944a7f6c932e1d54.png', '{"filename":"car_logo_PNG1649.png"}', 1836, 921, NULL, 'image/png', 56739, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:51:54', '2016-02-06 23:51:54'),
(31, 1, 'car_logo_PNG1648.png', NULL, 0, 'sonata.media.provider.image', 1, '0608b4f8b98f9b2d0c5cce76338f03db5478159d.png', '{"filename":"car_logo_PNG1648.png"}', 3200, 1485, NULL, 'image/png', 135745, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:52:20', '2016-02-06 23:52:20'),
(32, 1, 'car_logo_PNG1647.png', NULL, 0, 'sonata.media.provider.image', 1, '8ffcd214d56c7a77f9ce9890d9b00ca7ea7287ed.png', '{"filename":"car_logo_PNG1647.png"}', 1600, 1200, NULL, 'image/png', 258150, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:52:56', '2016-02-06 23:52:56'),
(33, 1, 'car_logo_PNG1643.png', NULL, 0, 'sonata.media.provider.image', 1, 'f88738546291323cd722f78acaed5a397df4d19c.png', '{"filename":"car_logo_PNG1643.png"}', 1156, 942, NULL, 'image/png', 443710, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:53:30', '2016-02-06 23:53:30'),
(34, 1, 'ferrari.png', NULL, 0, 'sonata.media.provider.image', 1, '2fa25ef19ee2a779ec201d67e208c6cef37fd28c.png', '{"filename":"ferrari.png"}', 575, 712, NULL, 'image/png', 370436, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:54:01', '2016-02-06 23:54:01'),
(35, 1, 'car_logo_PNG1640.png', NULL, 0, 'sonata.media.provider.image', 1, 'ae60b1e7bd319ab59c05eb007f87813b87dbbbb5.png', '{"filename":"car_logo_PNG1640.png"}', 1470, 1100, NULL, 'image/png', 294905, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:54:40', '2016-02-06 23:54:40'),
(36, 1, 'citroen.png', NULL, 0, 'sonata.media.provider.image', 1, '137e29b20b749523bb0f19fd3b3363e46fff5c79.png', '{"filename":"citroen.png"}', 1300, 1200, NULL, 'image/png', 177859, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:55:49', '2016-02-06 23:55:49'),
(37, 1, 'fiat.png', NULL, 0, 'sonata.media.provider.image', 1, 'ba5a4a1eefa0db5d6d029dd8ff53b7dddcbb8f46.png', '{"filename":"fiat.png"}', 1024, 1024, NULL, 'image/png', 1238752, NULL, NULL, 'General', NULL, NULL, NULL, NULL, '2016-02-06 23:59:02', '2016-02-06 23:59:02');

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `actif` tinyint(1) NOT NULL,
  `global` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `menu_item`;
CREATE TABLE `menu_item` (
  `menu_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `modele`;
CREATE TABLE `modele` (
  `id` int(11) NOT NULL,
  `Libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `notification__message`;
CREATE TABLE `notification__message` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `state` int(11) NOT NULL,
  `restart_count` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `page`;
CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `media_id` int(11) DEFAULT NULL,
  `page_parente_id` int(11) DEFAULT NULL,
  `bandeau_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `analytics` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `page_translations`;
CREATE TABLE `page_translations` (
  `id` int(11) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `foreign_key` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `objectClass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `piece`;
CREATE TABLE `piece` (
  `id` int(11) NOT NULL,
  `voiture_id` int(11) DEFAULT NULL,
  `Libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Date_Changement` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `revisions`;
CREATE TABLE `revisions` (
  `id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `revisions` (`id`, `timestamp`, `username`) VALUES
(1, '2016-02-08 14:39:53', 'admin'),
(2, '2016-02-08 16:58:52', 'admin'),
(3, '2016-02-08 17:32:28', 'admin'),
(4, '2016-02-08 17:39:48', 'admin'),
(5, '2016-02-08 18:08:15', 'admin'),
(6, '2016-02-08 18:46:47', 'admin'),
(7, '2016-02-08 18:47:39', 'admin'),
(8, '2016-02-08 18:51:57', 'admin'),
(9, '2016-02-08 19:08:02', 'admin'),
(10, '2016-02-08 19:08:13', 'admin'),
(11, '2016-02-08 19:11:08', 'admin');

DROP TABLE IF EXISTS `siteconf`;
CREATE TABLE `siteconf` (
  `id` int(11) NOT NULL,
  `contactAdmin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Ticket`;
CREATE TABLE `Ticket` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priorite` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `criticite` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `etat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateDemande` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Ticket_audit`;
CREATE TABLE `Ticket_audit` (
  `id` int(11) NOT NULL,
  `rev` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `priorite` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `criticite` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `etat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateDemande` datetime DEFAULT NULL,
  `revtype` varchar(4) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `timeline__action`;
CREATE TABLE `timeline__action` (
  `id` int(11) NOT NULL,
  `verb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status_current` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status_wanted` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duplicate_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duplicate_priority` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `timeline__action` (`id`, `verb`, `status_current`, `status_wanted`, `duplicate_key`, `duplicate_priority`, `created_at`) VALUES
(1, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-03 23:37:33'),
(2, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-03 23:37:52'),
(3, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-03 23:38:11'),
(4, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-03 23:38:53'),
(5, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-03 23:39:08'),
(6, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-03 23:39:49'),
(7, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-05 23:54:26'),
(8, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-05 23:54:32'),
(9, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-05 23:55:01'),
(10, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-05 23:55:05'),
(11, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-05 23:55:38'),
(12, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-05 23:55:42'),
(13, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-05 23:56:38'),
(14, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-05 23:56:48'),
(15, 'sonata.admin.delete', 'published', 'frozen', NULL, NULL, '2016-02-06 23:33:50'),
(16, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:36:05'),
(17, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:36:11'),
(18, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:36:41'),
(19, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:36:49'),
(20, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:37:20'),
(21, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:37:36'),
(22, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:38:03'),
(23, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:38:07'),
(24, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:38:37'),
(25, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:38:50'),
(26, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:39:21'),
(27, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:39:25'),
(28, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:40:17'),
(29, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:40:33'),
(30, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:41:46'),
(31, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:41:49'),
(32, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:42:16'),
(33, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:42:19'),
(34, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:42:48'),
(35, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:42:58'),
(36, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:43:25'),
(37, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:43:29'),
(38, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:43:55'),
(39, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:43:59'),
(40, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:44:27'),
(41, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:44:32'),
(42, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:45:05'),
(43, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:45:12'),
(44, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:45:41'),
(45, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:45:44'),
(46, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:46:08'),
(47, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:46:12'),
(48, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:46:36'),
(49, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:46:40'),
(50, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:47:15'),
(51, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:47:19'),
(52, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:47:44'),
(53, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:47:48'),
(54, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:48:13'),
(55, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:48:16'),
(56, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:48:51'),
(57, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:48:55'),
(58, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:50:00'),
(59, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:50:04'),
(60, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:50:30'),
(61, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:50:34'),
(62, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:50:59'),
(63, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:51:02'),
(64, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:51:27'),
(65, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:51:31'),
(66, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:51:54'),
(67, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:51:57'),
(68, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:52:21'),
(69, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:52:24'),
(70, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:52:57'),
(71, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:53:06'),
(72, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:53:30'),
(73, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:53:36'),
(74, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:54:02'),
(75, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:54:05'),
(76, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:54:41'),
(77, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:54:44'),
(78, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:55:50'),
(79, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:55:54'),
(80, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2016-02-06 23:57:13'),
(81, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:59:03'),
(82, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-06 23:59:10'),
(83, 'sonata.admin.delete', 'published', 'frozen', NULL, NULL, '2016-02-07 00:28:11'),
(84, 'sonata.admin.delete', 'published', 'frozen', NULL, NULL, '2016-02-07 00:35:55'),
(85, 'sonata.admin.delete', 'published', 'frozen', NULL, NULL, '2016-02-07 00:36:22'),
(86, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2016-02-08 14:39:53'),
(87, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2016-02-08 18:46:47'),
(88, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2016-02-08 18:47:39');

DROP TABLE IF EXISTS `timeline__action_component`;
CREATE TABLE `timeline__action_component` (
  `id` int(11) NOT NULL,
  `action_id` int(11) DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=349 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `timeline__action_component` (`id`, `action_id`, `component_id`, `type`, `text`) VALUES
(1, 1, 2, 'target', NULL),
(2, 1, NULL, 'target_text', 'CERP\\ClientBundle\\Entity\\Equipement:000000002bf16ec6000000012b45a85c'),
(3, 1, NULL, 'admin_code', 'cerp_client.admin.equipement'),
(4, 1, 1, 'subject', NULL),
(5, 2, 4, 'target', NULL),
(6, 2, NULL, 'target_text', 'CERP\\ClientBundle\\Entity\\Equipement:000000007dbf9c50000000014d9d8ef2'),
(7, 2, NULL, 'admin_code', 'cerp_client.admin.equipement'),
(8, 2, 1, 'subject', NULL),
(9, 3, 5, 'target', NULL),
(10, 3, NULL, 'target_text', 'CERP\\ClientBundle\\Entity\\Equipement:0000000006e7fb47000000013f4f7c6b'),
(11, 3, NULL, 'admin_code', 'cerp_client.admin.equipement'),
(12, 3, 1, 'subject', NULL),
(13, 4, 6, 'target', NULL),
(14, 4, NULL, 'target_text', 'CERP\\ClientBundle\\Entity\\Equipement:000000007f5734040000000150ce30c0'),
(15, 4, NULL, 'admin_code', 'cerp_client.admin.equipement'),
(16, 4, 1, 'subject', NULL),
(17, 5, 7, 'target', NULL),
(18, 5, NULL, 'target_text', 'CERP\\ClientBundle\\Entity\\Equipement:000000006ba6b16b000000012ef55270'),
(19, 5, NULL, 'admin_code', 'cerp_client.admin.equipement'),
(20, 5, 1, 'subject', NULL),
(21, 6, 8, 'target', NULL),
(22, 6, NULL, 'target_text', 'CERP\\ClientBundle\\Entity\\Equipement:000000002a9ffe1c00000001711dcba8'),
(23, 6, NULL, 'admin_code', 'cerp_client.admin.equipement'),
(24, 6, 1, 'subject', NULL),
(25, 7, 9, 'target', NULL),
(26, 7, NULL, 'target_text', 'ssangyong-official-logo-of-the-company-150x90.jpg'),
(27, 7, NULL, 'admin_code', 'sonata.media.admin.media'),
(28, 7, 1, 'subject', NULL),
(29, 8, 10, 'target', NULL),
(30, 8, NULL, 'target_text', 'CERP\\ClientBundle\\Entity\\Modele:00000000191f1268000000013f081434'),
(31, 8, NULL, 'admin_code', 'cerp_client.admin.modele'),
(32, 8, 1, 'subject', NULL),
(33, 9, 11, 'target', NULL),
(34, 9, NULL, 'target_text', 'maybach-official-logo-of-the-company-150x90.jpg'),
(35, 9, NULL, 'admin_code', 'sonata.media.admin.media'),
(36, 9, 1, 'subject', NULL),
(37, 10, 12, 'target', NULL),
(38, 10, NULL, 'target_text', 'CERP\\ClientBundle\\Entity\\Modele:0000000023df31dd0000000171ac2ef5'),
(39, 10, NULL, 'admin_code', 'cerp_client.admin.modele'),
(40, 10, 1, 'subject', NULL),
(41, 11, 13, 'target', NULL),
(42, 11, NULL, 'target_text', 'Pontiac-Official-Logo-of-the-Company-150x90.jpg'),
(43, 11, NULL, 'admin_code', 'sonata.media.admin.media'),
(44, 11, 1, 'subject', NULL),
(45, 12, 14, 'target', NULL),
(46, 12, NULL, 'target_text', 'CERP\\ClientBundle\\Entity\\Modele:000000004015c564000000010b18da80'),
(47, 12, NULL, 'admin_code', 'cerp_client.admin.modele'),
(48, 12, 1, 'subject', NULL),
(49, 13, 15, 'target', NULL),
(50, 13, NULL, 'target_text', 'Morris-Official-Logo-of-the-Company-150x90.jpg'),
(51, 13, NULL, 'admin_code', 'sonata.media.admin.media'),
(52, 13, 1, 'subject', NULL),
(53, 14, 16, 'target', NULL),
(54, 14, NULL, 'target_text', 'CERP\\ClientBundle\\Entity\\Modele:0000000026b4fd99000000016273e151'),
(55, 14, NULL, 'admin_code', 'cerp_client.admin.modele'),
(56, 14, 1, 'subject', NULL),
(57, 15, NULL, 'target_text', 'Ssangyong'),
(58, 15, NULL, 'admin_code', 'cerp_client.admin.modele'),
(59, 15, 1, 'subject', NULL),
(60, 16, 17, 'target', NULL),
(61, 16, NULL, 'target_text', 'bmw.png'),
(62, 16, NULL, 'admin_code', 'sonata.media.admin.media'),
(63, 16, 1, 'subject', NULL),
(64, 17, 18, 'target', NULL),
(65, 17, NULL, 'target_text', 'BMW'),
(66, 17, NULL, 'admin_code', 'cerp_client.admin.modele'),
(67, 17, 1, 'subject', NULL),
(68, 18, 19, 'target', NULL),
(69, 18, NULL, 'target_text', 'car_logo_PNG1639.png'),
(70, 18, NULL, 'admin_code', 'sonata.media.admin.media'),
(71, 18, 1, 'subject', NULL),
(72, 19, 20, 'target', NULL),
(73, 19, NULL, 'target_text', 'Alpha Romeo'),
(74, 19, NULL, 'admin_code', 'cerp_client.admin.modele'),
(75, 19, 1, 'subject', NULL),
(76, 20, 21, 'target', NULL),
(77, 20, NULL, 'target_text', 'car_logo_PNG1669.png'),
(78, 20, NULL, 'admin_code', 'sonata.media.admin.media'),
(79, 20, 1, 'subject', NULL),
(80, 21, 22, 'target', NULL),
(81, 21, NULL, 'target_text', 'Subaru'),
(82, 21, NULL, 'admin_code', 'cerp_client.admin.modele'),
(83, 21, 1, 'subject', NULL),
(84, 22, 23, 'target', NULL),
(85, 22, NULL, 'target_text', 'car_logo_PNG1668.png'),
(86, 22, NULL, 'admin_code', 'sonata.media.admin.media'),
(87, 22, 1, 'subject', NULL),
(88, 23, 24, 'target', NULL),
(89, 23, NULL, 'target_text', 'Volvo'),
(90, 23, NULL, 'admin_code', 'cerp_client.admin.modele'),
(91, 23, 1, 'subject', NULL),
(92, 24, 25, 'target', NULL),
(93, 24, NULL, 'target_text', 'car_logo_PNG1667.png'),
(94, 24, NULL, 'admin_code', 'sonata.media.admin.media'),
(95, 24, 1, 'subject', NULL),
(96, 25, 26, 'target', NULL),
(97, 25, NULL, 'target_text', 'Volkswagen'),
(98, 25, NULL, 'admin_code', 'cerp_client.admin.modele'),
(99, 25, 1, 'subject', NULL),
(100, 26, 27, 'target', NULL),
(101, 26, NULL, 'target_text', 'car_logo_PNG1666.png'),
(102, 26, NULL, 'admin_code', 'sonata.media.admin.media'),
(103, 26, 1, 'subject', NULL),
(104, 27, 28, 'target', NULL),
(105, 27, NULL, 'target_text', 'Ford'),
(106, 27, NULL, 'admin_code', 'cerp_client.admin.modele'),
(107, 27, 1, 'subject', NULL),
(108, 28, 29, 'target', NULL),
(109, 28, NULL, 'target_text', 'car_logo_PNG1665.png'),
(110, 28, NULL, 'admin_code', 'sonata.media.admin.media'),
(111, 28, 1, 'subject', NULL),
(112, 29, 30, 'target', NULL),
(113, 29, NULL, 'target_text', 'Toyora'),
(114, 29, NULL, 'admin_code', 'cerp_client.admin.modele'),
(115, 29, 1, 'subject', NULL),
(116, 30, 31, 'target', NULL),
(117, 30, NULL, 'target_text', 'car_logo_PNG1664.png'),
(118, 30, NULL, 'admin_code', 'sonata.media.admin.media'),
(119, 30, 1, 'subject', NULL),
(120, 31, 32, 'target', NULL),
(121, 31, NULL, 'target_text', 'Skoda'),
(122, 31, NULL, 'admin_code', 'cerp_client.admin.modele'),
(123, 31, 1, 'subject', NULL),
(124, 32, 33, 'target', NULL),
(125, 32, NULL, 'target_text', 'car_logo_PNG1663.png'),
(126, 32, NULL, 'admin_code', 'sonata.media.admin.media'),
(127, 32, 1, 'subject', NULL),
(128, 33, 34, 'target', NULL),
(129, 33, NULL, 'target_text', 'Porsche'),
(130, 33, NULL, 'admin_code', 'cerp_client.admin.modele'),
(131, 33, 1, 'subject', NULL),
(132, 34, 35, 'target', NULL),
(133, 34, NULL, 'target_text', 'car_logo_PNG1662.png'),
(134, 34, NULL, 'admin_code', 'sonata.media.admin.media'),
(135, 34, 1, 'subject', NULL),
(136, 35, 36, 'target', NULL),
(137, 35, NULL, 'target_text', 'Seat'),
(138, 35, NULL, 'admin_code', 'cerp_client.admin.modele'),
(139, 35, 1, 'subject', NULL),
(140, 36, 37, 'target', NULL),
(141, 36, NULL, 'target_text', 'car_logo_PNG1661.png'),
(142, 36, NULL, 'admin_code', 'sonata.media.admin.media'),
(143, 36, 1, 'subject', NULL),
(144, 37, 38, 'target', NULL),
(145, 37, NULL, 'target_text', 'Renault'),
(146, 37, NULL, 'admin_code', 'cerp_client.admin.modele'),
(147, 37, 1, 'subject', NULL),
(148, 38, 39, 'target', NULL),
(149, 38, NULL, 'target_text', 'car_logo_PNG1660.png'),
(150, 38, NULL, 'admin_code', 'sonata.media.admin.media'),
(151, 38, 1, 'subject', NULL),
(152, 39, 40, 'target', NULL),
(153, 39, NULL, 'target_text', 'Peugeot'),
(154, 39, NULL, 'admin_code', 'cerp_client.admin.modele'),
(155, 39, 1, 'subject', NULL),
(156, 40, 41, 'target', NULL),
(157, 40, NULL, 'target_text', 'car_logo_PNG1659.png'),
(158, 40, NULL, 'admin_code', 'sonata.media.admin.media'),
(159, 40, 1, 'subject', NULL),
(160, 41, 42, 'target', NULL),
(161, 41, NULL, 'target_text', 'Opel'),
(162, 41, NULL, 'admin_code', 'cerp_client.admin.modele'),
(163, 41, 1, 'subject', NULL),
(164, 42, 43, 'target', NULL),
(165, 42, NULL, 'target_text', 'car_logo_PNG1658.png'),
(166, 42, NULL, 'admin_code', 'sonata.media.admin.media'),
(167, 42, 1, 'subject', NULL),
(168, 43, 44, 'target', NULL),
(169, 43, NULL, 'target_text', 'Nissan'),
(170, 43, NULL, 'admin_code', 'cerp_client.admin.modele'),
(171, 43, 1, 'subject', NULL),
(172, 44, 45, 'target', NULL),
(173, 44, NULL, 'target_text', 'car_logo_PNG1657.png'),
(174, 44, NULL, 'admin_code', 'sonata.media.admin.media'),
(175, 44, 1, 'subject', NULL),
(176, 45, 46, 'target', NULL),
(177, 45, NULL, 'target_text', 'MG'),
(178, 45, NULL, 'admin_code', 'cerp_client.admin.modele'),
(179, 45, 1, 'subject', NULL),
(180, 46, 47, 'target', NULL),
(181, 46, NULL, 'target_text', 'car_logo_PNG1656.png'),
(182, 46, NULL, 'admin_code', 'sonata.media.admin.media'),
(183, 46, 1, 'subject', NULL),
(184, 47, 48, 'target', NULL),
(185, 47, NULL, 'target_text', 'Mitsubishi'),
(186, 47, NULL, 'admin_code', 'cerp_client.admin.modele'),
(187, 47, 1, 'subject', NULL),
(188, 48, 49, 'target', NULL),
(189, 48, NULL, 'target_text', 'car_logo_PNG1655.png'),
(190, 48, NULL, 'admin_code', 'sonata.media.admin.media'),
(191, 48, 1, 'subject', NULL),
(192, 49, 50, 'target', NULL),
(193, 49, NULL, 'target_text', 'Mercedes Benz'),
(194, 49, NULL, 'admin_code', 'cerp_client.admin.modele'),
(195, 49, 1, 'subject', NULL),
(196, 50, 51, 'target', NULL),
(197, 50, NULL, 'target_text', 'car_logo_PNG1654.png'),
(198, 50, NULL, 'admin_code', 'sonata.media.admin.media'),
(199, 50, 1, 'subject', NULL),
(200, 51, 52, 'target', NULL),
(201, 51, NULL, 'target_text', 'Mazda'),
(202, 51, NULL, 'admin_code', 'cerp_client.admin.modele'),
(203, 51, 1, 'subject', NULL),
(204, 52, 53, 'target', NULL),
(205, 52, NULL, 'target_text', 'car_logo_PNG1653.png'),
(206, 52, NULL, 'admin_code', 'sonata.media.admin.media'),
(207, 52, 1, 'subject', NULL),
(208, 53, 54, 'target', NULL),
(209, 53, NULL, 'target_text', 'Land Rover'),
(210, 53, NULL, 'admin_code', 'cerp_client.admin.modele'),
(211, 53, 1, 'subject', NULL),
(212, 54, 55, 'target', NULL),
(213, 54, NULL, 'target_text', 'car_logo_PNG1652.png'),
(214, 54, NULL, 'admin_code', 'sonata.media.admin.media'),
(215, 54, 1, 'subject', NULL),
(216, 55, 56, 'target', NULL),
(217, 55, NULL, 'target_text', 'MINI'),
(218, 55, NULL, 'admin_code', 'cerp_client.admin.modele'),
(219, 55, 1, 'subject', NULL),
(220, 56, 57, 'target', NULL),
(221, 56, NULL, 'target_text', 'car_logo_PNG1651.png'),
(222, 56, NULL, 'admin_code', 'sonata.media.admin.media'),
(223, 56, 1, 'subject', NULL),
(224, 57, 58, 'target', NULL),
(225, 57, NULL, 'target_text', 'Lexus'),
(226, 57, NULL, 'admin_code', 'cerp_client.admin.modele'),
(227, 57, 1, 'subject', NULL),
(228, 58, 59, 'target', NULL),
(229, 58, NULL, 'target_text', 'car_logo_PNG1646.png'),
(230, 58, NULL, 'admin_code', 'sonata.media.admin.media'),
(231, 58, 1, 'subject', NULL),
(232, 59, 60, 'target', NULL),
(233, 59, NULL, 'target_text', 'Infiniti'),
(234, 59, NULL, 'admin_code', 'cerp_client.admin.modele'),
(235, 59, 1, 'subject', NULL),
(236, 60, 61, 'target', NULL),
(237, 60, NULL, 'target_text', 'car_logo_PNG1645.png'),
(238, 60, NULL, 'admin_code', 'sonata.media.admin.media'),
(239, 60, 1, 'subject', NULL),
(240, 61, 62, 'target', NULL),
(241, 61, NULL, 'target_text', 'Hyundai'),
(242, 61, NULL, 'admin_code', 'cerp_client.admin.modele'),
(243, 61, 1, 'subject', NULL),
(244, 62, 63, 'target', NULL),
(245, 62, NULL, 'target_text', 'car_logo_PNG1644.png'),
(246, 62, NULL, 'admin_code', 'sonata.media.admin.media'),
(247, 62, 1, 'subject', NULL),
(248, 63, 64, 'target', NULL),
(249, 63, NULL, 'target_text', 'Chevrolet'),
(250, 63, NULL, 'admin_code', 'cerp_client.admin.modele'),
(251, 63, 1, 'subject', NULL),
(252, 64, 65, 'target', NULL),
(253, 64, NULL, 'target_text', 'car_logo_PNG1650.png'),
(254, 64, NULL, 'admin_code', 'sonata.media.admin.media'),
(255, 64, 1, 'subject', NULL),
(256, 65, 66, 'target', NULL),
(257, 65, NULL, 'target_text', 'Lancia'),
(258, 65, NULL, 'admin_code', 'cerp_client.admin.modele'),
(259, 65, 1, 'subject', NULL),
(260, 66, 67, 'target', NULL),
(261, 66, NULL, 'target_text', 'car_logo_PNG1649.png'),
(262, 66, NULL, 'admin_code', 'sonata.media.admin.media'),
(263, 66, 1, 'subject', NULL),
(264, 67, 68, 'target', NULL),
(265, 67, NULL, 'target_text', 'KIA'),
(266, 67, NULL, 'admin_code', 'cerp_client.admin.modele'),
(267, 67, 1, 'subject', NULL),
(268, 68, 69, 'target', NULL),
(269, 68, NULL, 'target_text', 'car_logo_PNG1648.png'),
(270, 68, NULL, 'admin_code', 'sonata.media.admin.media'),
(271, 68, 1, 'subject', NULL),
(272, 69, 70, 'target', NULL),
(273, 69, NULL, 'target_text', 'Jeep'),
(274, 69, NULL, 'admin_code', 'cerp_client.admin.modele'),
(275, 69, 1, 'subject', NULL),
(276, 70, 71, 'target', NULL),
(277, 70, NULL, 'target_text', 'car_logo_PNG1647.png'),
(278, 70, NULL, 'admin_code', 'sonata.media.admin.media'),
(279, 70, 1, 'subject', NULL),
(280, 71, 72, 'target', NULL),
(281, 71, NULL, 'target_text', 'Jaguar'),
(282, 71, NULL, 'admin_code', 'cerp_client.admin.modele'),
(283, 71, 1, 'subject', NULL),
(284, 72, 73, 'target', NULL),
(285, 72, NULL, 'target_text', 'car_logo_PNG1643.png'),
(286, 72, NULL, 'admin_code', 'sonata.media.admin.media'),
(287, 72, 1, 'subject', NULL),
(288, 73, 74, 'target', NULL),
(289, 73, NULL, 'target_text', 'Honda'),
(290, 73, NULL, 'admin_code', 'cerp_client.admin.modele'),
(291, 73, 1, 'subject', NULL),
(292, 74, 75, 'target', NULL),
(293, 74, NULL, 'target_text', 'ferrari.png'),
(294, 74, NULL, 'admin_code', 'sonata.media.admin.media'),
(295, 74, 1, 'subject', NULL),
(296, 75, 76, 'target', NULL),
(297, 75, NULL, 'target_text', 'Ferrari'),
(298, 75, NULL, 'admin_code', 'cerp_client.admin.modele'),
(299, 75, 1, 'subject', NULL),
(300, 76, 77, 'target', NULL),
(301, 76, NULL, 'target_text', 'car_logo_PNG1640.png'),
(302, 76, NULL, 'admin_code', 'sonata.media.admin.media'),
(303, 76, 1, 'subject', NULL),
(304, 77, 78, 'target', NULL),
(305, 77, NULL, 'target_text', 'Audi'),
(306, 77, NULL, 'admin_code', 'cerp_client.admin.modele'),
(307, 77, 1, 'subject', NULL),
(308, 78, 79, 'target', NULL),
(309, 78, NULL, 'target_text', 'citroen.png'),
(310, 78, NULL, 'admin_code', 'sonata.media.admin.media'),
(311, 78, 1, 'subject', NULL),
(312, 79, 80, 'target', NULL),
(313, 79, NULL, 'target_text', 'Citroen'),
(314, 79, NULL, 'admin_code', 'cerp_client.admin.modele'),
(315, 79, 1, 'subject', NULL),
(316, 80, 20, 'target', NULL),
(317, 80, NULL, 'target_text', 'Alfa Romeo'),
(318, 80, NULL, 'admin_code', 'cerp_client.admin.modele'),
(319, 80, 1, 'subject', NULL),
(320, 81, 81, 'target', NULL),
(321, 81, NULL, 'target_text', 'fiat.png'),
(322, 81, NULL, 'admin_code', 'sonata.media.admin.media'),
(323, 81, 1, 'subject', NULL),
(324, 82, 82, 'target', NULL),
(325, 82, NULL, 'target_text', 'Fiat'),
(326, 82, NULL, 'admin_code', 'cerp_client.admin.modele'),
(327, 82, 1, 'subject', NULL),
(328, 83, NULL, 'target_text', 'Peugeot'),
(329, 83, NULL, 'admin_code', 'cerp_client.admin.marque'),
(330, 83, 1, 'subject', NULL),
(331, 84, NULL, 'target_text', 'Peugeot'),
(332, 84, NULL, 'admin_code', 'cerp_client.admin.marque'),
(333, 84, 1, 'subject', NULL),
(334, 85, NULL, 'target_text', 'Renault'),
(335, 85, NULL, 'admin_code', 'cerp_client.admin.marque'),
(336, 85, 1, 'subject', NULL),
(337, 86, 83, 'target', NULL),
(338, 86, NULL, 'target_text', 'Test'),
(339, 86, NULL, 'admin_code', 'cerp_client.admin.locataire'),
(340, 86, 1, 'subject', NULL),
(341, 87, 84, 'target', NULL),
(342, 87, NULL, 'target_text', 'Volkswagen'),
(343, 87, NULL, 'admin_code', 'cerp_client.admin.marque'),
(344, 87, 1, 'subject', NULL),
(345, 88, 84, 'target', NULL),
(346, 88, NULL, 'target_text', 'Volkswagen'),
(347, 88, NULL, 'admin_code', 'cerp_client.admin.marque'),
(348, 88, 1, 'subject', NULL);

DROP TABLE IF EXISTS `timeline__component`;
CREATE TABLE `timeline__component` (
  `id` int(11) NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `identifier` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `timeline__component` (`id`, `model`, `identifier`, `hash`) VALUES
(1, 'CERP\\UserBundle\\Entity\\User', 's:1:"1";', 'CERP\\UserBundle\\Entity\\User#s:1:"1";'),
(2, 'CERP\\ClientBundle\\Entity\\Equipement', 's:1:"1";', 'CERP\\ClientBundle\\Entity\\Equipement#s:1:"1";'),
(3, 'CERP\\UserBundle\\Entity\\User', 's:1:"2";', 'CERP\\UserBundle\\Entity\\User#s:1:"2";'),
(4, 'CERP\\ClientBundle\\Entity\\Equipement', 's:1:"2";', 'CERP\\ClientBundle\\Entity\\Equipement#s:1:"2";'),
(5, 'CERP\\ClientBundle\\Entity\\Equipement', 's:1:"3";', 'CERP\\ClientBundle\\Entity\\Equipement#s:1:"3";'),
(6, 'CERP\\ClientBundle\\Entity\\Equipement', 's:1:"4";', 'CERP\\ClientBundle\\Entity\\Equipement#s:1:"4";'),
(7, 'CERP\\ClientBundle\\Entity\\Equipement', 's:1:"5";', 'CERP\\ClientBundle\\Entity\\Equipement#s:1:"5";'),
(8, 'CERP\\ClientBundle\\Entity\\Equipement', 's:1:"6";', 'CERP\\ClientBundle\\Entity\\Equipement#s:1:"6";'),
(9, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:1:"1";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:1:"1";'),
(10, 'CERP\\ClientBundle\\Entity\\Modele', 's:1:"1";', 'CERP\\ClientBundle\\Entity\\Modele#s:1:"1";'),
(11, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:1:"2";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:1:"2";'),
(12, 'CERP\\ClientBundle\\Entity\\Modele', 's:1:"2";', 'CERP\\ClientBundle\\Entity\\Modele#s:1:"2";'),
(13, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:1:"3";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:1:"3";'),
(14, 'CERP\\ClientBundle\\Entity\\Modele', 's:1:"3";', 'CERP\\ClientBundle\\Entity\\Modele#s:1:"3";'),
(15, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:1:"4";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:1:"4";'),
(16, 'CERP\\ClientBundle\\Entity\\Modele', 's:1:"4";', 'CERP\\ClientBundle\\Entity\\Modele#s:1:"4";'),
(17, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:1:"5";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:1:"5";'),
(18, 'CERP\\ClientBundle\\Entity\\Modele', 's:1:"5";', 'CERP\\ClientBundle\\Entity\\Modele#s:1:"5";'),
(19, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:1:"6";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:1:"6";'),
(20, 'CERP\\ClientBundle\\Entity\\Modele', 's:1:"6";', 'CERP\\ClientBundle\\Entity\\Modele#s:1:"6";'),
(21, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:1:"7";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:1:"7";'),
(22, 'CERP\\ClientBundle\\Entity\\Modele', 's:1:"7";', 'CERP\\ClientBundle\\Entity\\Modele#s:1:"7";'),
(23, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:1:"8";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:1:"8";'),
(24, 'CERP\\ClientBundle\\Entity\\Modele', 's:1:"8";', 'CERP\\ClientBundle\\Entity\\Modele#s:1:"8";'),
(25, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:1:"9";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:1:"9";'),
(26, 'CERP\\ClientBundle\\Entity\\Modele', 's:1:"9";', 'CERP\\ClientBundle\\Entity\\Modele#s:1:"9";'),
(27, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"10";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"10";'),
(28, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"10";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"10";'),
(29, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"11";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"11";'),
(30, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"11";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"11";'),
(31, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"12";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"12";'),
(32, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"12";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"12";'),
(33, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"13";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"13";'),
(34, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"13";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"13";'),
(35, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"14";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"14";'),
(36, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"14";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"14";'),
(37, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"15";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"15";'),
(38, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"15";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"15";'),
(39, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"16";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"16";'),
(40, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"16";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"16";'),
(41, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"17";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"17";'),
(42, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"17";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"17";'),
(43, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"18";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"18";'),
(44, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"18";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"18";'),
(45, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"19";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"19";'),
(46, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"19";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"19";'),
(47, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"20";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"20";'),
(48, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"20";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"20";'),
(49, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"21";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"21";'),
(50, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"21";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"21";'),
(51, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"22";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"22";'),
(52, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"22";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"22";'),
(53, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"23";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"23";'),
(54, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"23";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"23";'),
(55, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"24";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"24";'),
(56, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"24";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"24";'),
(57, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"25";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"25";'),
(58, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"25";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"25";'),
(59, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"26";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"26";'),
(60, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"26";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"26";'),
(61, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"27";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"27";'),
(62, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"27";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"27";'),
(63, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"28";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"28";'),
(64, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"28";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"28";'),
(65, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"29";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"29";'),
(66, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"29";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"29";'),
(67, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"30";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"30";'),
(68, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"30";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"30";'),
(69, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"31";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"31";'),
(70, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"31";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"31";'),
(71, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"32";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"32";'),
(72, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"32";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"32";'),
(73, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"33";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"33";'),
(74, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"33";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"33";'),
(75, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"34";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"34";'),
(76, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"34";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"34";'),
(77, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"35";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"35";'),
(78, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"35";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"35";'),
(79, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"36";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"36";'),
(80, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"36";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"36";'),
(81, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:2:"37";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:2:"37";'),
(82, 'CERP\\ClientBundle\\Entity\\Modele', 's:2:"37";', 'CERP\\ClientBundle\\Entity\\Modele#s:2:"37";'),
(83, 'CERP\\ClientBundle\\Entity\\Locataire', 's:1:"7";', 'CERP\\ClientBundle\\Entity\\Locataire#s:1:"7";'),
(84, 'CERP\\ClientBundle\\Entity\\Marque', 's:1:"9";', 'CERP\\ClientBundle\\Entity\\Marque#s:1:"9";');

DROP TABLE IF EXISTS `timeline__timeline`;
CREATE TABLE `timeline__timeline` (
  `id` int(11) NOT NULL,
  `action_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=353 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `timeline__timeline` (`id`, `action_id`, `subject_id`, `context`, `type`, `created_at`) VALUES
(1, 1, 1, 'GLOBAL', 'timeline', '2016-02-03 23:37:33'),
(2, 1, 3, 'GLOBAL', 'timeline', '2016-02-03 23:37:33'),
(3, 1, 1, 'SONATA_ADMIN', 'timeline', '2016-02-03 23:37:33'),
(4, 1, 3, 'SONATA_ADMIN', 'timeline', '2016-02-03 23:37:33'),
(5, 2, 1, 'GLOBAL', 'timeline', '2016-02-03 23:37:52'),
(6, 2, 3, 'GLOBAL', 'timeline', '2016-02-03 23:37:52'),
(7, 2, 1, 'SONATA_ADMIN', 'timeline', '2016-02-03 23:37:52'),
(8, 2, 3, 'SONATA_ADMIN', 'timeline', '2016-02-03 23:37:52'),
(9, 3, 1, 'GLOBAL', 'timeline', '2016-02-03 23:38:11'),
(10, 3, 3, 'GLOBAL', 'timeline', '2016-02-03 23:38:11'),
(11, 3, 1, 'SONATA_ADMIN', 'timeline', '2016-02-03 23:38:11'),
(12, 3, 3, 'SONATA_ADMIN', 'timeline', '2016-02-03 23:38:11'),
(13, 4, 1, 'GLOBAL', 'timeline', '2016-02-03 23:38:53'),
(14, 4, 3, 'GLOBAL', 'timeline', '2016-02-03 23:38:53'),
(15, 4, 1, 'SONATA_ADMIN', 'timeline', '2016-02-03 23:38:53'),
(16, 4, 3, 'SONATA_ADMIN', 'timeline', '2016-02-03 23:38:53'),
(17, 5, 1, 'GLOBAL', 'timeline', '2016-02-03 23:39:08'),
(18, 5, 3, 'GLOBAL', 'timeline', '2016-02-03 23:39:08'),
(19, 5, 1, 'SONATA_ADMIN', 'timeline', '2016-02-03 23:39:08'),
(20, 5, 3, 'SONATA_ADMIN', 'timeline', '2016-02-03 23:39:08'),
(21, 6, 1, 'GLOBAL', 'timeline', '2016-02-03 23:39:49'),
(22, 6, 3, 'GLOBAL', 'timeline', '2016-02-03 23:39:49'),
(23, 6, 1, 'SONATA_ADMIN', 'timeline', '2016-02-03 23:39:49'),
(24, 6, 3, 'SONATA_ADMIN', 'timeline', '2016-02-03 23:39:49'),
(25, 7, 1, 'GLOBAL', 'timeline', '2016-02-05 23:54:26'),
(26, 7, 3, 'GLOBAL', 'timeline', '2016-02-05 23:54:26'),
(27, 7, 1, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:54:26'),
(28, 7, 3, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:54:26'),
(29, 8, 1, 'GLOBAL', 'timeline', '2016-02-05 23:54:32'),
(30, 8, 3, 'GLOBAL', 'timeline', '2016-02-05 23:54:32'),
(31, 8, 1, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:54:32'),
(32, 8, 3, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:54:32'),
(33, 9, 1, 'GLOBAL', 'timeline', '2016-02-05 23:55:01'),
(34, 9, 3, 'GLOBAL', 'timeline', '2016-02-05 23:55:01'),
(35, 9, 1, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:55:01'),
(36, 9, 3, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:55:01'),
(37, 10, 1, 'GLOBAL', 'timeline', '2016-02-05 23:55:05'),
(38, 10, 3, 'GLOBAL', 'timeline', '2016-02-05 23:55:05'),
(39, 10, 1, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:55:05'),
(40, 10, 3, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:55:05'),
(41, 11, 1, 'GLOBAL', 'timeline', '2016-02-05 23:55:38'),
(42, 11, 3, 'GLOBAL', 'timeline', '2016-02-05 23:55:38'),
(43, 11, 1, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:55:38'),
(44, 11, 3, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:55:38'),
(45, 12, 1, 'GLOBAL', 'timeline', '2016-02-05 23:55:42'),
(46, 12, 3, 'GLOBAL', 'timeline', '2016-02-05 23:55:42'),
(47, 12, 1, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:55:42'),
(48, 12, 3, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:55:42'),
(49, 13, 1, 'GLOBAL', 'timeline', '2016-02-05 23:56:38'),
(50, 13, 3, 'GLOBAL', 'timeline', '2016-02-05 23:56:38'),
(51, 13, 1, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:56:38'),
(52, 13, 3, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:56:38'),
(53, 14, 1, 'GLOBAL', 'timeline', '2016-02-05 23:56:48'),
(54, 14, 3, 'GLOBAL', 'timeline', '2016-02-05 23:56:48'),
(55, 14, 1, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:56:48'),
(56, 14, 3, 'SONATA_ADMIN', 'timeline', '2016-02-05 23:56:48'),
(57, 15, 1, 'GLOBAL', 'timeline', '2016-02-06 23:33:50'),
(58, 15, 3, 'GLOBAL', 'timeline', '2016-02-06 23:33:50'),
(59, 15, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:33:50'),
(60, 15, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:33:50'),
(61, 16, 1, 'GLOBAL', 'timeline', '2016-02-06 23:36:05'),
(62, 16, 3, 'GLOBAL', 'timeline', '2016-02-06 23:36:05'),
(63, 16, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:36:05'),
(64, 16, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:36:05'),
(65, 17, 1, 'GLOBAL', 'timeline', '2016-02-06 23:36:11'),
(66, 17, 3, 'GLOBAL', 'timeline', '2016-02-06 23:36:11'),
(67, 17, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:36:11'),
(68, 17, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:36:11'),
(69, 18, 1, 'GLOBAL', 'timeline', '2016-02-06 23:36:41'),
(70, 18, 3, 'GLOBAL', 'timeline', '2016-02-06 23:36:41'),
(71, 18, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:36:41'),
(72, 18, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:36:41'),
(73, 19, 1, 'GLOBAL', 'timeline', '2016-02-06 23:36:49'),
(74, 19, 3, 'GLOBAL', 'timeline', '2016-02-06 23:36:49'),
(75, 19, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:36:49'),
(76, 19, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:36:49'),
(77, 20, 1, 'GLOBAL', 'timeline', '2016-02-06 23:37:20'),
(78, 20, 3, 'GLOBAL', 'timeline', '2016-02-06 23:37:20'),
(79, 20, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:37:20'),
(80, 20, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:37:20'),
(81, 21, 1, 'GLOBAL', 'timeline', '2016-02-06 23:37:36'),
(82, 21, 3, 'GLOBAL', 'timeline', '2016-02-06 23:37:36'),
(83, 21, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:37:36'),
(84, 21, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:37:36'),
(85, 22, 1, 'GLOBAL', 'timeline', '2016-02-06 23:38:03'),
(86, 22, 3, 'GLOBAL', 'timeline', '2016-02-06 23:38:03'),
(87, 22, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:38:03'),
(88, 22, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:38:03'),
(89, 23, 1, 'GLOBAL', 'timeline', '2016-02-06 23:38:07'),
(90, 23, 3, 'GLOBAL', 'timeline', '2016-02-06 23:38:07'),
(91, 23, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:38:07'),
(92, 23, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:38:07'),
(93, 24, 1, 'GLOBAL', 'timeline', '2016-02-06 23:38:37'),
(94, 24, 3, 'GLOBAL', 'timeline', '2016-02-06 23:38:37'),
(95, 24, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:38:37'),
(96, 24, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:38:37'),
(97, 25, 1, 'GLOBAL', 'timeline', '2016-02-06 23:38:50'),
(98, 25, 3, 'GLOBAL', 'timeline', '2016-02-06 23:38:50'),
(99, 25, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:38:50'),
(100, 25, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:38:50'),
(101, 26, 1, 'GLOBAL', 'timeline', '2016-02-06 23:39:21'),
(102, 26, 3, 'GLOBAL', 'timeline', '2016-02-06 23:39:21'),
(103, 26, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:39:21'),
(104, 26, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:39:21'),
(105, 27, 1, 'GLOBAL', 'timeline', '2016-02-06 23:39:25'),
(106, 27, 3, 'GLOBAL', 'timeline', '2016-02-06 23:39:25'),
(107, 27, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:39:25'),
(108, 27, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:39:25'),
(109, 28, 1, 'GLOBAL', 'timeline', '2016-02-06 23:40:17'),
(110, 28, 3, 'GLOBAL', 'timeline', '2016-02-06 23:40:17'),
(111, 28, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:40:17'),
(112, 28, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:40:17'),
(113, 29, 1, 'GLOBAL', 'timeline', '2016-02-06 23:40:33'),
(114, 29, 3, 'GLOBAL', 'timeline', '2016-02-06 23:40:33'),
(115, 29, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:40:33'),
(116, 29, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:40:33'),
(117, 30, 1, 'GLOBAL', 'timeline', '2016-02-06 23:41:46'),
(118, 30, 3, 'GLOBAL', 'timeline', '2016-02-06 23:41:46'),
(119, 30, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:41:46'),
(120, 30, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:41:46'),
(121, 31, 1, 'GLOBAL', 'timeline', '2016-02-06 23:41:49'),
(122, 31, 3, 'GLOBAL', 'timeline', '2016-02-06 23:41:49'),
(123, 31, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:41:49'),
(124, 31, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:41:49'),
(125, 32, 1, 'GLOBAL', 'timeline', '2016-02-06 23:42:16'),
(126, 32, 3, 'GLOBAL', 'timeline', '2016-02-06 23:42:16'),
(127, 32, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:42:16'),
(128, 32, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:42:16'),
(129, 33, 1, 'GLOBAL', 'timeline', '2016-02-06 23:42:19'),
(130, 33, 3, 'GLOBAL', 'timeline', '2016-02-06 23:42:19'),
(131, 33, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:42:19'),
(132, 33, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:42:19'),
(133, 34, 1, 'GLOBAL', 'timeline', '2016-02-06 23:42:48'),
(134, 34, 3, 'GLOBAL', 'timeline', '2016-02-06 23:42:48'),
(135, 34, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:42:48'),
(136, 34, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:42:48'),
(137, 35, 1, 'GLOBAL', 'timeline', '2016-02-06 23:42:58'),
(138, 35, 3, 'GLOBAL', 'timeline', '2016-02-06 23:42:58'),
(139, 35, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:42:58'),
(140, 35, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:42:58'),
(141, 36, 1, 'GLOBAL', 'timeline', '2016-02-06 23:43:25'),
(142, 36, 3, 'GLOBAL', 'timeline', '2016-02-06 23:43:25'),
(143, 36, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:43:25'),
(144, 36, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:43:25'),
(145, 37, 1, 'GLOBAL', 'timeline', '2016-02-06 23:43:29'),
(146, 37, 3, 'GLOBAL', 'timeline', '2016-02-06 23:43:29'),
(147, 37, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:43:29'),
(148, 37, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:43:29'),
(149, 38, 1, 'GLOBAL', 'timeline', '2016-02-06 23:43:55'),
(150, 38, 3, 'GLOBAL', 'timeline', '2016-02-06 23:43:55'),
(151, 38, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:43:55'),
(152, 38, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:43:55'),
(153, 39, 1, 'GLOBAL', 'timeline', '2016-02-06 23:43:59'),
(154, 39, 3, 'GLOBAL', 'timeline', '2016-02-06 23:43:59'),
(155, 39, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:43:59'),
(156, 39, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:43:59'),
(157, 40, 1, 'GLOBAL', 'timeline', '2016-02-06 23:44:27'),
(158, 40, 3, 'GLOBAL', 'timeline', '2016-02-06 23:44:27'),
(159, 40, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:44:27'),
(160, 40, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:44:27'),
(161, 41, 1, 'GLOBAL', 'timeline', '2016-02-06 23:44:32'),
(162, 41, 3, 'GLOBAL', 'timeline', '2016-02-06 23:44:32'),
(163, 41, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:44:32'),
(164, 41, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:44:32'),
(165, 42, 1, 'GLOBAL', 'timeline', '2016-02-06 23:45:05'),
(166, 42, 3, 'GLOBAL', 'timeline', '2016-02-06 23:45:05'),
(167, 42, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:45:05'),
(168, 42, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:45:05'),
(169, 43, 1, 'GLOBAL', 'timeline', '2016-02-06 23:45:12'),
(170, 43, 3, 'GLOBAL', 'timeline', '2016-02-06 23:45:12'),
(171, 43, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:45:12'),
(172, 43, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:45:12'),
(173, 44, 1, 'GLOBAL', 'timeline', '2016-02-06 23:45:41'),
(174, 44, 3, 'GLOBAL', 'timeline', '2016-02-06 23:45:41'),
(175, 44, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:45:41'),
(176, 44, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:45:41'),
(177, 45, 1, 'GLOBAL', 'timeline', '2016-02-06 23:45:44'),
(178, 45, 3, 'GLOBAL', 'timeline', '2016-02-06 23:45:44'),
(179, 45, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:45:44'),
(180, 45, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:45:44'),
(181, 46, 1, 'GLOBAL', 'timeline', '2016-02-06 23:46:08'),
(182, 46, 3, 'GLOBAL', 'timeline', '2016-02-06 23:46:08'),
(183, 46, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:46:08'),
(184, 46, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:46:08'),
(185, 47, 1, 'GLOBAL', 'timeline', '2016-02-06 23:46:12'),
(186, 47, 3, 'GLOBAL', 'timeline', '2016-02-06 23:46:12'),
(187, 47, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:46:12'),
(188, 47, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:46:12'),
(189, 48, 1, 'GLOBAL', 'timeline', '2016-02-06 23:46:36'),
(190, 48, 3, 'GLOBAL', 'timeline', '2016-02-06 23:46:36'),
(191, 48, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:46:36'),
(192, 48, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:46:36'),
(193, 49, 1, 'GLOBAL', 'timeline', '2016-02-06 23:46:40'),
(194, 49, 3, 'GLOBAL', 'timeline', '2016-02-06 23:46:40'),
(195, 49, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:46:40'),
(196, 49, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:46:40'),
(197, 50, 1, 'GLOBAL', 'timeline', '2016-02-06 23:47:15'),
(198, 50, 3, 'GLOBAL', 'timeline', '2016-02-06 23:47:15'),
(199, 50, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:47:15'),
(200, 50, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:47:15'),
(201, 51, 1, 'GLOBAL', 'timeline', '2016-02-06 23:47:19'),
(202, 51, 3, 'GLOBAL', 'timeline', '2016-02-06 23:47:19'),
(203, 51, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:47:19'),
(204, 51, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:47:19'),
(205, 52, 1, 'GLOBAL', 'timeline', '2016-02-06 23:47:44'),
(206, 52, 3, 'GLOBAL', 'timeline', '2016-02-06 23:47:44'),
(207, 52, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:47:44'),
(208, 52, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:47:44'),
(209, 53, 1, 'GLOBAL', 'timeline', '2016-02-06 23:47:48'),
(210, 53, 3, 'GLOBAL', 'timeline', '2016-02-06 23:47:48'),
(211, 53, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:47:48'),
(212, 53, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:47:48'),
(213, 54, 1, 'GLOBAL', 'timeline', '2016-02-06 23:48:13'),
(214, 54, 3, 'GLOBAL', 'timeline', '2016-02-06 23:48:13'),
(215, 54, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:48:13'),
(216, 54, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:48:13'),
(217, 55, 1, 'GLOBAL', 'timeline', '2016-02-06 23:48:16'),
(218, 55, 3, 'GLOBAL', 'timeline', '2016-02-06 23:48:16'),
(219, 55, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:48:16'),
(220, 55, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:48:16'),
(221, 56, 1, 'GLOBAL', 'timeline', '2016-02-06 23:48:52'),
(222, 56, 3, 'GLOBAL', 'timeline', '2016-02-06 23:48:52'),
(223, 56, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:48:52'),
(224, 56, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:48:52'),
(225, 57, 1, 'GLOBAL', 'timeline', '2016-02-06 23:48:55'),
(226, 57, 3, 'GLOBAL', 'timeline', '2016-02-06 23:48:55'),
(227, 57, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:48:55'),
(228, 57, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:48:55'),
(229, 58, 1, 'GLOBAL', 'timeline', '2016-02-06 23:50:00'),
(230, 58, 3, 'GLOBAL', 'timeline', '2016-02-06 23:50:00'),
(231, 58, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:50:00'),
(232, 58, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:50:00'),
(233, 59, 1, 'GLOBAL', 'timeline', '2016-02-06 23:50:04'),
(234, 59, 3, 'GLOBAL', 'timeline', '2016-02-06 23:50:04'),
(235, 59, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:50:04'),
(236, 59, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:50:04'),
(237, 60, 1, 'GLOBAL', 'timeline', '2016-02-06 23:50:30'),
(238, 60, 3, 'GLOBAL', 'timeline', '2016-02-06 23:50:30'),
(239, 60, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:50:30'),
(240, 60, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:50:30'),
(241, 61, 1, 'GLOBAL', 'timeline', '2016-02-06 23:50:34'),
(242, 61, 3, 'GLOBAL', 'timeline', '2016-02-06 23:50:34'),
(243, 61, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:50:34'),
(244, 61, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:50:34'),
(245, 62, 1, 'GLOBAL', 'timeline', '2016-02-06 23:50:59'),
(246, 62, 3, 'GLOBAL', 'timeline', '2016-02-06 23:50:59'),
(247, 62, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:50:59'),
(248, 62, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:50:59'),
(249, 63, 1, 'GLOBAL', 'timeline', '2016-02-06 23:51:02'),
(250, 63, 3, 'GLOBAL', 'timeline', '2016-02-06 23:51:02'),
(251, 63, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:51:02'),
(252, 63, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:51:02'),
(253, 64, 1, 'GLOBAL', 'timeline', '2016-02-06 23:51:27'),
(254, 64, 3, 'GLOBAL', 'timeline', '2016-02-06 23:51:27'),
(255, 64, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:51:27'),
(256, 64, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:51:27'),
(257, 65, 1, 'GLOBAL', 'timeline', '2016-02-06 23:51:31'),
(258, 65, 3, 'GLOBAL', 'timeline', '2016-02-06 23:51:31'),
(259, 65, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:51:31'),
(260, 65, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:51:31'),
(261, 66, 1, 'GLOBAL', 'timeline', '2016-02-06 23:51:54'),
(262, 66, 3, 'GLOBAL', 'timeline', '2016-02-06 23:51:54'),
(263, 66, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:51:54'),
(264, 66, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:51:54'),
(265, 67, 1, 'GLOBAL', 'timeline', '2016-02-06 23:51:57'),
(266, 67, 3, 'GLOBAL', 'timeline', '2016-02-06 23:51:57'),
(267, 67, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:51:57'),
(268, 67, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:51:57'),
(269, 68, 1, 'GLOBAL', 'timeline', '2016-02-06 23:52:21'),
(270, 68, 3, 'GLOBAL', 'timeline', '2016-02-06 23:52:21'),
(271, 68, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:52:21'),
(272, 68, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:52:21'),
(273, 69, 1, 'GLOBAL', 'timeline', '2016-02-06 23:52:24'),
(274, 69, 3, 'GLOBAL', 'timeline', '2016-02-06 23:52:24'),
(275, 69, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:52:24'),
(276, 69, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:52:24'),
(277, 70, 1, 'GLOBAL', 'timeline', '2016-02-06 23:52:57'),
(278, 70, 3, 'GLOBAL', 'timeline', '2016-02-06 23:52:57'),
(279, 70, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:52:57'),
(280, 70, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:52:57'),
(281, 71, 1, 'GLOBAL', 'timeline', '2016-02-06 23:53:06'),
(282, 71, 3, 'GLOBAL', 'timeline', '2016-02-06 23:53:06'),
(283, 71, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:53:06'),
(284, 71, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:53:06'),
(285, 72, 1, 'GLOBAL', 'timeline', '2016-02-06 23:53:30'),
(286, 72, 3, 'GLOBAL', 'timeline', '2016-02-06 23:53:30'),
(287, 72, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:53:30'),
(288, 72, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:53:30'),
(289, 73, 1, 'GLOBAL', 'timeline', '2016-02-06 23:53:36'),
(290, 73, 3, 'GLOBAL', 'timeline', '2016-02-06 23:53:36'),
(291, 73, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:53:36'),
(292, 73, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:53:36'),
(293, 74, 1, 'GLOBAL', 'timeline', '2016-02-06 23:54:02'),
(294, 74, 3, 'GLOBAL', 'timeline', '2016-02-06 23:54:02'),
(295, 74, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:54:02'),
(296, 74, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:54:02'),
(297, 75, 1, 'GLOBAL', 'timeline', '2016-02-06 23:54:05'),
(298, 75, 3, 'GLOBAL', 'timeline', '2016-02-06 23:54:05'),
(299, 75, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:54:05'),
(300, 75, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:54:05'),
(301, 76, 1, 'GLOBAL', 'timeline', '2016-02-06 23:54:41'),
(302, 76, 3, 'GLOBAL', 'timeline', '2016-02-06 23:54:41'),
(303, 76, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:54:41'),
(304, 76, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:54:41'),
(305, 77, 1, 'GLOBAL', 'timeline', '2016-02-06 23:54:44'),
(306, 77, 3, 'GLOBAL', 'timeline', '2016-02-06 23:54:44'),
(307, 77, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:54:44'),
(308, 77, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:54:44'),
(309, 78, 1, 'GLOBAL', 'timeline', '2016-02-06 23:55:50'),
(310, 78, 3, 'GLOBAL', 'timeline', '2016-02-06 23:55:50'),
(311, 78, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:55:50'),
(312, 78, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:55:50'),
(313, 79, 1, 'GLOBAL', 'timeline', '2016-02-06 23:55:54'),
(314, 79, 3, 'GLOBAL', 'timeline', '2016-02-06 23:55:54'),
(315, 79, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:55:54'),
(316, 79, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:55:54'),
(317, 80, 1, 'GLOBAL', 'timeline', '2016-02-06 23:57:13'),
(318, 80, 3, 'GLOBAL', 'timeline', '2016-02-06 23:57:13'),
(319, 80, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:57:13'),
(320, 80, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:57:13'),
(321, 81, 1, 'GLOBAL', 'timeline', '2016-02-06 23:59:03'),
(322, 81, 3, 'GLOBAL', 'timeline', '2016-02-06 23:59:03'),
(323, 81, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:59:03'),
(324, 81, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:59:03'),
(325, 82, 1, 'GLOBAL', 'timeline', '2016-02-06 23:59:10'),
(326, 82, 3, 'GLOBAL', 'timeline', '2016-02-06 23:59:10'),
(327, 82, 1, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:59:10'),
(328, 82, 3, 'SONATA_ADMIN', 'timeline', '2016-02-06 23:59:10'),
(329, 83, 1, 'GLOBAL', 'timeline', '2016-02-07 00:28:11'),
(330, 83, 3, 'GLOBAL', 'timeline', '2016-02-07 00:28:11'),
(331, 83, 1, 'SONATA_ADMIN', 'timeline', '2016-02-07 00:28:11'),
(332, 83, 3, 'SONATA_ADMIN', 'timeline', '2016-02-07 00:28:11'),
(333, 84, 1, 'GLOBAL', 'timeline', '2016-02-07 00:35:55'),
(334, 84, 3, 'GLOBAL', 'timeline', '2016-02-07 00:35:55'),
(335, 84, 1, 'SONATA_ADMIN', 'timeline', '2016-02-07 00:35:55'),
(336, 84, 3, 'SONATA_ADMIN', 'timeline', '2016-02-07 00:35:55'),
(337, 85, 1, 'GLOBAL', 'timeline', '2016-02-07 00:36:22'),
(338, 85, 3, 'GLOBAL', 'timeline', '2016-02-07 00:36:22'),
(339, 85, 1, 'SONATA_ADMIN', 'timeline', '2016-02-07 00:36:22'),
(340, 85, 3, 'SONATA_ADMIN', 'timeline', '2016-02-07 00:36:22'),
(341, 86, 1, 'GLOBAL', 'timeline', '2016-02-08 14:39:53'),
(342, 86, 3, 'GLOBAL', 'timeline', '2016-02-08 14:39:53'),
(343, 86, 1, 'SONATA_ADMIN', 'timeline', '2016-02-08 14:39:53'),
(344, 86, 3, 'SONATA_ADMIN', 'timeline', '2016-02-08 14:39:53'),
(345, 87, 1, 'GLOBAL', 'timeline', '2016-02-08 18:46:47'),
(346, 87, 3, 'GLOBAL', 'timeline', '2016-02-08 18:46:47'),
(347, 87, 1, 'SONATA_ADMIN', 'timeline', '2016-02-08 18:46:47'),
(348, 87, 3, 'SONATA_ADMIN', 'timeline', '2016-02-08 18:46:47'),
(349, 88, 1, 'GLOBAL', 'timeline', '2016-02-08 18:47:39'),
(350, 88, 3, 'GLOBAL', 'timeline', '2016-02-08 18:47:39'),
(351, 88, 1, 'SONATA_ADMIN', 'timeline', '2016-02-08 18:47:39'),
(352, 88, 3, 'SONATA_ADMIN', 'timeline', '2016-02-08 18:47:39');

DROP TABLE IF EXISTS `typevehicule`;
CREATE TABLE `typevehicule` (
  `id` int(11) NOT NULL,
  `Libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `typevehicule` (`id`, `Libelle`) VALUES
(1, 'Citadine'),
(2, '4x4');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `media_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expire_at` datetime DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twoStepVerificationCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `users` (`id`, `media_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expire_at`, `facebook_id`, `facebook_access_token`, `google_id`, `google_access_token`, `first_name`, `middle_name`, `last_name`, `birthday`, `created_at`, `updated_at`, `gender`, `locale`, `timezone`, `phone`, `twoStepVerificationCode`) VALUES
(1, NULL, 'admin', 'admin', 'admin@admin.com', 'admin@admin.com', 1, 'h9bpsitg6aok0848oo0c8c8kooow84o', 'RlcCLaaBMNIlv5mZ1osrIJDkT7uCIFMJYlJF1SmeT+mqjbIXbF6gsLdXGPy8xwJ0WPpms1A0F7RbTe1siI8o7A==', '2016-02-08 14:37:33', 0, NULL, NULL, NULL, 'a:3:{i:0;s:9:"ROLE_USER";i:1;s:10:"ROLE_ADMIN";i:2;s:16:"ROLE_SUPER_ADMIN";}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-11-26 20:12:07', '2015-11-26 20:12:07', 'u', NULL, NULL, NULL, NULL),
(2, NULL, 'touzani', 'touzani', 'seddi@gm.com', 'seddi@gm.com', 1, '48o3bxohu9es0og44g8gcwgssk44gws', 'yFT4dCt7snPHHqRiREMLcff83Sjsncn2kcpKA34GwEfPdDp27Xl4x1QwGyAiPetIYhRAFDtkurxieGsDy0BG1A==', '2016-01-29 21:01:02', 0, NULL, NULL, NULL, 'a:3:{i:0;s:9:"ROLE_USER";i:1;s:10:"ROLE_ADMIN";i:2;s:16:"ROLE_SUPER_ADMIN";}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-12-01 16:21:18', '2015-12-01 16:21:18', 'u', NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `version`;
CREATE TABLE `version` (
  `id` int(11) NOT NULL,
  `Libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `voiture`;
CREATE TABLE `voiture` (
  `id` int(11) NOT NULL,
  `energie_id` int(11) DEFAULT NULL,
  `boitevitesse_id` int(11) DEFAULT NULL,
  `matricule` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `couleur` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `puissance_fiscale` int(11) NOT NULL,
  `nbr_vitesse` int(11) NOT NULL,
  `annee` int(11) NOT NULL,
  `nbr_porte` int(11) NOT NULL,
  `actif` int(11) DEFAULT NULL,
  `chassis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kilometrage` int(11) NOT NULL,
  `typeVehicule_id` int(11) DEFAULT NULL,
  `marque_id` int(11) DEFAULT NULL,
  `prixLocation` double DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `dateCreation` datetime NOT NULL,
  `dateMiseEnCirculation` datetime NOT NULL,
  `vidange` int(11) NOT NULL,
  `dateAssurance` datetime NOT NULL,
  `dateVisiteTechnique` datetime NOT NULL,
  `modele` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prixVoiture` double DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `voiture` (`id`, `energie_id`, `boitevitesse_id`, `matricule`, `couleur`, `puissance_fiscale`, `nbr_vitesse`, `annee`, `nbr_porte`, `actif`, `chassis`, `kilometrage`, `typeVehicule_id`, `marque_id`, `prixLocation`, `user_id`, `dateCreation`, `dateMiseEnCirculation`, `vidange`, `dateAssurance`, `dateVisiteTechnique`, `modele`, `prixVoiture`) VALUES
(6, 1, 1, '56789 - T - 67', '#13c445', 6, 6, 2016, 3, NULL, 'kljkljkl', 20000, 1, 27, 2500, 1, '2016-02-07 01:38:05', '2016-02-01 00:00:00', 77890, '2016-02-03 00:00:00', '1949-05-02 00:00:00', 'Serie A', 10000);

DROP TABLE IF EXISTS `Voiture_audit`;
CREATE TABLE `Voiture_audit` (
  `id` int(11) NOT NULL,
  `rev` int(11) NOT NULL,
  `energie_id` int(11) DEFAULT NULL,
  `boitevitesse_id` int(11) DEFAULT NULL,
  `marque_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `matricule` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `couleur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `puissance_fiscale` int(11) DEFAULT NULL,
  `nbr_vitesse` int(11) DEFAULT NULL,
  `annee` int(11) DEFAULT NULL,
  `prixLocation` double DEFAULT NULL,
  `nbr_porte` int(11) DEFAULT NULL,
  `dateCreation` datetime DEFAULT NULL,
  `dateMiseEnCirculation` datetime DEFAULT NULL,
  `chassis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modele` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `actif` int(11) DEFAULT NULL,
  `kilometrage` int(11) DEFAULT NULL,
  `vidange` int(11) DEFAULT NULL,
  `dateAssurance` datetime DEFAULT NULL,
  `dateVisiteTechnique` datetime DEFAULT NULL,
  `typeVehicule_id` int(11) DEFAULT NULL,
  `revtype` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `prixVoiture` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `Voiture_audit` (`id`, `rev`, `energie_id`, `boitevitesse_id`, `marque_id`, `user_id`, `matricule`, `couleur`, `puissance_fiscale`, `nbr_vitesse`, `annee`, `prixLocation`, `nbr_porte`, `dateCreation`, `dateMiseEnCirculation`, `chassis`, `modele`, `actif`, `kilometrage`, `vidange`, `dateAssurance`, `dateVisiteTechnique`, `typeVehicule_id`, `revtype`, `prixVoiture`) VALUES
(6, 2, 1, 1, 13, 1, '56789 - T - 67', '#2e2e89', 6, 6, 2016, 2500, 3, '2016-02-07 01:38:05', '2016-02-01 00:00:00', 'kljkljkl', 'Serie A', NULL, 20000, 77890, '2016-02-03 00:00:00', '1949-05-02 00:00:00', 1, 'UPD', NULL),
(6, 3, 1, 1, 13, 1, '56789 - T - 67', '#7c0e5e', 6, 6, 2016, 2500, 3, '2016-02-07 01:38:05', '2016-02-01 00:00:00', 'kljkljkl', 'Serie A', NULL, 20000, 77890, '2016-02-03 00:00:00', '1949-05-02 00:00:00', 1, 'UPD', NULL),
(6, 4, 1, 1, 13, 1, '56789 - T - 67', '#1225ce', 6, 6, 2016, 2500, 3, '2016-02-07 01:38:05', '2016-02-01 00:00:00', 'kljkljkl', 'Serie A', NULL, 20000, 77890, '2016-02-03 00:00:00', '1949-05-02 00:00:00', 1, 'UPD', NULL),
(6, 5, 1, 1, 9, 1, '56789 - T - 67', '#1124ce', 6, 6, 2016, 2500, 3, '2016-02-07 01:38:05', '2016-02-01 00:00:00', 'kljkljkl', 'Serie A', NULL, 20000, 77890, '2016-02-03 00:00:00', '1949-05-02 00:00:00', 1, 'UPD', NULL),
(6, 8, 1, 1, 34, 1, '56789 - T - 67', '#1024ce', 6, 6, 2016, 2500, 3, '2016-02-07 01:38:05', '2016-02-01 00:00:00', 'kljkljkl', 'Serie A', NULL, 20000, 77890, '2016-02-03 00:00:00', '1949-05-02 00:00:00', 1, 'UPD', NULL),
(6, 9, 1, 1, 34, 1, '56789 - T - 67', '#0f24ce', 6, 6, 2016, 2500, 3, '2016-02-07 01:38:05', '2016-02-01 00:00:00', 'kljkljkl', 'Serie A', NULL, 20000, 77890, '2016-02-03 00:00:00', '1949-05-02 00:00:00', 1, 'UPD', 10000),
(6, 10, 1, 1, 27, 1, '56789 - T - 67', '#0e24ce', 6, 6, 2016, 2500, 3, '2016-02-07 01:38:05', '2016-02-01 00:00:00', 'kljkljkl', 'Serie A', NULL, 20000, 77890, '2016-02-03 00:00:00', '1949-05-02 00:00:00', 1, 'UPD', 10000),
(6, 11, 1, 1, 27, 1, '56789 - T - 67', '#13c445', 6, 6, 2016, 2500, 3, '2016-02-07 01:38:05', '2016-02-01 00:00:00', 'kljkljkl', 'Serie A', NULL, 20000, 77890, '2016-02-03 00:00:00', '1949-05-02 00:00:00', 1, 'UPD', 10000);

DROP TABLE IF EXISTS `voiture_equipement`;
CREATE TABLE `voiture_equipement` (
  `voiture_id` int(11) NOT NULL,
  `equipement_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `voiture_equipement` (`voiture_id`, `equipement_id`) VALUES
(6, 3),
(6, 5);


ALTER TABLE `acl_classes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`);

ALTER TABLE `acl_entries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  ADD KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  ADD KEY `IDX_46C8B806EA000B10` (`class_id`),
  ADD KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_46C8B806DF9183C9` (`security_identity_id`);

ALTER TABLE `acl_object_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  ADD KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`);

ALTER TABLE `acl_object_identity_ancestors`
  ADD PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  ADD KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_825DE299C671CEA1` (`ancestor_id`);

ALTER TABLE `acl_security_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`);

ALTER TABLE `anomalie`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `Anomalie_audit`
  ADD PRIMARY KEY (`id`,`rev`);

ALTER TABLE `blacklist`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_19D47E18D8A38199` (`locataire_id`);

ALTER TABLE `BlackList_audit`
  ADD PRIMARY KEY (`id`,`rev`);

ALTER TABLE `blacklist_incident`
  ADD PRIMARY KEY (`blacklist_id`,`incident_id`),
  ADD KEY `IDX_286C007193FA9730` (`blacklist_id`),
  ADD KEY `IDX_286C007159E53FB9` (`incident_id`);

ALTER TABLE `block`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_831B9722C4663E4` (`page_id`);

ALTER TABLE `boitevitesse`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `classification__category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_43629B36727ACA70` (`parent_id`),
  ADD KEY `IDX_43629B36E25D857E` (`context`),
  ADD KEY `IDX_43629B36EA9FDD75` (`media_id`);

ALTER TABLE `classification__collection`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tag_collection` (`slug`,`context`),
  ADD KEY `IDX_A406B56AE25D857E` (`context`),
  ADD KEY `IDX_A406B56AEA9FDD75` (`media_id`);

ALTER TABLE `classification__context`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `classification__tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tag_context` (`slug`,`context`),
  ADD KEY `IDX_CA57A1C7E25D857E` (`context`);

ALTER TABLE `energie`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `equipement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F7E9C523181A8BA` (`voiture_id`);

ALTER TABLE `ext_log_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `log_user_lookup_idx` (`username`),
  ADD KEY `log_class_lookup_idx` (`objectClass`),
  ADD KEY `log_date_lookup_idx` (`loggedAt`),
  ADD KEY `log_version_lookup_idx` (`objectId`,`objectClass`,`username`);

ALTER TABLE `ext_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lookup_unique_idx` (`locale`,`objectClass`,`field`,`foreignKey`),
  ADD KEY `translations_lookup_idx` (`locale`,`objectClass`,`foreignKey`);

ALTER TABLE `fos_user_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_583D1F3E5E237E06` (`name`);

ALTER TABLE `fos_user_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_C560D76192FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_C560D761A0D96FBF` (`email_canonical`);

ALTER TABLE `fos_user_user_group`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `IDX_B3C77447A76ED395` (`user_id`),
  ADD KEY `IDX_B3C77447FE54D947` (`group_id`);

ALTER TABLE `incident`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1F1B251EC4663E4` (`page_id`);

ALTER TABLE `item_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lookup_unique_idx` (`locale`,`object_id`,`field`,`foreign_key`),
  ADD KEY `IDX_9ED2746232D562B` (`object_id`);

ALTER TABLE `lexik_translation_file`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `hash_idx` (`hash`);

ALTER TABLE `lexik_trans_unit`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key_domain_idx` (`key_name`,`domain`);

ALTER TABLE `lexik_trans_unit_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `trans_unit_locale_idx` (`trans_unit_id`,`locale`),
  ADD KEY `IDX_B0AA394493CB796C` (`file_id`),
  ADD KEY `IDX_B0AA3944C3C583C9` (`trans_unit_id`);

ALTER TABLE `local`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `locataire`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_468D74483D427250` (`CIN`),
  ADD UNIQUE KEY `UNIQ_468D744817389453` (`permis`),
  ADD KEY `IDX_468D7448A76ED395` (`user_id`);

ALTER TABLE `Locataire_audit`
  ADD PRIMARY KEY (`id`,`rev`);

ALTER TABLE `location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_A7E8EB9D181A8BA` (`voiture_id`),
  ADD KEY `IDX_A7E8EB9DD8A38199` (`locataire_id`),
  ADD KEY `IDX_A7E8EB9DA76ED395` (`user_id`);

ALTER TABLE `marque`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5DC394F8EA9FDD75` (`media_id`);

ALTER TABLE `Marque_audit`
  ADD PRIMARY KEY (`id`,`rev`);

ALTER TABLE `media__gallery`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `media__gallery_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_80D4C5414E7AF8F` (`gallery_id`),
  ADD KEY `IDX_80D4C541EA9FDD75` (`media_id`);

ALTER TABLE `media__media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5C6DD74E12469DE2` (`category_id`);

ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`menu_id`,`item_id`),
  ADD KEY `IDX_D754D550CCD7E912` (`menu_id`),
  ADD KEY `IDX_D754D550126F525E` (`item_id`);

ALTER TABLE `modele`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `notification__message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_created_at` (`created_at`),
  ADD KEY `notification_message_state_idx` (`state`),
  ADD KEY `notification_message_created_at_idx` (`created_at`);

ALTER TABLE `page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_140AB620EA9FDD75` (`media_id`),
  ADD KEY `IDX_140AB620C9948F0E` (`page_parente_id`),
  ADD KEY `IDX_140AB620FE927F3B` (`bandeau_id`);

ALTER TABLE `page_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lookup_unique_idx` (`locale`,`object_id`,`field`,`foreign_key`),
  ADD KEY `IDX_78AB76C9232D562B` (`object_id`);

ALTER TABLE `piece`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_850B2427181A8BA` (`voiture_id`);

ALTER TABLE `revisions`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `siteconf`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `Ticket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_900CA895A76ED395` (`user_id`);

ALTER TABLE `Ticket_audit`
  ADD PRIMARY KEY (`id`,`rev`);

ALTER TABLE `timeline__action`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `timeline__action_component`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6ACD1B169D32F035` (`action_id`),
  ADD KEY `IDX_6ACD1B16E2ABAFFF` (`component_id`);

ALTER TABLE `timeline__component`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1B2F01CDD1B862B8` (`hash`);

ALTER TABLE `timeline__timeline`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FFBC6AD59D32F035` (`action_id`),
  ADD KEY `IDX_FFBC6AD523EDC87` (`subject_id`);

ALTER TABLE `typevehicule`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1483A5E992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_1483A5E9A0D96FBF` (`email_canonical`),
  ADD KEY `IDX_1483A5E9EA9FDD75` (`media_id`);

ALTER TABLE `version`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `voiture`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_265FB893B732A364` (`energie_id`),
  ADD KEY `IDX_265FB89334B45C66` (`boitevitesse_id`),
  ADD KEY `IDX_265FB893B29E9C9B` (`typeVehicule_id`),
  ADD KEY `IDX_265FB8934827B9B2` (`marque_id`),
  ADD KEY `IDX_265FB893A76ED395` (`user_id`);

ALTER TABLE `Voiture_audit`
  ADD PRIMARY KEY (`id`,`rev`);

ALTER TABLE `voiture_equipement`
  ADD PRIMARY KEY (`voiture_id`,`equipement_id`),
  ADD KEY `IDX_C99F3755181A8BA` (`voiture_id`),
  ADD KEY `IDX_C99F3755806F0F5C` (`equipement_id`);


ALTER TABLE `acl_classes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `acl_entries`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `acl_object_identities`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `acl_security_identities`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `anomalie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `blacklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
ALTER TABLE `block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `boitevitesse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `classification__category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `classification__collection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `classification__tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `energie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `equipement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
ALTER TABLE `ext_log_entries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `ext_translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `fos_user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `fos_user_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `incident`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `item_translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lexik_translation_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lexik_trans_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `lexik_trans_unit_translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `local`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `locataire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
ALTER TABLE `marque`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
ALTER TABLE `media__gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `media__gallery_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `media__media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `modele`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
ALTER TABLE `notification__message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `page_translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `piece`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `revisions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
ALTER TABLE `siteconf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `timeline__action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=89;
ALTER TABLE `timeline__action_component`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=349;
ALTER TABLE `timeline__component`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=85;
ALTER TABLE `timeline__timeline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=353;
ALTER TABLE `typevehicule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `version`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `voiture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;

ALTER TABLE `acl_entries`
  ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `acl_object_identities`
  ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

ALTER TABLE `acl_object_identity_ancestors`
  ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `blacklist`
  ADD CONSTRAINT `FK_19D47E18D8A38199` FOREIGN KEY (`locataire_id`) REFERENCES `locataire` (`id`);

ALTER TABLE `blacklist_incident`
  ADD CONSTRAINT `FK_286C007159E53FB9` FOREIGN KEY (`incident_id`) REFERENCES `incident` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_286C007193FA9730` FOREIGN KEY (`blacklist_id`) REFERENCES `blacklist` (`id`) ON DELETE CASCADE;

ALTER TABLE `block`
  ADD CONSTRAINT `FK_831B9722C4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`);

ALTER TABLE `classification__category`
  ADD CONSTRAINT `FK_43629B36727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `classification__category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_43629B36E25D857E` FOREIGN KEY (`context`) REFERENCES `classification__context` (`id`),
  ADD CONSTRAINT `FK_43629B36EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`) ON DELETE SET NULL;

ALTER TABLE `classification__collection`
  ADD CONSTRAINT `FK_A406B56AE25D857E` FOREIGN KEY (`context`) REFERENCES `classification__context` (`id`),
  ADD CONSTRAINT `FK_A406B56AEA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`) ON DELETE SET NULL;

ALTER TABLE `classification__tag`
  ADD CONSTRAINT `FK_CA57A1C7E25D857E` FOREIGN KEY (`context`) REFERENCES `classification__context` (`id`);

ALTER TABLE `equipement`
  ADD CONSTRAINT `FK_F7E9C523181A8BA` FOREIGN KEY (`voiture_id`) REFERENCES `voiture` (`id`);

ALTER TABLE `fos_user_user_group`
  ADD CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_user_group` (`id`) ON DELETE CASCADE;

ALTER TABLE `item`
  ADD CONSTRAINT `FK_1F1B251EC4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`);

ALTER TABLE `item_translations`
  ADD CONSTRAINT `FK_9ED2746232D562B` FOREIGN KEY (`object_id`) REFERENCES `item` (`id`) ON DELETE CASCADE;

ALTER TABLE `lexik_trans_unit_translations`
  ADD CONSTRAINT `FK_B0AA394493CB796C` FOREIGN KEY (`file_id`) REFERENCES `lexik_translation_file` (`id`),
  ADD CONSTRAINT `FK_B0AA3944C3C583C9` FOREIGN KEY (`trans_unit_id`) REFERENCES `lexik_trans_unit` (`id`);

ALTER TABLE `locataire`
  ADD CONSTRAINT `FK_468D7448A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

ALTER TABLE `location`
  ADD CONSTRAINT `FK_A7E8EB9D181A8BA` FOREIGN KEY (`voiture_id`) REFERENCES `voiture` (`id`),
  ADD CONSTRAINT `FK_A7E8EB9DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_A7E8EB9DD8A38199` FOREIGN KEY (`locataire_id`) REFERENCES `locataire` (`id`);

ALTER TABLE `marque`
  ADD CONSTRAINT `FK_5DC394F8EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`);

ALTER TABLE `media__gallery_media`
  ADD CONSTRAINT `FK_80D4C5414E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `media__gallery` (`id`),
  ADD CONSTRAINT `FK_80D4C541EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`);

ALTER TABLE `media__media`
  ADD CONSTRAINT `FK_5C6DD74E12469DE2` FOREIGN KEY (`category_id`) REFERENCES `classification__category` (`id`) ON DELETE SET NULL;

ALTER TABLE `menu_item`
  ADD CONSTRAINT `FK_D754D550126F525E` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_D754D550CCD7E912` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE;

ALTER TABLE `page`
  ADD CONSTRAINT `FK_140AB620C9948F0E` FOREIGN KEY (`page_parente_id`) REFERENCES `page` (`id`),
  ADD CONSTRAINT `FK_140AB620EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`),
  ADD CONSTRAINT `FK_140AB620FE927F3B` FOREIGN KEY (`bandeau_id`) REFERENCES `media__media` (`id`);

ALTER TABLE `page_translations`
  ADD CONSTRAINT `FK_78AB76C9232D562B` FOREIGN KEY (`object_id`) REFERENCES `page` (`id`) ON DELETE CASCADE;

ALTER TABLE `piece`
  ADD CONSTRAINT `FK_850B2427181A8BA` FOREIGN KEY (`voiture_id`) REFERENCES `voiture` (`id`);

ALTER TABLE `Ticket`
  ADD CONSTRAINT `FK_900CA895A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

ALTER TABLE `timeline__action_component`
  ADD CONSTRAINT `FK_6ACD1B169D32F035` FOREIGN KEY (`action_id`) REFERENCES `timeline__action` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_6ACD1B16E2ABAFFF` FOREIGN KEY (`component_id`) REFERENCES `timeline__component` (`id`) ON DELETE CASCADE;

ALTER TABLE `timeline__timeline`
  ADD CONSTRAINT `FK_FFBC6AD523EDC87` FOREIGN KEY (`subject_id`) REFERENCES `timeline__component` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_FFBC6AD59D32F035` FOREIGN KEY (`action_id`) REFERENCES `timeline__action` (`id`);

ALTER TABLE `users`
  ADD CONSTRAINT `FK_1483A5E9EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`);

ALTER TABLE `voiture`
  ADD CONSTRAINT `FK_265FB89334B45C66` FOREIGN KEY (`boitevitesse_id`) REFERENCES `boitevitesse` (`id`),
  ADD CONSTRAINT `FK_265FB8934827B9B2` FOREIGN KEY (`marque_id`) REFERENCES `marque` (`id`),
  ADD CONSTRAINT `FK_265FB893A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_265FB893B29E9C9B` FOREIGN KEY (`typeVehicule_id`) REFERENCES `typevehicule` (`id`),
  ADD CONSTRAINT `FK_265FB893B732A364` FOREIGN KEY (`energie_id`) REFERENCES `energie` (`id`);

ALTER TABLE `voiture_equipement`
  ADD CONSTRAINT `FK_C99F3755806F0F5C` FOREIGN KEY (`equipement_id`) REFERENCES `Equipement` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_C99F3755181A8BA` FOREIGN KEY (`voiture_id`) REFERENCES `Voiture` (`id`) ON DELETE CASCADE;
